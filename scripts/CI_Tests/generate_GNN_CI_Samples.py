"""
Module to create dummy ntuples for CI for GNNs
"""

import numpy as np
import uproot


class point():
    """
    Class to define a simple x,y,z point
    """

    def __init__(self, x, y, z):
        """Init method of point class

        :param x: x coordinate
        :param y: y coordinate
        :param z: z coordinate
        :returns: point object

        """
        self.x = x
        self.y = y
        self.z = z

    def __add__(self, other):
        """overloading the add operator

        :param other: other point object
        :returns: point object

        """
        x = self.x + other.x
        y = self.y + other.y
        z = self.z + other.z
        return point(x, y, z)

    def __sub__(self, other):
        """overloading the sub operator

        :param other: other point object
        :returns: point object

        """
        x = self.x - other.x
        y = self.y - other.y
        z = self.z - other.z
        return point(x, y, z)

    def __mult__(self, other):
        """overloading the mult operator

        :param other: other point object
        :returns: point object

        """
        x = self.x * other.x
        y = self.y * other.y
        z = self.z * other.z
        return point(x, y, z)

    def mag(self):
        """Calculates inner product of point object

        :returns: inner product of point object

        """
        return np.sqrt(self.x**2 + self.y**2 + self.z**2)


class graph():
    """
    Class for a simple graph
    """

    def __init__(self, d, n):
        """Init method of graph class

        :param d: target distance between nodes in graph
        :param n: number of nodes
        :returns: graph object

        """
        self.nodes = []
        self.fill(d, n)

    def fill(self, d, n):
        """Fill a graph with n points of maximum distance d

        :param d: maximum distance in negative and positive direction
        :param n: number of nodes in graph
        :returns:

        """
        for _ in range(n):
            x, y, z = np.random.uniform(-1 * d, 1 * d, size=3)
            self.addnode(point(x, y, z))

    def addnode(self, new_point):
        """Adds a node

        :param point: point object
        :returns: nothing

        """
        self.nodes.append(new_point)

    def get_edge(self, node_i, node_j):
        """Adds an edge between node i and node j

        :param node_i: integer i
        :param node_j: integer j
        :returns: nothing

        """
        return (self.nodes[node_i] - self.nodes[node_j]).mag()

    def get_x(self, node_i):
        """return x value of node i

        :param node_i: integer i
        :returns: x value of node i

        """
        return self.nodes[node_i].x

    def get_y(self, node_i):
        """return y value of node i

        :param node_i: integer i
        :returns: y value of node i

        """
        return self.nodes[node_i].y

    def get_z(self, node_i):
        """return z value of node i

        :param node_i: integer i
        :returns: z value of node i

        """
        return self.nodes[node_i].z

    def get_x_tot(self):
        """get sum of x values of all nodes

        :returns: sum of x values

        """

        return np.sum([n.x for n in self.nodes])

    def get_y_tot(self):
        """get sum of y values of all nodes

        :returns: sum of y values

        """

        return np.sum([n.y for n in self.nodes])

    def get_z_tot(self):
        """get sum of z values of all nodes

        :returns: sum of z values

        """
        return np.sum([n.z for n in self.nodes])


graph_dict = {}
ngraphs = 10000
nnodes = 4
for distance in [1, 2, 5]:
    graph_dict[f"Graph_{distance}"] = [graph(distance, nnodes) for i in range(ngraphs)]

for distance in [1, 2, 5]:
    val_dict = {}
    gname = f"Graph_{distance}"
    print(f"CI PREPARATION:\t creating {gname}.root")
    with uproot.recreate(f"CI_Test_GNN_Ntuples/{gname}.root") as f_data:
        for i in range(nnodes):
            val_dict[f"x_{i}"] = np.array([g.get_x(i) for g in graph_dict[gname]])
            val_dict[f"y_{i}"] = np.array([g.get_x(i) for g in graph_dict[gname]])
            val_dict[f"z_{i}"] = np.array([g.get_x(i) for g in graph_dict[gname]])
        val_dict["x_tot"] = np.array([g.get_x_tot() for g in graph_dict[gname]])
        val_dict["y_tot"] = np.array([g.get_y_tot() for g in graph_dict[gname]])
        val_dict["z_tot"] = np.array([g.get_z_tot() for g in graph_dict[gname]])
        val_dict["eventNumber"] = np.arange(ngraphs)
        val_dict["weight_1"] = np.random.normal(loc=1.0, scale=1.0, size=ngraphs)
        val_dict["weight_2"] = np.random.normal(loc=1.0, scale=1.0, size=ngraphs)
        val_dict["Selection_1"] = np.random.normal(loc=1.0, scale=1.0, size=ngraphs)
        val_dict["Selection_2"] = np.random.normal(loc=1.0, scale=1.0, size=ngraphs)
        for i in range(nnodes):
            for j in range(i, nnodes):
                val_dict[f"D_{i}_{j}"] = np.array([g.get_edge(i, j) for g in graph_dict[gname]])
        f_data["test_tree"] = val_dict
        f_data.close()
print("CI PREPARATION:\t Done!")
