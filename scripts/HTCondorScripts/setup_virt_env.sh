#!/bin/bash

echo -e "SETUP\t Python version and ROOT"
setupATLAS
lsetup "root 6.26.08-x86_64-centos7-gcc11-opt"
echo -e "SETUP\t Setting up environmental variables"

# Print current working directory

echo "Script executed in: $(pwd)"

# Create virtual environment

echo "Creating virtual environment..."
python3 -m venv myenv

# Activate virtual environment
source myenv/bin/activate

# Reset the PYTHONPATH variable and add ROOT libraries to it
export PYTHONPATH=$VIRTUAL_ENV/lib64/python3.9/:$ROOTSYS/lib
# Install required packages

python -m ensurepip --upgrade

pip install --no-cache-dir skl2onnx # will also install scikit-learn
pip install --no-cache-dir uproot
pip install --no-cache-dir pandas
pip install --no-cache-dir tables
pip install --no-cache-dir torch
pip install --no-cache-dir torchvision torchaudio
pip install --no-cache-dir torch_geometric
pip install --no-cache-dir pydot

# Check if ROOT is available

 if command -v root >/dev/null 2>&1 ; then
     echo "ROOT is available in the environment"
 else
     echo "ROOT is not available in the environment. Please install ROOT and add it to your PATH."
 fi

 # Check if packages were installed correctly

pip freeze | grep -E "uproot|pandas|numpy|tables|scikit-learn|torch"

# Deactivate virtual environment
echo "Deactivate virtual environment"
deactivate

