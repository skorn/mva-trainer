#!/bin/bash

cd $MVA_TRAINER_BASE_DIR
echo "=== Executing setup ==="
if [ -d "scripts/HTCondorScripts/myenv" ]; then #Check if the venv option is used. Default is that no venv is used
    echo "Virtual environment found!"
    source scripts/HTCondorScripts/myenv/bin/activate
else
    echo "First run setup_virt_env.sh script in HTCondorScripts directory"
    exit 1
fi

echo "Sourcing mva-trainer setup script"
source setup.sh
echo "Current working directory:"
pwd
echo "Listing content of current directory:"
ls

#Initialise output value of python script to 0
conv_return_value=0
train_return_value=0
eval_return_value=0

if [ $1 == "Converter" ]; then
    echo "=== Executing Converter ==="
    ./python/mva-trainer.py -c config/$2 --convert
    conv_return_value=$? #Get the exit code of the python script -> In HTCondor exit code not passed on properly
fi
if [ $1 == "Trainer" ]; then
    echo "=== Executing Trainer ==="
    ./python/mva-trainer.py -c config/$2 --train --fold $3
    train_return_value=$? #Get the exit code of the python script -> In HTCondor exit code not passed on properly
fi
if [ $1 == "Evaluater" ]; then
    echo "=== Executing Evaluater ==="
    ./python/mva-trainer.py -c config/$2 --evaluate
    eval_return_value=$? #Get the exit code of the python script -> In HTCondor exit code not passed on properly
fi
if [ $1 == "All" ]; then
    echo "=== Executing Converter ==="
    ./python/mva-trainer.py -c config/$2 --convert
    conv_return_value=$? #Get the exit code of the python script -> In HTCondor exit code not passed on properly
    echo "=== Executing Trainer ==="
    ./python/mva-trainer.py -c config/$2 --train --fold $3
    train_return_value=$? #Get the exit code of the python script -> In HTCondor exit code not passed on properly
    echo "=== Executing Evaluater ==="
    ./python/mva-trainer.py -c config/$2 --evaluate
    eval_return_value=$? #Get the exit code of the python script -> In HTCondor exit code not passed on properly
fi

echo "=== Leaving venv ==="
deactivate
echo "=== End ==="

#If somewhere the python script failed, exit with that explicit exit code
if [ $conv_return_value != 0 ]; then
    exit $conv_return_value
fi
if [ $train_return_value != 0 ]; then
    exit $train_return_value
fi
if [ $eval_return_value != 0 ]; then
    exit $eval_return_value
fi

