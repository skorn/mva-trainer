# K-Folding: A Technique for Better Model Training and Evaluation

When training machine learning models, evaluating their performance on data that was not used for training is crucial. One way to do this is by using k-folding, a technique that involves dividing the dataset into `k` equal subsets, or "folds," using one as the validation set while the others are used for training.

Let us say we have a dataset with `n` events and want to train `k` models on this dataset. To use k-folding, we would first divide the dataset into `k` folds, where `k` is a positive integer. Each fold contains `n/k` events.

Next, we train `k` models, each using a different combination of `k-1` folds for training and the remaining fold for testing. For example, if we were using 4-folding (k=4), we would train five models as follows:

    Model 1: train on folds 2-4, test on fold 1
    Model 2: train on folds 1, 3-4, test on fold 2
    Model 3: train on folds 1-2, 4, test on fold 3
    Model 4: train on folds 1-3, test on fold 4

Once we have trained the models, we can evaluate the events using the model foreseen to test this particular fold.

The k-folding procedure is also visualised in the picture below. In addition, the validation set is shown in light blue.
It is drawn from the dark blue train set. It is used to monitor the validation loss throughout the training. If `Patience` and `MinDelta` is defined, it will be used to determine when to stop the training of a fold.

![kfolding](../img/kfolding.png)

In mva-trainer, you can define the number of folds using the `Folds` parameter in the `GENERAL` settings.
When reading the ntuples, the code will iterate through the input variables and split all events based on the variable defined in `FoldSplitVariable`.
By default, this will be `eventNumber`.
The split is done by applying the modulo operator `%` on the `FoldSplitVariable`.
The result of this operation uniquely determines the fold an event enters.
This procedure is performed on MC and data.

The consequence of this is, that you will not have "one model" that does everything but instead `k` models, where each model is only applied on a subset of MC and data.
If your training sample is large enough this should have no significant effect on the shape of the output distributions.
However, analysers are advised to check that
* their training statistics is indeed large enough such that splitting it into subfolds has no significant effect.
* the output distributions do not differ significantly between the different models.
* the achieved loss values converge towards a similar global minimum.