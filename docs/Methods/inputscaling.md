# Scaling of input variables

`MinMax` and `Standard` scaling are both commonly used techniques to normalize the input data before feeding it into a neural network.
Normalizing the input data is important because it helps the neural network converge faster and perform better.

## MinMax Scaling

`MinMax` scaling is a normalization technique that scales the input features to a fixed range of values between 0 and 1. It works by subtracting the minimum value of the feature and then dividing by the range (i.e., the difference between the maximum and minimum values). The formula for MinMax scaling is:

<img src="https://latex.codecogs.com/svg.image?X_{\text{scaled}}=\frac{X-X_{\text{min}}}{X_{\text{max}}-X_{\text{min}}}" title="MinMax scaling formula" />

where X is the input feature, X_min and X_max are the minimum and maximum values of the feature, respectively, and X_scaled is the normalized value of X.

## Standard Scaling

`Standard` scaling, also known as z-score normalization, is another popular normalization technique. It scales the input features so that they have a mean of 0 and a standard deviation of 1. It works by subtracting the mean of the feature and then dividing by the standard deviation. The formula for standard scaling is:

<img src="https://latex.codecogs.com/svg.image?X_{\text{scaled}}=\frac{X-\mu}{\sigma}" title="Standard scaling formula" />

where X is the input feature, mu is the mean of the feature, sigma is the standard deviation of the feature, and X_scaled is the normalized value of X.

Both MinMax scaling and standard scaling have their pros and cons, and the choice of normalization technique depends on the specific problem and the nature of the input data. In general, MinMax scaling is a good choice when the input data is known to have a fixed range, while standard scaling is a good choice when the input data is normally distributed.

## pT-based scaling

pT-based scaling is a method of scaling values based on the pT (transverse momentum) of objects of the same type.

To use pT-based scaling, you first define the objects in the `VARIABLE` block and group them by type using the `Type` setting. Then, for each object of a particular type, the pT value is taken and divided by the sum of the pT values of all the objects in that type. Additionally, the logarithm is taken. This is because pT values can vary over many orders of magnitude, and taking the logarithm helps to balance that out. The formula for pT-based scaling is:

<img src="https://latex.codecogs.com/svg.image?\text{Scaled&space;Value}_i=\log\left(\frac{p_{T,i}}{\sum_{j\in\text{Type}}p_{T,j}}\right)" title="pT-based scaling formula" />

Where `i` is the index of the object you're scaling, and `j` is the index of the other objects in the same type.

## Mixing different scaling options

Different scaling options can also be combined, i.e. you can e.g. use `MinMax` scaling for one variable `Standard` for another and `pT-based scaling` for some or all of your pT-related variables.
An example of this is shown in the figure below. Notice that you can also "split up" the pT-based scaling.

![mix_of_scaling](../img/scaling.png)