# Dense Neural Network

## Introduction 
Dense neural networks, also known as fully connected neural networks, are a type of artificial neural network where each neuron in one layer is connected to every neuron in the following layer. They are commonly used in deep learning for various applications, such as image and speech recognition, natural language processing, and predictive analytics. In a dense neural network, the input is passed through a series of hidden layers, where each layer learns increasingly complex data features before producing an output. With the help of backpropagation and optimisation algorithms, the network's weights and biases are adjusted to minimise a predefined loss function.

In MVA-trainer, dense neural networks are realised using [PyTorch](https://pytorch.org/).

PyTorch is a popular open-source machine learning library for Python used to develop and train deep learning models. Developed by Facebook's artificial intelligence research group, PyTorch is widely used by researchers and developers for its ease of use.

Information to get started with PyTorch can be found [here](https://pytorch.org/get-started/locally/).

## Defining the Architecture

When it comes to deep neural networks, several essential terms are commonly used to describe the architecture of a network. The most important terms include hidden layers, nodes, activation functions, dropout layers, and batch normalisation layers.

A neural network consists of one or more hidden layers responsible for processing the input data and producing an output. Each hidden layer comprises a series of nodes, which perform calculations on the input data and pass the result to the next layer.
A typical layour is sketched in the figure below.

![NN_layout](../img/NN_layout.png)

*Nodes* are individual units within a neural network that receive input from the previous layer and produce output for the next layer. Each node performs a weighted sum of the input data, applies an activation function to the result, and passes the output to the next layer.

*Activation functions* are used within nodes to introduce nonlinearity into the network output. They are used to model complex relationships between the input and output data and ensure that the network output is bounded and can be easily interpreted.

*Dropout layers* reduce overfitting in a neural network by randomly dropping out a percentage of nodes during training. This helps prevent the network from becoming too specialised in the training data and ensures that it can generalise to new data.

*Batch normalisation layers* are used to normalise the input data to each layer of the network, improving the stability and performance of the network. They work by normalising the output of each layer to have a mean of 0 and a standard deviation of 1, making it easier for the network to learn the appropriate weights and biases for each layer.

The first step of building a dense neural network is defining its architecture.
For this purpose, several tools are in place in MVA-trainer.
The architecture of a dense neural network is mainly defined by the following:

* the number of layers
* the number of nodes in each layer
* The activation functions of the layers
* where regularisation layers such as `dropout` or `batch normalisation` is applied.

In the `DNNMODEL` block, a dense neural network can be easily realised.
In the following, we will look at the example of a simple classifier with three layers with 50, 40 and 30 nodes each:

```
DNNMODEL
    Name = "My_Classifier"
    Type = Classification-DNN
    Nodes = 50,40,30
    ActivationFunctions = "relu","tanh","sigmoid"
    BatchNormIndece = 1,2
    DropoutIndece = 2,3
    OutputSize = 1
    OutputActivation = "sigmoid"
```
In the block, the name and type of the classifier are defined using the `name` and `type` options.
The `Nodes` option expects a list of integers which (in this example) defines the number of nodes per hidden layer.
The `ActivationFunctions` option defines which activation functions are used in the hidden layers.
In this example, `relu`, `tanh`, and `sigmoid` are used.
`BatchNormIndece` and `DropoutIndece` describe the number of hidden layers, after which either `dropout` or `batch normalisation` layers are added for regularisation.
The `OutputSize` and `OutputActivation` options define the shape and activation function of the output layer and hence determine whether the classifier is a binary or multi-class classifier.

### Simple mathematical operations inside a DNN

Dense neural networks, also known as fully connected networks, constitute a fundamental architecture in deep learning. In these networks, each node in a given layer is connected to every node in the subsequent layer. Mathematically, the operation of a node in a dense layer can be expressed as follows:

Let $x_i$ be the input to the node, $w_{ij}$ denote the weight of the connection between node $i$ in the current layer and node $j$ in the next layer, and $b_j$ represent the bias term for node $j$. The output $y_j$ of node $j$ is obtained through the application of an activation function $f$ on the weighted sum of inputs and biases:

\begin{align}
y_j = f\left(\sum_{i} w_{ij}x_i + b_j\right)
\end{align}

Activation functions, such as the widely used Rectified Linear Unit (ReLU) or sigmoid functions, introduce non-linearity into the network. This non-linearity is crucial for the network's ability to learn complex patterns and relationships within the data. Activation functions enable neural networks to model intricate mappings and capture hierarchical features, enhancing the network's capacity to represent and generalize from diverse datasets.

### Dropout explained

Dropout is a technique used in dense neural networks to prevent overfitting and improve the model's generalisation performance. In a dense neural network, the neurons in each layer are interconnected. Dropout randomly deactivates a fraction of these neurons at each iteration during training (symbolised by the white neurons in the hidden layer in the figure below), meaning that their outputs are ignored for that particular iteration. This prevents any single neuron from becoming too specialised or dominant in learning.

![Dropout](../img/NN_layout_with_dropout.png)

The dropout process introduces uncertainty into the training procedure, as the network is forced to learn incomplete information due to the random dropout of neurons. This encourages the network to be more robust and less reliant on specific neurons, features, or combinations. Consequently, dropout improves the model's ability to generalise well to new, unseen data.

During inference or when making predictions, all neurons are typically active, and their outputs are scaled by the dropout rate used during training. This scaling ensures that the expected output remains consistent with what the network learned during training, effectively emulating an ensemble of different neural network architectures.

### Batch normalisation explained

Batch Normalization (BN) is a technique in deep learning that aims to improve the training stability and speed of neural networks by normalizing the input of each layer. During training, BN calculates the mean ($\mu$) and standard deviation ($\sigma$) of the inputs within a mini-batch and normalizes the values using the following formulas:

\begin{align}
\hat{x} = \frac{x - \mu}{\sqrt{\sigma^2 + \epsilon}}
\end{align}

Here, $x$ is the input to the layer, $\epsilon$ is a small constant added for numerical stability, an $\hat{x}$ is the normalized output. The normalized values are then scaled $\gamma$ and shifted $\beta\$ using learnable parameters:

\begin{align}
y = \gamma \hat{x} + \beta
\end{align}

This normalization helps mitigate issues like internal covariate shift, where the distribution of inputs to a layer changes during training. BN enables the use of higher learning rates, accelerates convergence, and reduces sensitivity to weight initialization. Additionally, it acts as a regularizer, reducing the need for other regularization techniques like dropout. By normalizing the activations, Batch Normalization has become a widely adopted method in deep learning architectures, contributing to improved model generalization and overall performance.

## Early Stopping

In machine learning, *Early Stopping* is a technique used to prevent overfitting of the model on the training data by monitoring the validation loss during the training process. The idea is to stop the training process early, before the model overfits, by selecting the best-performing model based on the validation loss. The idea is schematically sketched in the figure below:

![Early Stopping](../img/Early_Stopping.png)

The training process is stopped early if the model's performance starts to degrade.
I.e., given a validation set with loss function values $l_1, l_2, \ldots, l_n$, the training process is terminated when the loss function value stops decreasing, such as when
\begin{align}
 v_l \leq l_{i-p}-\Delta_\mathrm{min}\quad|\forall p\in[0,P];\;i\in\mathbb{Z};\;i-p>0,
\end{align}
where $P$ describes a positive integer number of epochs to be monitored, denoted $\textit{patience}$, and $\Delta_\mathrm{min}$ the minimum amount of change of the loss value that qualifies as an improvement.
Afterwards, the model configuration with minimal loss is restored.

## Hyperparameter optimisation

Hyparameter optimisation is a crucial aspect of training a good neural network. 
Currently, hyperparameter optimisation can be performed for deep neural networks.
The following parameters steer the hyperparameter optimisation:

* MaxLayers
* MinLayers
* MaxNodes
* MinNodes
* StepNodes
* SetActivationFunctions

To run hyperparameter optimisation, you can use the `--optimise` flag for MVA-trainer.
```sh
python3 python/mva-trainer -c <config_path> --HO_options <HO_options> --NModels <NModels> --RandomSeed <RandomSeed>
```
The script will take the config file specified by `config_path` (and the corresponding options) as a "seed config" and create `NModels`, new randomised (control seed using `RandomSeed`) configs in the `HTCondorConfig_path` directory. Furthermore, `.sub` (and `.dag`) files are created.
Three `HO_options` are available to the user:

| Option | Effect |
| ------ | ------ |
| Converter | In this case, a `.sub` file is created in the specified `HTCondorConfig_path` directory. To start the conversion (in this case, only the conversion is run), submit the `.sub` file to the HTCondor daemon using the `condor_submit` command.|
| **Trainer (Default)** | In this case, a `.sub` file is created in the specified `HTCondorConfig_path` directory. To start the training (in this case, only the training is run), submit the `.sub` file to the HTCondor daemon using the `condor_submit` command. **For this job to work, converted files must already exist.**|
| Evaluater | In this case, a `.sub` file is created in the specified `HTCondorConfig_path` directory. To start the evaluation (in this case, only the evaluation is run), submit the `.sub` file to the HTCondor daemon using the `condor_submit` command. **For this job to work, the training step must have been executed beforehand.**|
| All |  In this case, a `.dag` file is created in the specified `HTCondorConfig_path` directory. In addition, two `.sub` files are created, one for the conversion and one for the training. The files are created so that the conversion only runs once per optimisation. To start optimising (in this case, a conversion followed by training all networks), submit the `.dag` file to the HTCondor daemon using the `condor_submit_dag` command.|
