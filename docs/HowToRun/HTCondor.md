# Running individual commands using HTCondor

[HTCondor](https://htcondor.org/), also known as the High-Throughput Computing Condor, is a powerful and versatile distributed computing software system. It is designed to manage and execute computing tasks across a large pool of networked computers or clusters.

At its core, HTCondor provides a job queuing mechanism that allows users to submit their computational tasks to a central queue. The system then intelligently schedules and dispatches these jobs to available resources, which can range from desktop workstations to high-performance computing clusters. HTCondor is particularly effective in harnessing idle computing resources and maximizing their utilization, making it an ideal solution for organizations with a substantial number of underutilized machines.

HTCondor supports a wide range of job types and can handle both parallel and serial computations. It also offers a variety of sophisticated scheduling policies and resource management features, including priority-based job scheduling, job preemption, and fault tolerance mechanisms. This enables users to define complex workflows and optimize the execution of their computational workloads.

When working with MVA-trainer you might be interested in running several custom configs at the same time. This is where a batch system like HTCondor can be of help. The following sections will briefly explain how the submission of individual jobs works. If you are looking for the submission of several hundred jobs please consult the optimisation part of this documentation.

## Writing a shell script

The core of running an mva-trainer "job" using HTCondor is a simple shell script.
The purpose of this script is to "wrap" around the python command you would otherwise execute in your shell.
Furthermore, the script sets up or finds the relevant working environment.
In our case this is a virtual environment.

An example of such a script is given in `scripts/HTCondorScripts/HTCondorMain.sh`.
For the script to work via HTCondor you have to execute the `setup.sh` script and provide a virtual environment within the `scripts/HTCondorScripts` directory.
A dedicated setup-script for the virtual environment is provded in the form of `setup_virt_env.sh`.

## Writing a condor submission script

To "submit" your "job" to the HTCondor queue you will need to write a so-called submission script.
A HTCondor submission script is a file that outlines the job details, such as the executable, input/output files, and job-specific parameters. It specifies the execution environment, file transfers, dependencies, and execution configuration. Once submitted, HTCondor schedules and runs the job based on the script and available resources.
An example of such a scrupt is provided in `scripts/HTCondorScripts/MVA.sub`.
To make this work for you can change the following parameters within the submission script:

* in the `arguments` option: You can use `converter`, `trainer`, `evaluater`.
* in the `arguments` option: Make sure to provide the absolute path to the config needed.
* in the `JobFlavour` option: Here you can make use of different job flavours for longer run times.
* in the `OutputFiles` section: Here you can steer how the error, log and command line output is written and where it is saved.
* finally in the `Notifications` section: Here you can configure whether you want to receive emails upon the completion of your jobs.
