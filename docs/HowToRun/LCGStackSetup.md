# LCG stacks

LCG stacks are provided by the SPI team in EP-SFT at CERN.
For a introduction to the LCG software stack, please visit the LCG [Introduction for end users](https://lcgdocs.web.cern.ch/lcgdocs/lcgreleases/introduction/).
In a nutshell these "LCG stacks" are software stacks which provide you with the necessary packages you need and also allow you to run newer python versions on lxplus.

# Setup using LCG stacks

To setup the necessary packages we will use an LCG stack. For this example we will use `LCG_102cuda`. If you encounter issues with individual stacks try out others to see whether these issues are stack dependent.
For this do:

```sh
SetupATLAS
source /cvmfs/sft.cern.ch/lcg/views/LCG_102cuda/x86_64-centos7-gcc8-opt/setup.sh
```

This will set up an LCG stack containing the necessary packages to run the code. 

In principle you can also use your favourite LCG stack from [here](https://lcginfo.cern.ch/).
Just make sure you check the `Packages` section to confirm, that the necessary packages are actually included.

You should make sure the following packages are part of the LCG stack:
* tensorflow
* numpy
* tables
* pydot
* uproot
* pandas
* scikit-learn

# Summary of tested LCG stacks

Here is a list of LCG stacks which are currently known to be "buggy" (i.e. stacks that don't work for mva-trainer) or have other issues. Check out the comment column for details:

| **LCG Stack** | **path** | **Comment**|
| ------ | ------ | ------ |
| LCG_102_cuda | `/cvmfs/sft.cern.ch/lcg/views/LCG_102cuda/x86_64-centos7-gcc8-opt/setup.sh` | `Should be used as a default.|
| ------ | ------ | ------ |
| LCG_100_nxcals | `/cvmfs/sft.cern.ch/lcg/views/LCG_100_nxcals/x86_64-centos7-gcc9-opt/setup.sh` | `Tensorflow` is not detected by setup script. Other script still seems to work though.|
| LCG_dev3 | `/cvmfs/sft.cern.ch/lcg/views/LCG_dev3/x86_64-centos7-gcc8-opt/setup.sh` | `uproot` is not found at all and setup script returns error. |
| LCG_100_ATLAS_1 | `/cvmfs/sft.cern.ch/lcg/views/LCG_100_ATLAS_1/x86_64-centos7-gcc8-opt/setup.sh` | `uproot` is not found at all and setup script returns error. |
| LCG_97apython3_ATLAS_1 | `/cvmfs/sft.cern.ch/lcg/views/LCG_97apython3_ATLAS_1/x86_64-centos7-gcc8-opt/setup.sh` | `uproot` version not compatible with code. This leads to a crash when the conversion step or injection step is run.|
