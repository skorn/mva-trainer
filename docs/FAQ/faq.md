If you have questions to which you cannot find the answer, you can use the mattermost channel to raise these questions to a larger audience.
You can find the contact information on the first page of this website.

## Training questions

???+ question "What should I do in case my training does not seem to converge or I see poor performance?"
     There can be several reasons why your model is not performing as good as you'd like it to perform. Firstly you should remember that you
     are probably working on a complicated problem for which you would not expect perfect performance in the first place. Hence some imperfections
     are always expected. Nevertheless here are some ideas on what you can do to improve your model:
     
     * Think about the problem from a different point of view. Sometimes rethinking what your model should actually do helps already a lot.
     * Think about the variables you provide to your model. Are these enough to learn about the underlying physics? Can you maybe engineer other useful variables?
     * Study the control plots. Don't focus on one particular metric. Look at the bigger picture. The distributions in the control plots can give you indications where the problems are.
     * Try hyperparameter optimisation.

???+ question "I observe spiky loss curves. What does this mean?"
     Spiky loss curves usually mean, that your model is very sensitive to small changes.
     If you observe a spiky loss evolution during the training of a neural network, there are several things you can try to improve the performance:
     
     * Adjust the learning rate: The learning rate determines the size of the step the optimizer takes in the direction of the gradient. A high learning rate can cause the optimizer to overshoot the optimal solution and lead to a spiky loss evolution. Try reducing the learning rate to see if it improves the stability of the loss.
     * Increase the batch size: A small batch size can cause the loss to be more sensitive to noise and result in spiky loss evolution. Try increasing the batch size to smooth out the loss and see if it improves performance.
     * Add regularization: Regularization techniques like dropout can help prevent overfitting and improve the generalization of the model, which can lead to a smoother loss evolution.

     * Check for data quality: Spiky loss evolution can also be caused by poor quality or noisy data. Check if the data is correctly preprocessed and if there are any data anomalies that need to be addressed.
     * Use a different optimizer: The choice of optimizer can also affect the stability of the loss evolution. Try using a different optimizer to see if it improves performance.

???+ question "How is the splitting into several folds done?"
     The splitting into several folds relies on a `k`-folding procedure.
     Ir is done using your config file's FoldSplitVariable' parameter. By default, this is set to `eventNumber`.
     The code then applies the `%` operator to `FoldSplitVariable` such that
     ```
	fold = FoldSplitVariable % Folds
     ```
     where `Folds` is also specified in your config file.
     You should hence make sure, that your `FoldSplitVariable` is actually an `int`!
     This is done for **all** samples, including data.

???+ question "How can I train on a subset of samples and ignore others in the training of a classifier?"
     You can set the `TrainingLabel` parameter of samples you want to ignore in training to any negative value.
     They will subsequently have no direct impact on the training and will only be "piped through" to all control plots.

???+ question "How can I train on a subset of events (e.g. for debugging)?"
     You can set the `DoNOnly` parameter in the `GENERAL` block.
     It expects an integer as an input.
     If set the code will only consider the number of events you specified.

???+ question "How are negative weights handled in the code?"
     There are currently three ways to handle negative event weights. They are all steered via the `TreatNegWeights` parameter in the `GENERAL` block.
     * `None`: Negative weights will impact the training. From a physics point of view this is the recommended treatment. If negative loss values are observed, it is necessary to switch to another option.
     * `Scale`: Per sample all positive weights are scaled down to account for the effect of the negative weights on the sample. Afterwards all negative weights are set to zero. This is recommended if `None` does not work.
     * `SetZero`: All negative event weights are set to zero and hence have no impact on the training

???+ question "Why do I have to access vector elements, e.g. jet_pt[:,0], in such a particular way?"
     The underlying library to read ntuples is [uproot](https://uproot.readthedocs.io/en/latest/index.html), a library for reading and writing ROOT files in pure Python and NumPy.
     Since the ntuples are read simultaneously as arrays vectors are read in as "rows" within one "column" of a (e.g.) pandas [DataFrame](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html) object.
     Hence we have to use the typical python syntax to access "all elements of a given row" of a vector as described in the documentation.

## Statistical tests

???+ question "In the train-test plots I see a probability, what is this probability?"
     For comparison of distributions two statistical tests are available and can be defined by the user through the `ComparisonTest` parameter.
     The Kolmogorov-Smirnov (KS) and Anderson-Darling (AD) tests are nonparametric statistical tests used to compare distributions or test whether a sample follows a specific distribution.

     The KS test compares the empirical distribution function (EDF) of a sample to a known or hypothesized distribution, while the AD test places more weight on the tails of the distribution.
     Both tests are sensitive to both the location and shape of the distribution being tested.

     The KS test statistic is the maximum absolute difference between the EDFs, given by:
     
     <img src="https://latex.codecogs.com/svg.image?{D = \max_{1\leq i\leq n} | F_n(x_i) - F(x_i) |}" title="KS test statistic formula" />
     
     The AD test statistic is defined as:
     
     <img src="https://latex.codecogs.com/svg.image?{A^2 = -n - 1/n \cdot \sum_{i=1}^{n} (2i-1) [ \ln(F(x_i)) + \ln(1-F_n(x_i)) ]}" title="AD test statistic formula" />
     
     The KS test is generally used to compare continuous distributions, while the AD test can be used to test for a variety of distributions, including discrete distributions.
     Hence the AD test is used as the default test.
     The two tests are implemented as described in [Kolmogorov-Smirnov test](https://root.cern.ch/doc/master/classTH1.html#aeadcf087afe6ba203bcde124cfabbee4) and [Anderson-Darling test](https://root.cern.ch/doc/master/classTH1.html#aa395c473ea9693359a74189fbe0ee0db).
     The probability you see in the plot is the so called `p-value`.
     They are a statistical measure used to determine the probability of obtaining a result as extreme as, or more extreme than, the observed result if the null hypothesis is true. In this particular case the null hypothesis assumes that the two distributions (i.e. train versus test) are originating from the same underlying probability distribution (colloquially speaking are identical).
     
     In other words, p-values help to assess the strength of evidence against the null hypothesis. A low p-value  suggests that the observed result is statistically significant, which means it is unlikely to have occurred by chance alone, and we can reject the null hypothesis in favor of the alternative hypothesis at a `confidence limit` given by `1-p`.   

     However, it is important to note that p-values only provide evidence against the null hypothesis and not evidence in favor of the alternative hypothesis.

## Injection questions

???+ question "How can I inject predictions back into my ntuples?"
     For the injection procedure check out [this manual](../Short_Walkthrough/RunInjection.md).

## Questions related to working on lxplus

???+ question "I run the code on lxplus, and after a while I get permission denied errors on `afs` or `eos`. What is happening?"
    This problem most likely comes down to a kerberos authentification problem. I.e. after a while your authentification
    token will become invalid, and you will lose your write permissions.
    This [twiki](https://twiki.cern.ch/twiki/bin/view/Main/Kerberos) can be helpful in solving your problem.
