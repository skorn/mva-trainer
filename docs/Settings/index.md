# Settings for mva-trainer configs

The following tables contain config file options and the effect these options have.

#### `GENERAL` block options
| **Option** | **Effect** |
| ------ | ------ |
| **Inputs** | |
| Job | The name of the `Job`. An output directory (using this string given to this option as the name of the directory) is automatically created and all scripts will store their output in it.|
| NtuplePath | Path to your directory holding your Ntuples. Can be relative or absolute.|
| DoNOnly | Integer value. Can be set to only read the first `N` entries of a file during conversion. Useful for debugging.|
| MCWeight | Python style weight string. Pass a string of (multiplied) `weights`: *weight_mc\*weight_pileup\*...*. |
| Selection | Python style selection string. Pass a string of (boolean) criteria: *(nJets>=2)&(nEl==2) ...* . This can not be a c++-style string because `uproot` is used for reading the `.root` files.|
| Treename | Name of ROOT `TTree` from which the events are taken in the conversion step. |
| IgnoreTreenames | Names of ROOT `TTree` objects which are ignored during the inection step. These trees are then simply copied. You can add a list of TTrees or wildcards of trees here, that do not contain the variables which your model is using. |
| IgnoreBranches | Names of ROOT `TBranch` objects which are ignored during the inection step. You can add a list of TBranches or wildcards of branches here. |
| **Preprocessing** | |
| InputScaling | Determine procedure to scale inputs. Possible options are [minmax](https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.MinMaxScaler.html) minmax_symmetric or [standard](https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.StandardScaler.html) and `none`. If `minmax` is chosen inputs are scaled into a range between 0 and 1. If `standard` is chosen the input features are scaled by removing the mean and scaling to unit variance. If `none` is chosen no scaling is applied. Default is `minmax`. |
| RecalcScaler | Can be `True`(default) or `False`. If set to false the scaler (if present) is simply loaded and not re-calculated. This is useful if you want to run a different conversion but dont't want to change the scaling obtained from a previous configuration.|
| WeightScaling | Can be set to `True` of `False`. If set to `True` (default) weights are scaled such that the sum over all labels is normalised.|
| WeightsToUnity | Can be set to `True` of `False`. If set to `True` (default) weights are scaled such that the mean weight is 1.|
| TreatNegWeights | Can be set to `Scale` (removes negative weights and scales remaining weights down to account for difference, default), `SetZero` (sets all negative weights to zero) or `None` (negative weights stay untouched). Alternatively this can be set to `TakeAbs` in which case the absolute value of weights is taken. Users are encouraged to propose different weighting schemes and especially check which one is adequate for their individual case. In case the `Scale` option is chosen comparison plots are created with both the scaled and non-negative weight distributions.|
| Folds | Integer number which determines how many folds (k-folding) should be performed during training. All folds will be generated using the `eventNumber` variable within the ntuples. Hence you need to make sure that you have such a branch in your ntuples.|
| FoldSplitVariable | String representing the name of the variable which is used to determine which fold an event ends up being in. For this the modulo operator is applied to this variable. Default `eventNumber`.|
| **Cosmetics** | |
| ATLASLabel | Text that is printed behind the ATLAS label. E.g. "Internal". In case only MC is shown "Simulation" is automatically added in front of the string. If "None" is chosen, no ATLAS label is used.|
| CMLabel | CM label for the plots. *#sqrt{s}* is automatically added in front of this string.|
| CustomLabel | Custom text that is printed below the ATLAS label.|
| PlotFormat | List of file endings to specify the file format of the produced plots. Allowed options are `pdf`, `png`, `eps`, `jpeg`, `gif` and `svg`.|
| LargeCorrelation | Float value above which a correlation between variables is considered large (default 0.5). Based on this value an extra correlation matrix is drawn only containing large correlations. |
| RatioMax | Float to define the maximum value in ratio plots. Default is 1.55.|
| RatioMin | Float to define the minimum value in ratio plots. Default is 0.45.|
| LegendXMax | Float to define the maximum X value for the legend. Default is 0.95.|
| LegendXMin | Float to define the minimum X value for the legend. Default is 0.58.|
| LegendYMax | Float to define the maximum Y value for the legend. Default is 0.90.|
| LegendYMin | Float to define the minimum Y value for the legend. Default is 0.725.|
| NbinsSB2DPlots | Integer describing the number of bins for the 2D S/B and S/sqrt(B) plots along each axis. Default is 100.
| SoverBSF | Float scale factor which is used to determine the weight of the S/B contribution when calculating the S/B + S/sqrt(B) combination score. Default is 1.
| SoversqrtBSF | Float scale factor which is used to determine the weight of the S/sqrt(B) contribution when calculating the S/B + S/sqrt(B) combination score. Default is 1.
| DoCtrlPlots | Boolean deciding whether conversion control plots are produced Can be `True` or `False`. Default is `True`.|
| DoYields | Boolean deciding whether yields are drawn. Can be `True` or `False`. Default is `False`.|
| Blinding | Option (float value) to blind data. Any datapoint beyond a given threshold (e.g. 0.5) will be blinded in the plot.|
| SBBlinding | Option (float value) to blind data. Any datapoint in histograms with an S/B ratio larger than the given value will be blinded.|
| SignificanceBlinding | Option (float value) to blind data. Any datapoint in histograms with an significance, S/sqrt(B), larger than the given value will be blinded.|
| UnderFlowBin | Option (bool) to enable/disable the underflow bin. If set to `false` the content of the underflow bin is not added to the first bin. Default is `true`.|
| OverFlowBin | Option (bool) to enable/disable the overflow bin. If set to `false` the content of the overflow bin is not added to the last bin. Default is `true`.|
| LossPlotXScale | Can be `Linear` or `Log`. Determines the X-axis scale for Loss (and other metric) plots. |
| LossPlotYScale | Can be `Linear` or `Log`. Determines the Y-axis scale for Loss (and other metric) plots. |
| ConfusionPlotScale | Can be `Linear` or `Log`. Determines the Y-axis scale for binary confusion plots. |
| ConfusionMatrixNorm | Can be `row`, `column` or `None`. When `row` (`column`) is chosen, the rows (columns) of the confusion matrix are normalised to unity. If `None` is chosen, no normalisation is applied. |
| **Misc.** | |
| ROCSampling | Number of points to use in a ROC curve. Default is 100.|
| MinSBSignalYield | Float value. Defines the minimum percentage of the total signal yields for S/B 2D plots. Default is 0.3. The value has to be in (0.0,1.0). Corresponding plots are only produced for 3-class classifiers. |
| MinSBSignalYield | Float value. Defines the minimum percentage of the total signal yields for S/sqrt(B) 2D plots. Default is 0.3. The value has to be in (0.0,1.0). Corresponding plots are only produced for 3-class classifiers. |
| ComparisonTest | String determining which test is run to compare training and testing distributions. Can be `Kolmogorov-Smirnov` or `Anderson-Darling` (default).|

#### `VARIABLE` block options
| **Option** | **Effect** |
| ------ | ------ |
| Name | Name of the variable. |
| Label | Label of the variable. This label will be used in all plots. |
| Binning | Default ROOT-style binning, i.e. 10,0,1 is 10 bins from 0 to 1 |
| CustomBinning | Custom binning, i.e. user-defined edges: e.g. 0.1,0.4,0.6,0.7,0.8,1.0 |
| YMin | Minimum y-value of the y-axis. Default 0.|
| YMax | Minimum y-value of the y-axis.|
| Scale | Scale of the y-axis. Can be `Linear` or `Log`. Default is `Linear`.|
| Scaling | Function to apply a variable-specific scaling of values themselves. Can be `None`, `minmax`, `minmax_symmetric`, `standard` or `pT_scale`. `minmax`-scaling scales all values into the range [0,1], the symmetric version does the same but for [-1,1]. `standard`-scaling subtracts the mean of the distribution and divides by the standard deviation. `pT_scale`-scaling can be used for pT-based variables. For this scaling an event-based scaling is used where all variables of the same type (e.g. Jet) are taken into account and each entry is divided by the sum of all pT-objects of the same type. Afterwards the logarithm is taken.|
| ScalingRange | Tuple of two values (minimum, maximum). If given the scaling values for this variable are calculated using only values within the given range.|
| Type | Relevant parameter for some scaling options, e.g. `pT_scale`. This parameter is used to identify groups of objects that should be taken into account in scaling options that depend on other variables.|
| PaddingValue | Relevant for input variables that can have different sizes, e.g. vectors. If there is a chance that a given variable is not filled for all events, it can be "padded" using this option. Default value is `None`.|

#### `OUTPUT` block options
| **Option** | **Effect** |
| ------ | ------ |
| Name | Name of the output variable. |
| Label | Label of the output variable. This label will be used in all plots. |
| Binning | Default ROOT-style binning, i.e. 10,0,1 is 10 bins from 0 to 1 |
| CustomBinning | Custom binning, i.e. user-defined edges: e.g. 0.1,0.4,0.6,0.7,0.8,1.0 |
| Ymin | Minimum y-value of the y-axis. Default 0.|
| Ymax | Minimum y-value of the y-axis.|
| Scale | Scale of the y-axis. Can be `Linear` or `Log`. Default is `Linear`.|
| PermImportanceShuffles | Integer which determines how many shuffles per variable are performed to determine the mean permutation importance. Default is 5. |
| PermImportancePlotScale | Can be `Linear` or `Log`. Determines the X-axis scale for permutation importance plots. |

#### `GRAPHNode` block options
| **Option** | **Effect** |
| ------ | ------ |
| Name | Name of the node. |
| Type | String specifying the type of the node. `Global` is reserved for global features and can only occur once. |
| Features | List of `VARIABLE` objects (names) which are the features of the node. |
| Targets | List of `GRAPHNODE` names. These nodes are the targets of this particular node and define the connections (edges).|
| EdgeFeatures | List of `VARIABLE` objects (names) per given target, separated via `|`.|
| PruneIFValue | List of values corresponding to the length of `Features`. If any feature value is equal to any the respective given pruning value, the node is pruned.|

#### `SAMPLE` block options
| **Option** | **Effect** |
| ------ | ------ |
| Name | Name of the sample. This name will be used in all plots.|
| Type | Type of the sample. Can be `Signal`, `Background`, `Systematic`, or `Fake` (this is not referring to fake leptons).|
| Target | Integer describing the label used during training.|
| NtupleFiles | Comma-separated list of ntuple files. Remember that the parent path is already given in the setup script.|
| NtuplePath | Path to your directory holding your Ntuples. Can be relative or absolute. Will override `NtuplePath` in `GENERAL` block.|
| TreeName| Name of the TTree to be used for this sample. This option overrides the `TreeName` option in the `GENERAL` block.|
| Selection | Sample specific Python style selection string. This selection is combined with the `GENERAL Selection` using a logical `and`.|
| MCWeight | Sample specific Python style MCWeight string. This string is combined using `*` with the `GENERAL MCWeight`.|
| PenaltyFactor | Scalar value (float) that is multiplied to the training weight.|
| FillColor | Color value (integer) which is used to fill the corresponding Histogram in a stack plot. |
| ScaleToBkg | Can be `True` or `False`. Specifies whether the sample is scaled up/down w.r.t. the backgrounds. This only really makes sense for a signal sample.|

#### Generic Model Options
These options are supported in all models.

| **Option** | **Effect** |
| ------ | ------ |
| Name | Custom name of the model. This name needs to be unique. I.e. if you plan to inject several models you need to make sure the names of the models are unambiguous.|
| Type | Type of the model. Currently supported: `Classification-DNN`, `Classification-GNN`, `3LZ-Reconstruction`, `4LZ-Reconstruction`, `Regression-DNN`, and `Classification-WGAN` (WIP).|
| ValidationSize | Relative size of the validation set used during training. Default if 0.2.|
| Verbosity | Can be 0, 1 or 2. Defines the verbosity level of the training output. 0 represents the most silent option. For configs, that are supposed to run with HTCondor and are produced using the `Optimise` script the verbosity will be set to 1 to reduce the size of the output files.|
| GetONNXModel | Can be `True` or `False`. Defines whether a `.onnx` model is automatically created during the train step.|
| PreCompileModel | Can be `True` or `False`.  Triggers the latest method to speed up your PyTorch code! Specifically, `torch.compile` makes PyTorch code run faster by JIT-compiling PyTorch code into optimized kernels. Setting `PreCompileModel` to `True` will trigger this. Requires `pytorch>=2.0`.|

#### `DNNModel` block options

| **Option** | **Effect** |
| ------ | ------ |
| **Architecture related options** | |
| Nodes | Comma-separated list of neurons for each layer.|
| MaxNodes | Integer defining the maximum number of nodes in any layer of a DNN for hyperparameter optimisation. Default 100.|
| MinNodes | Integer defining the minimum number of nodes in any layer of a DNN for hyperparameter optimisation. Default 10.|
| StepNodes | Integer defining the stepsize between `MaxNodes` and `MinNodes` of a DNN for hyperparameter optimisation. Default 10.|
| MaxLayers | Integer defining the maximum number of layers of a DNN for hyperparameter optimisation. Default 5.|
| MinLayers | Integer defining the minimum number of layers of a DNN for hyperparameter optimisation. Default 1.|
| DropoutIndece | Layer indece at which [Dropout](https://keras.io/api/layers/regularization_layers/dropout/) layers are added. Only supported in a DNN.|
| DropoutProb | Probability of dropout. Default is 0.1.|
| BatchNormIndece | Layer indece at which [BatchNormalisation](https://keras.io/api/layers/normalization_layers/batch_normalization/) layers are added. Only supported in a DNN.|
| SetActivationFunctions | List of activation functions from which possible activation functions for the layers for hyperparameter optimisation are randomly drawn. The default only contains `ReLU`.|
| ActivationFunctions | [Activation function](https://keras.io/api/layers/activations/) in the hidden layers. Pass a list of activation functions with a length that corresponds to the number of hidden layers defined in 'Nodes'.|
| OutputActivation | [Activation function](https://keras.io/api/layers/activations/) in the output layer of a DNN.|
| **Learning related options** | |
| Epochs | Number of training epochs. Default is 100.|
| BatchSize | Batch size used in training. Default is 32.|
| LearningRate | Initial learning rate for the training. Default is 0.001.|
| MinLearningRate | Minimal learning rate used in the optimisation for the training. Default is 0.0001.|
| MaxLearningRate | Maximum learning rate used in the optimisation for the training. Default is 0.1.|
| Loss | Loss function which is used in the training. Currently supported: `binary_crossentropy`, `categorical_crossentropy`, `huber_loss`, `mean_squared_error`, `mean_absolute_error`, `mean_absolute_percentage_error`, `mean_squared_logarithmic_error`, `cosine_similarity`, `huber_loss`, `log_cosh`, [`categorical_focal_loss`](https://arxiv.org/abs/1708.02002), [`binary_focal_loss`](https://arxiv.org/abs/1708.02002).|
| Optimiser | String to defined the optimiser used during the training of a deep neural network. Allowed optimisers are: `Adadelta`, `Adagrad`, `Adam`, `Adamax`, `Ftrl, `Nadam`(Default), `RMSprop` and `SGD`.|
| Patience | Number of epochs with no improvement after which training will be stopped.|
| MinDelta | Minimum change in the monitored quantity to qualify as an improvement.|
| Metrics | Comma-separated list of metrics to be evaluated during training. Supported for a DNNM`|
| RestoreBest | Boolean, can be `True` or `False`. If set to `True` the best model is reconstructed at the end of the training. |
| ReweightTargetDistribution | Boolean, can be `True` or `False`. If set to `True` the target distribution (for a regression task) will be reweighted such that a flat distribution within the given binning is achieved. |

#### `GNNModel` block options

| **Option** | **Effect** |
| ------ | ------ |
| **Architecture related options** | |
| GraphStructure | Can be `homogeneous` or `heterogeneous`.|
| GraphLayers | List of graph convolutions which are applied to the graph data. Supported layers are: `GCNConv`, `GATConv`, `TransformerConv`, `SAGEConv`, and `GraphConv` for more information on these layers see [here](https://pytorch-geometric.readthedocs.io/en/latest/cheatsheet/gnn_cheatsheet.html).|
| GraphChannels | List of integer describing the number of channels per graph convolutional layer.|
| GraphActivationFunctions | List of strings describing the activation functions used after each graph convolutional layer. Supported are: `relu`, `sigmoid`, `tanh`, `elu`, `leaky_relu`.|
| GraphDropoutIndece | Graph layer indece at which [Dropout](https://keras.io/api/layers/regularization_layers/dropout/) layers are added. Only supported in a DNN.|
| GraphDropoutProb | Probability of dropout for each graph layer. Default is 0.1.|
| GlobalNodes | List of integer describing the number of nodes per hidden layer in the global network of a GNN.|
| GlobalActivationFunctions | List of strings describing the activation functions used after layer in the global network of a GNN. Supported are: `relu`, `sigmoid`, `tanh`, `elu`, `leaky_relu`.|
| FinalNodes | List of integer describing the number of nodes per hidden layer in the final fully connected network layers of a GNN.|
| FinalActivationFunctions | List of strings describing the activation functions used in the final fully connected network layers of a GNN. Supported are: `relu`, `sigmoid`, `tanh`, `elu`, `leaky_relu`.|
| OutputSize | Number of neurons in the output layer of a GNN.|
|**Learning related options** | |
| Epochs | Number of training epochs. Default is 100.|
| BatchSize | Batch size used in training. Default is 32.|
| LearningRate | Initial learning rate for the training. Default is 0.001.|
| Patience | Number of epochs with no improvement after which training will be stopped.|
| MinDelta | Minimum change in the monitored quantity to qualify as an improvement.|
| Optimiser | String to defined the optimiser used during the training of a deep neural network. Allowed optimisers are: `Adadelta`, `Adagrad`, `Adam`, `Adamax`, `Ftrl, `Nadam`(Default), `RMSprop` and `SGD`.|
| Loss | Loss function which is used in the training. Currently supported: `binary_crossentropy` and `categorical_crossentropy`.|
| RestoreBest | Boolean, can be `True` or `False`. If set to `True` the best model is reconstructed at the end of the training. |