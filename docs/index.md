# Documentation for mva-trainer

`mva-trainer` is a framework for the training of deep neural networks and decision trees. The code can be found on [GitLab](https://gitlab.cern.ch/skorn/mva-trainer). This website collects documentation and tutorials for the `mva-trainer` framework. The navigation is performed using the panel on the left.

This framework builds MVA-based models such as deep neural networks based on [PyTorch](https://pytorch.org/), [PyG](https://pytorch-geometric.readthedocs.io/en/latest/) and [scikit-learn](https://scikit-learn.org/stable/). Helper functions from [scikit-learn](https://scikit-learn.org/stable/) are used too but the heavy NN lifting is performed by PyTorch-based functions. The training of decision trees on the other hand is solely performed by scikit-learn. The current implementation uses the [GradientBoostingClassifier](https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.GradientBoostingClassifier.html) as implemented in scikit-learn.
For more information on the respective frameworks please visit the corresponding linked web pages.

The structure of the documentation on this web page is as follows:

* [Setup](Setup/file.md): Cloning of the script and general information about the setup
* [Settings](Settings/index.md): Detailed list of config settings which steer model training
* Instructions on how to run [DNNs](DNNs/RunDNN.md), and [GNNs](GNNs/RunGNN.md).
* Explanations on methods such as [k-folding](Methods/kfolding.md), [input scaling](Methods/inputscaling.md), and others
* A short walkthrough of the [conversion](Short_walkthrough/RunConversion.md), [training](Short_walkthrough/RunTraining.md), [evaluation](Short_walkthrough/RunEvaluation.md), and [injection](Short_walkthrough/RunInjection.md) steps.
* Instructions how to run the code using [virtual environments (e.g. on lxplus)](HowToRun/VirtualEnv.md), [Docker images](HowToRun/Docker.md) or [Singularity images (e.g. on lxplus)](HowToRun/SingularityLxplus.md)
* [FAQ](Settings/faq.md): List of frequently asked questions

The intention of this framework is to allow for an easy introduction into potent MVA-based techniques for HEP analyses. It removes the requirement to rewrite code from scratch and hence reduces the requred time analyses need to converge on the usage of MVA algorithms.

Contributions to this framework are always welcome.

# mva-trainer authors

Managers:

* Steffen Korn [steffen.korn@cern.ch](steffen.korn@cern.ch)

Additional helpers/managers are very welcome!
