#!/bin/sh

# Some messages to the user
echo "************************************************************************"
echo -e "SETUP\t This is the MVA-Trainer setup script"
echo -e "SETUP\t "
echo -e "SETUP\t You can find more information at https://gitlab.cern.ch/skorn/mva-trainer"
echo -e "SETUP\t The documentation can be found at https://mva-trainer-docs-site.docs.cern.ch/" 
echo "************************************************************************"
echo -e "SETUP\t Setting up environmental variables"

# Determine where this script is located (this is surprisingly difficult).
if [ "${BASH_SOURCE[0]}" != "" ]; then
    # This should work in bash.
    _src=${BASH_SOURCE[0]}
elif [ "${ZSH_NAME}" != "" ]; then
    # And this in zsh.
    _src=${(%):-%x}
elif [ "${1}" != "" ]; then
    # If none of the above works, we take it from the command line.
    _src="${1/setup.sh/}/setup.sh"
else
    echo -e "SETUP\t Failed to determine the MVA-Trainer base directory. Aborting ..."
    echo -e "SETUP\t Can you give the source script location as additional argument? E.g. with"
    echo -e "SETUP\t . ../foo/bar/scripts/setup.sh ../foo/bar/scripts"
    return 1
fi
echo "************************************************************************"
echo -e "SETUP\t Determining distribution we are currently running with..."

# Run cat /etc/os-release and save the output to a variable
os_release_content=$(cat /etc/os-release)

# Extract NAME and VERSION using grep and awk
name=$(grep -oP -m 1 'NAME="\K[^"]+' <<< "$os_release_content")
version=$(grep -oP -m 1 'VERSION="\K[^"]+' <<< "$os_release_content")

# Display the results
echo -e "SETUP\t Running distribution: $name"
echo -e "SETUP\t Version of distribution: $version"

echo -e "SETUP\t Determining MVA-Trainer version we are currently running with..."
git config --global --add safe.directory /builds/skorn/mva-trainer
MVATRAINERVERSION=$(git rev-parse HEAD)
if [ $? -eq 0 ]; then
    echo -e "SETUP\t You are running the following version of MVA-Trainer: $MVATRAINERVERSION"
else
    echo -e "SETUP\t Warning: Unable to retrieve MVA-Trainer version. Please ensure that Git is installed and the repository is properly set up."
fi

MVATRAINERCOMMIT=$(git log -1 --pretty=format:"%h %s (%cd)" --date=local)
if [ $? -eq 0 ]; then
    echo -e "SETUP\t You are running the following commit of MVA-Trainer: $MVATRAINERCOMMIT"
else
    echo -e "SETUP\t Warning: Unable to retrieve MVA-Trainer commit information. Please ensure that Git is installed and the repository is properly set up."
fi

# Set up MVA-Trainer environment variables that are picked up from the code.
export MVA_TRAINER_BASE_DIR="$(cd -P "$(dirname "${_src}")" && pwd)"

echo -e "SETUP\t Set up MVA_TRAINER_BASE_DIR in ${MVA_TRAINER_BASE_DIR}."

# Setting aliases
echo -e "SETUP\t Setting up aliases."

alias mvatrainer="python3 $MVA_TRAINER_BASE_DIR/python/mva-trainer.py"

echo -e "SETUP\t Checking ROOT version ..."
if command -v root > /dev/null 2>&1; then
    ROOT_VERSION=$(root-config --version)
    ROOT_PATH=$(root-config --prefix)
    ROOT_ARCH=$(root-config --arch)
    echo -e "SETUP\t ROOT is installed."
    echo -e "SETUP\t Version: $ROOT_VERSION"
    echo -e "SETUP\t Installation path: $ROOT_PATH"
    echo -e "SETUP\t Architecture: $ROOT_ARCH"
else
    echo -e "\e[31mERROR\t ROOT is not installed. Please install ROOT before running this script.\e[0m"
    return 1
fi

echo "************************************************************************"
# Determine whether Python3 is installed
echo -e "SETUP\t Checking Python3 version ..."
# Let's make sure we print the version...
pyv="$(python3 -V 2>&1)"
echo -e "\e[32mSETUP\t Detected Python3 version is $pyv \e[0m"
if ! hash python3; then
    echo -e '\e[31mERROR\t Python3 is not installed or can not be found.\e[0m'
    return 1
fi
# Ok, now that we now that we have Python3 let's check whether we also have the minimal version required (3.8)
ver=$((python3 -c 'import sys; print(sys.version_info[:][0]*100+sys.version_info[:][1])') 2>&1)
if [ "$ver" -lt "306" ]; then
    echo -e "\e[31mERROR\t Python3 3.6 or greater required!\e[0m"
    return 1
fi

# Determine whether requiered packages are installed. Cumbersome but necessary...
echo "************************************************************************"
echo -e "SETUP\t Checking python packages..."

installed_packages=$(pip freeze)
packages=("numpy" "pandas" "uproot" "torch" "torch_geometric" "tables" "pydot")

for package in "${packages[@]}"; do
    # Use grep to find the package in the list
    package_info=$(echo "$installed_packages" | grep -i "^$package==")
    echo -e "SETUP\t Checking for $package..."
    if [ $? -eq 0 ]; then
        # Extract and display the package version
        version=$(echo "$package_info" | awk -F'==' '{print $2}')
        echo -e "\e[32mSETUP\t $package is installed (version $version)!\e[0m"
    else
        echo -e "\e[31mERROR\t $package can not be found\e[0m"
	return 1
    fi
done

echo -e "SETUP\t Package check complete."

echo "************************************************************************"
echo -e "SETUP\t Configuration finished!"
echo -e "SETUP\t "
