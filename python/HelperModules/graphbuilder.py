"""Module to host 'graphbuilder' class. Here all the graph building/conversion
for Graph Neural Networks (GNNs) takes place.
"""

import numpy as np
import torch
from HelperModules.messagehandler import ErrorMessage
from torch_geometric.data import Data, HeteroData


class graphbuilder():
    """
    Class to build graphs.
    """

    def __init__(self, gnodes, graphstruct):
        self.m_gnodes = gnodes
        self.m_edgetypes = []  # for heterogeneous models
        self.m_map = {}
        self.m_inactivelist = []
        self.m_graphstruct = graphstruct

    ###########################################################################
    ######################## global attributes graphs #########################
    ###########################################################################

    def _nodeisactive(self, nodename):
        """Determine whether a given node is active

        :param nodename: Name of the node
        :returns:

        """
        return nodename not in self.m_inactivelist

    def _nodeisglobal(self, nodetype):
        """Determine whether a given node is a global node

        :param nodetype: Type of the node.
        :returns:

        """
        return nodetype.lower() == "global"

    def _prune_nodes(self, dfrow):
        """Method to prune nodes in a graph.
        This is done by checking if any value in the node equals the 'PruneIfValue' for that feature.
        The node is then deactivated which means that it is not taken into account in the graph building.

        :param dfrow: Pandas DataFrame row containing all node variables.
        :returns:

        """
        for node in self.m_gnodes:
            if node.get("PruneIfValue") is not None:
                if any(
                    dfrow[f] == v for f,
                    v in zip(
                        node.get("Features"),
                        node.get("PruneIfValue"))):
                    self.m_inactivelist.append(node.get("Name"))

    def _build_global(self, dfrow, data):
        """Build global features.

        :param dfrow: pandas DataFrame row
        :param data: PyTorch Data object
        :returns:

        """
        nglobal = 0
        for node in self.m_gnodes:
            if self._nodeisglobal(node.get("Type")) and self._nodeisactive(node.get("Name")):
                nglobal += 1
                data.u = torch.from_numpy(
                    np.array([np.array([dfrow[f] for f in node.get("Features")])]))
        if nglobal > 1:
            ErrorMessage("Found more than one GRAPHNODE block with type = Global!")

    ###########################################################################
    ########################### homogeneous graphs ############################
    ###########################################################################
    def _build_HomGNNnodes(self, dfrow):
        """Method to build the node feature matrix with shape [num_nodes, num_node_features].

        :param dfrow: Pandas DataFrame row containing all node variables.
        :returns: The feature matrices for all node types.

        """
        nodedata = []
        for node in self.m_gnodes:
            if not self._nodeisglobal(
                    node.get("Type")) and self._nodeisactive(
                    node.get("Name")):  # node not global and active
                nodedata.append(np.array([dfrow[f] for f in node.get("Features")]))
                self.m_map[node.get("Name")] = {"Idx": len(nodedata) - 1,
                                                "Node": node}
        try:
            return torch.from_numpy(np.array(nodedata))
        except TypeError as e:
            type_dict = {node.get("Name"): {f: (dfrow[f], type(dfrow[f]))
                                            for f in node.get("Features")} for node in self.m_gnodes}
            ErrorMessage(f"Conversion to torch tensor was not successful! Check: {type_dict}. {e}")
            return None

    def _build_HomGNNedges(self, dfrow):
        """Method to build:
        - the graph connectivity matrix (edge_index) for a homogeneous graph in COO format with shape [2, num_edges]
        - the edge feature matrix (edge_attr) for a homogeneous graph with shape [num_edges, num_edge_features]

        :param dfrow: Pandas DataFrame row containing all node variables.
        :returns: The graph connectivity matrix and the edge feature matrix.

        """
        edge_index = [np.array([]), np.array([])]
        edge_attr = []
        for node in self.m_gnodes:
            if not self._nodeisglobal(
                    node.get("Type")) and self._nodeisactive(
                    node.get("Name")):  # source node not global and active
                source = node.get("Name")
                for target_ID, target in enumerate(node.get("Targets")):
                    if self._nodeisactive(target) and not self._nodeisglobal(
                            self.m_map[target]["Node"].get("Type")):  # same for target
                        edge_index[0] = np.append(edge_index[0], self.m_map[source]["Idx"])
                        edge_index[1] = np.append(edge_index[1], self.m_map[target]["Idx"])
                        if node.get("EdgeFeatures"):
                            edge_attr.append(
                                np.array([dfrow[feature] for feature in node.get("EdgeFeatures")[target_ID]]))
        try:
            if edge_attr:
                return torch.from_numpy(
                    np.array(edge_index)).contiguous().long(), torch.from_numpy(
                    np.array(edge_attr))
            else:
                return torch.from_numpy(np.array(edge_index)).contiguous().long(), None
        except TypeError as e:
            ErrorMessage(f"Conversion to torch tensor was not successful! {e}")
            return None

    def _build_HomGraph(self, dfrow, y, w):
        """Method to build a homogeneous graph. The method used the private build methods for the nodes, edges and edge features.

        :param dfrow: Pandas DataFrame row containing all node variables.
        :param y: Target label of the graph. Default is None.
        :param w: Sample weight of the graph used in training to weight the loss. Default is 1.0.
        :returns: A data object describing a homogeneous graph. The data object can hold node-level, link-level and graph-level attributes.

        """
        self._prune_nodes(dfrow)
        data = Data()
        data.x = self._build_HomGNNnodes(dfrow)
        data.num_nodes = len(data.x)
        data.edge_index, data.edge_attr = self._build_HomGNNedges(dfrow)
        data.y, data.w = torch.from_numpy(y).float(), torch.from_numpy(w).float()
        self._build_global(dfrow, data)
        return data

    ###########################################################################
    ########################### heterogenous graphs ###########################
    ###########################################################################
    def _build_HetGNNnodes(self, dfrow, data):
        """Method to build the node feature matrix dictionary for a heterogeneous graph with shape {num_nodeTypes: [num_nodes, num_node_features]}.

        :param dfrow: Pandas DataFrame row containing all node variables.
        :param data: A data object describing a heterogeneous graph. The data object can hold node-level, link-level and graph-level attributes.
        :returns:

        """
        for node in self.m_gnodes:
            if not self._nodeisglobal(
                    node.get("Type")) and self._nodeisactive(
                    node.get("Name")):  # node not global and active
                nodeType = node.get("Type")
                if nodeType not in data.metadata()[0]:
                    data[nodeType].x = []
                data[nodeType].x.append([dfrow[f] for f in node.get("Features")])
                self.m_map[node.get("Name")] = {"Type": nodeType,
                                                "Idx": len(data[nodeType].x) - 1,
                                                "Node": node}
        nodetypes, _ = data.metadata()
        for nt in nodetypes:
            data[nt].x = torch.FloatTensor(data[nt].x)

    def _build_HetGNNedges(self, dfrow, data):
        """Method to build:
        - the graph connectivity matrix (edge_index) dictionary for a heterogeneous graph in COO format with shape {num_connection_types: [2, num_edges]}
        - the edge feature matrix (edge_attr) dictionary for a heterogeneous graph with shape {num_connection_types: [num_edges, num_edge_features]}

        :param dfrow: Pandas DataFrame row containing all node variables.
        :param data: A data object describing a heterogeneous graph. The data object can hold node-level, link-level and graph-level attributes.
        :returns:

        """
        for node in self.m_gnodes:
            if not self._nodeisglobal(
                    node.get("Type")) and self._nodeisactive(
                    node.get("Name")):  # source not is not global and active
                source = node.get("Name")
                sourceType = node.get("Type")
                for target_ID, target in enumerate(node.get("Targets")):
                    if not self._nodeisglobal(
                            self.m_map[target]["Node"].get("Type")) and self._nodeisactive(
                            self.m_map[target]["Node"].get("Type")):  # same for target
                        targetType = self.m_map[target]["Type"]
                        key = (sourceType, sourceType + "_to_" + targetType, targetType)
                        if key not in self.m_edgetypes:
                            self.m_edgetypes.append(key)
                        if key not in data.metadata()[1]:
                            data[key].edge_index = [
                                [self.m_map[source]["Idx"]], [self.m_map[target]["Idx"]]]
                            if node.get("EdgeFeatures"):
                                data[key].edge_attr = [[dfrow[feature]
                                                        for feature in node.get("EdgeFeatures")[target_ID]]]
                        else:
                            data[key].edge_index[0].append(self.m_map[source]["Idx"])
                            data[key].edge_index[1].append(self.m_map[target]["Idx"])
                            if node.get("EdgeFeatures"):
                                data[key].edge_attr.append(
                                    [dfrow[feature] for feature in node.get("EdgeFeatures")[target_ID]])
        for edgeType in data.metadata()[1]:
            data[edgeType].edge_index = torch.tensor(data[edgeType].edge_index, dtype=torch.long)
            if data[edgeType].edge_attr:
                data[edgeType].edge_attr = torch.tensor(data[edgeType].edge_attr, dtype=torch.float)
            else:
                data[edgeType].edge_attr = None

    def _build_Homglobal(self, dfrow):
        """Build global features for homogeneous GNN

        :param dfrow: Pandas DataFrame row
        :returns:

        """
        for node in self.m_gnodes:
            if self._nodeisglobal(
                    node.get("Type")) and self._nodeisactive(
                    node.get("Name")):  # node is global and active
                try:
                    return torch.from_numpy(
                        np.array([np.array([dfrow[f] for f in node.get("Features")])]))
                except TypeError as e:
                    ErrorMessage(
                        f"Conversion to torch tensor was not successful for node {node.get('Name')}! {e}")
        return None

    def _build_HetGraph(self, dfrow, y, w):
        """Method to build a heterogeneous graph. The method used the private build methods for the nodes, edges and edge features.

        :param dfrow: Pandas DataFrame row containing all node variables.
        :param y: Target label of the graph. Default is None.
        :param w: Sample weight of the graph used in training to weight the loss. Default is 1.0.
        :returns: A data object describing a heterogeneous graph. The data object can hold node-level, link-level and graph-level attributes.

        """
        self._prune_nodes(dfrow)
        data = HeteroData()
        self._build_HetGNNnodes(dfrow, data)
        self._build_HetGNNedges(dfrow, data)
        data.y = torch.from_numpy(y).float()
        data.w = torch.from_numpy(w).float()
        self._build_global(dfrow, data)
        return data

    def buildgraphs(self, x, y=None, w=None):
        """Wrapper function to build graphs

        :param x: Input data
        :param y: Targets
        :param w: Weights
        :returns: List of graphs

        """
        graphs = None
        if y is None:
            y = np.empty((len(x), 1))
            y[:] = np.nan
        if w is None:
            w = np.ones((len(x), 1))
        if self.m_graphstruct == "heterogeneous":
            graphs = [self._build_HetGraph(x_, y_, w_) for x_, y_, w_ in zip(x, y, w)]
        if self.m_graphstruct == "homogeneous":
            graphs = [self._build_HomGraph(x_, y_, w_) for x_, y_, w_ in zip(x, y, w)]
        return graphs
