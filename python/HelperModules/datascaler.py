"""Module handling scaling of the input variables

"""
import json
import numpy as np
import pandas as pd
from HelperModules.messagehandler import ErrorMessage


def getminmax(xin, xin_min, xin_max, maximum=1, minimum=0):
    """Function to determine scale and offset for minmax scaling

    :param xin: Input data in Pandas DataFrame format
    :param xin_min: Lower boundary, at which data is taken into account for the scale and offset value
    :param xin_max: Upper boundary, at which data is taken into account for the scale and offset value
    :param maximum: Maximum output value for the minmax scaling; Default is 1.
    :param minimum: Minimum output value for the minmax scaling; Default is 0.
    :returns: scale, offset

    """
    data = xin.where((xin >= xin_min) & (xin <= xin_max))
    return np.float64((maximum - minimum) / (max(data) - min(data))
                      ), np.float64(minimum - min(data) * (maximum - minimum) / (max(data) - min(data)))


def getstandard(xin, xin_min, xin_max):
    """Function to determine scale and offset for standard scaling

    :param xin: Input data in Pandas DataFrame format
    :param xin_min: Lower boundary, at which data is taken into account for the scale and offset value
    :param xin_max: Upper boundary, at which data is taken into account for the scale and offset value
    :returns: scale, offset

    """
    data = xin.where((xin >= xin_min) & (xin <= xin_max))
    return np.float64(1 / np.std(data)), np.float64(-np.mean(data) / np.std(data))


class scaler():
    """ Class to handle the scaling of the data

    """

    def __init__(self, cfgset):
        self.m_cfgset = cfgset
        self.m_scale_value_dict = {}

    def _gettypedict(self):
        """Creates a dictionary for each type to

        :returns:

        """
        typedict = {}
        for vname, optdict in self.m_scale_value_dict.items():
            if optdict["Option"] == "pT_scaling":
                if optdict["Type"] not in typedict:
                    typedict[optdict["Type"]] = [vname]
                else:
                    typedict[optdict["Type"]].append(vname)
        return typedict

    def load(self, fname):
        """Load scale information from file

        :param fname: file name
        :returns:

        """
        with open(f"{fname}", "r", encoding="utf-8") as j:
            self.m_scale_value_dict = json.load(j)

    def save(self, fname):
        """Save scaling information to file

        :param fname: file name
        :returns:

        """
        with open(f"{fname}", 'w', encoding="utf-8") as json_file:
            json.dump(self.m_scale_value_dict,
                      json_file,
                      sort_keys=True,
                      indent=4)

    def fit(self, xin):
        """TODO describe function

        :param xin: Input data in pandas DataFrame format
        :returns:

        """
        for variable in self.m_cfgset.get("VARIABLE"):
            if max(xin[variable.get("Name")]) == min(xin[variable.get("Name")]):
                ErrorMessage(
                    f"Input variable '{variable.get('Name')}' only contains {min(xin[variable.get('Name')])}. You need to check this!")
            scaleoption = variable.get("Scaling") if not variable.get(
                "Scaling") is None else self.m_cfgset.get("GENERAL").get("InputScaling")
            if scaleoption is None:
                scale = 1
                offset = 0

            if variable.get("ScalingRange") is not None:
                x_min = variable.get("ScalingRange")[0]
                x_max = variable.get("ScalingRange")[1]
            else:
                x_min = min(xin[variable.get("Name")].values)
                x_max = max(xin[variable.get("Name")].values)
            if scaleoption == "minmax":
                scale, offset = getminmax(xin[variable.get("Name")], xin_min=x_min, xin_max=x_max)
            elif scaleoption == "minmax_symmetric":
                scale, offset = getminmax(xin[variable.get("Name")],
                                          xin_min=x_min, xin_max=x_max, maximum=1, minimum=-1)
            elif scaleoption == "standard":
                scale, offset = getstandard(xin[variable.get("Name")], xin_min=x_min, xin_max=x_max)
            else:
                scale = 1.0
                offset = 0.0
            self.m_scale_value_dict[variable.get("Name")] = {"Option": scaleoption,
                                                             "Type": variable.get("Type"),
                                                             "Scale": scale,
                                                             "Offset": offset}

    def transform(self, xin):
        """TODO describe function

        :param xin: Input data in pandas DataFrame format
        :returns: scaled data

        """
        xout = pd.DataFrame()
        for vname in xin:
            if self.m_scale_value_dict[vname]["Option"] != "pT_scaling":
                xout[vname] = xin[vname] * self.m_scale_value_dict[vname]["Scale"] + \
                    self.m_scale_value_dict[vname]["Offset"]

        # Loop over self._gettypedict(). This gives a dictionary which the pairs
        # {pT_scaling_Type:[variables]} -> If no pT_scaling the dictionary is
        # empty
        for types in self._gettypedict():
            # Only scale the data, where the pT's are > 0, else it as seen as an
            # object which is not existent in the event
            pT_data = xin[self._gettypedict()[types]]
            denominater = pT_data.where(pT_data > 0).sum(axis=1).values
            xout[self._gettypedict()[types]] = np.log(xin[self._gettypedict()[types]].divide(
                denominater, axis=0), where=(xin[self._gettypedict()[types]].divide(denominater, axis=0) > 0))
        return xout

    def fittransform(self, xin):
        """Perform fitting and transformation using scaling information

        :param xin: Input data in Pandas DataFrame format
        :returns:

        """
        self.fit(xin)
        return self.transform(xin)
