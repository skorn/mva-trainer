"""
Module to handle the definition and reading of given options in the config file.
"""
import pickle
import re
from operator import gt, lt

from HelperModules.messagehandler import ErrorMessage


def boolify(s):
    """Function to convert True/true and False/false to booleans.

    :param s: Input string to be converted to boolean.
    :returns: Boolified string.
    :rtype: boolean

    """

    if s == 'True':
        return True
    if s == 'true':
        return True
    if s == 'False':
        return False
    if s == 'false':
        return False
    raise ValueError


def autoconvert(s):
    """Function to autoconvert a string into a boolean, int or float.

    :param s: Input string to be converted.
    :returns: Type-converted string
    :rtype: string, boolean, int or float

    """

    for fn in (boolify, int, float):
        try:
            return fn(s)
        except ValueError:
            pass
    return s


def prepstr(s):
    """Prepares the string. Searches for LaTeX escape characters and replaces
    them with the ones used in ROOT. Also extracts string delimeters,
    thereby conserving whitespaces.

    :param s: Input string.
    :returns: Cleaned output string.
    :rtype: string

    """
    if "\"" not in s and "'" not in s:
        s = s.replace(" ", "")
        return s.replace('\\', '#')
    s_1 = [_.start() for _ in re.finditer("\"", s)]  # strings with "
    s_2 = [_.start() for _ in re.finditer("'", s)]  # strings with '
    if len(s_1) == 2:
        return s[s_1[0] + 1:s_1[1]].replace('\\', '#')
    if len(s_2) == 2:
        return s[s_2[0] + 1:s_2[1]].replace('\\', '#')
    return None


class optcheck():
    """
    Class defining a check. A check consists of the a check function (fn)
    and its parameters (checkkwargs).
    The class serves as a simple container class to simplify the handling
    of checks for the option class.
    """

    def __init__(self, fn, **checkkwargs):
        self.fn = fn
        self.checkkwargs = checkkwargs

    def run(self):
        """Runner method. When called the check is actually performed.

        :returns: None

        """
        self.fn(**self.checkkwargs)


class option():
    """
    Class defining a config option.
    A config option consists of a name (oname), and a default value
    (odefault). Trough additional arguments the exact type of the
    option is specified. Such as list (islist) or nested list (isnlist).
    The checks list (m_checks) contains registered checks for the option.
    """

    def __init__(self, oname, odefault, **kwargs):
        self.m_name = oname
        self.m_ostr = None
        self.m_value = odefault
        self.m_kwargs = kwargs
        self.m_isset = False
        self.m_checks = []

    # Public
    def addcheck(self, fn, **kwargs):
        """Adding a check object to the check list.

        :param fn: Function to be used in the check.
        :returns: None

        """
        self.m_checks.append(optcheck(fn, **kwargs))

    def runchecks(self):
        """Class method to run all option checks.

        :returns: None

        """
        for c in self.m_checks:
            c.run()

    def getostr(self):
        """Getter method to retrieve the original option string

        :returns: Option string

        """
        return f"{self.m_name} = {self.m_ostr}"

    def setval(self, string):
        """Setter method to set the value of an option

        :param s: Input string containing the name of the option and its value.
        :returns: None

        """
        # First the string is converted
        string = " ".join(string.split())  # Removing whitespaces
        self.m_ostr = string
        if "islist" in self.m_kwargs and self.m_kwargs["islist"]:
            if string.count('"') >= 2:
                s = [prepstr(s) for s in string.split('"')[1::2]]
            elif string.count("'") >= 2:
                s = [prepstr(s) for s in string.split("'")[1::2]]
            else:
                s = [prepstr(s) for s in string.split(",")]
            val = [autoconvert(s) for s in s]
        elif "isnlist" in self.m_kwargs and self.m_kwargs["isnlist"]:
            if string.count('"') >= 2:
                s = [[prepstr(a) for a in s.split('"')[1::2]] for s in string.split("|")]
            elif string.count("'") >= 2:
                s = [[prepstr(a) for a in s.split("'")[1::2]] for s in string.split("|")]
            else:
                s = [[prepstr(a) for a in s.split(",")] for s in string.split("|")]
            val = [[autoconvert(sub_sub_s) for sub_sub_s in sub_s]
                   for sub_s in s]
        else:
            val = autoconvert(prepstr(string))
        self.m_value = val
        self.m_isset = True

    def getval(self):
        """Getter method for option values.

        :returns: value of option
        :rtype: Can be int, str, float, bool or list/list of list

        """

        return self.m_value

    def is_set(self):
        """Method to check whether an option was set by the user.

        :returns: True if option is set, False otherwise.

        """

        return self.m_isset

    def __str__(self):
        """Method returning the string representation of an option object.

        :returns: string representation of option object.
        :rtype: string

        """

        if isinstance(self.m_value, list):
            return f"Option: {self.m_name}\t Value: {','.join([str(val) for val in self.m_value])}"
        return f"Option: {self.m_name}\t Value: {str(self.m_value)}"


class optionhandler():
    """
    Class defining the definition and interaction of several options.
    An optionhandler gathers all possible options for a given config
    block and performs the checks on the option objects themselves.
    This class is also the interface for the rest of the code to get
    option values set in the config file.
    """

    def __init__(self, *args):
        if len(args) == 0:
            self.m_options = {}
            self.m_descr = "NONEGIVEN"
        if len(args) == 1:
            self.m_options = args[0].getopts()
            self.m_descr = "NONEGIVEN"

    # Private

    def _checkvalidity(self, ostr):
        """Private method to check whether a given option string contains a valid option.

        :param ostr: Option string, i.e. the full string describing the option.
        :returns: tuple(name, string) containing the option name and the option string.

        """
        try:
            name, string = ostr.split("=", 1)
        except ValueError as e:
            ErrorMessage(
                f"The option string '{ostr}' can not be properly split. You need to look at this! {e}")
        name = name.replace(" ", "")
        if name not in self.m_options.keys():
            ErrorMessage(
                f"'{name}' is not an allowed {self.m_descr} option!")
        return name, string

    def _is_inlist(self, oname, lst):
        """Check whether a given option defined by 'oname' is in a given list ('lst').

        :param oname: Name defining the option.
        :param lst: List to be checked against.
        :returns: None

        """
        if not self.m_options[oname].getval() in lst:
            ErrorMessage(
                f"{self.m_descr} has invalid option set for {oname}! Allowed options are {','.join(lst)}")

    def _is_subset(self, oname, lst):
        """Check whether a given option defined by 'oname' is in a subset of a list ('lst').

        :param oname: Name defining the option.
        :param lst: List to be checked against.
        :returns: None

        """
        if not set(self.m_options[oname].getval()).issubset(set(lst)):
            ErrorMessage(
                f"{self.m_descr} has invalid option set for {oname}! Must be subset of {lst}.")

    def _is_equal_len(self, oname, l):
        """Method to check whether 2 options are of equal length or one option has a specified length.

        :param oname: Name defining the option.
        :param l: Dedicated length to be checked against or string identyfing another option to check against its length.
        :returns: None

        """
        if isinstance(l, str):
            self._is_set(l)
            if isinstance(self.m_options[l].getval(), list):
                if len(
                        self.m_options[oname].getval()) != len(
                            self.m_options[l].getval()):
                    ErrorMessage(
                        f"Missmatching length between {oname}({len(self.m_options[oname].getval())}) and {l}({len(self.m_options[l].getval())})!")
            else:
                if len(self.m_options[oname].getval()
                       ) != self.m_options[l].getval():
                    ErrorMessage(
                        f"Missmatching length of {oname}({len(self.m_options[oname].getval())}) and {l}({self.m_options[l].getval()})!")
        elif len(self.m_options[oname].getval()) != l:
            ErrorMessage(
                f"Missmatching length of {oname}({len(self.m_options[oname].getval())})! Should be %{l}!")

    def _compsize(self, check, oname, thr):
        """Method to compare whether a given option is larger/smaller than a given threshold (thr)

        :param check: Can be 'smaller' or 'larger'. Determines whether '<' or '>' is used for comparison.
        :param oname: Name defining the option.
        :param thr: Threshold (float) to be checked against. This can also be another optioname to check against its value.
        :returns: None

        """
        if check == "larger":
            fn = gt
        elif check == "smaller":
            fn = lt
        if self.m_options[oname].is_set():
            if isinstance(
                    thr,
                    str) and not fn(
                    self.m_options[oname].getval(),
                    self.m_options[thr].getval()):
                ErrorMessage(
                    f"Expected {oname} to be {check} than {self.m_options[oname].getval()}!")
            elif not fn(self.m_options[oname].getval(), thr):
                ErrorMessage(
                    f"Expected {oname} to be {check} than {thr}!")

    def _is_set(self, oname):
        """Method to check whether a given option is set.

        :param oname: Name defining the option.
        :returns:

        """
        if isinstance(oname, list):
            if not any(self.m_options[name].is_set() for name in oname):
                ErrorMessage(
                    f"{self.m_descr} has needs at least one of {oname} set!")
        else:
            if not self.m_options[oname].is_set():
                ErrorMessage(
                    f"{self.m_descr} has no {oname} set!")

    # Public
    def getopts(self):
        """Private getter method to return the list of options.

        :returns: List of option objects.
        :rtype: List of option objects

        """
        return self.m_options

    def get(self, name):
        """Simple getter method to return the value of a given option.

        :param name: Name of the option.
        :returns: Value of the option
        :rtype: can be int, str, float, bool or list/list of list

        """
        try:
            return self.m_options[name].getval()
        except KeyError as e:
            ErrorMessage(
                f"The code could not read the option {name}. Check that strings are encapsuled using string delimeters and the option is valid. {e}")
            return None

    def setdescr(self, descr):
        """Sets the decription of the optionhandler object.
        This servers as an identifier of the command line output.
        This can (e.g.) be 'SAMPLE'.

        :param descr: String describing the optionhandler
        :returns: None

        """

        self.m_descr = descr

    def register(self, oname, odefault, **kwargs):
        """Method to add (register) an option to an optionhandler object

        :oname: name of the option
        :odefault: the default value of the option
        :returns:

        """
        self.m_options[oname] = option(oname=oname, odefault=odefault, **kwargs)

    def register_check(self, oname, except_case=None, case=None, **kwargs):
        """Method to add (register) an option check to an optionhandler object

        :oname: name of the option
        :except_case: don't run the check for these types of models
        :case: run the check for these types of models
        :returns:

        """
        if except_case is not None and self.m_options["Type"].getval(
        ) == except_case:
            return
        if case is not None and self.m_options["Type"].getval(
        ) != case:
            return
        oname = oname[0] if isinstance(
            oname, list) else oname
        if kwargs["check"] == "inlist":
            kwargs.pop("check")
            self.m_options[oname].addcheck(self._is_inlist, oname=oname, lst=kwargs["lst"])
        elif kwargs["check"] == "is_subset":
            kwargs.pop("check")
            self.m_options[oname].addcheck(self._is_subset, oname=oname, **kwargs)
        elif kwargs["check"] == "equal_len":
            kwargs.pop("check")
            self.m_options[oname].addcheck(self._is_equal_len, oname=oname, **kwargs)
        elif kwargs["check"] == "larger" or kwargs["check"] == "smaller":
            self.m_options[oname].addcheck(self._compsize, oname=oname, **kwargs)
        elif kwargs["check"] == "set":
            self.m_options[oname].addcheck(self._is_set, oname=oname)

    def check_options(self):
        """Perform all registered option checks.

        :returns:

        """

        for o in self.m_options.values():
            o.runchecks()

    def set_option(self, ostr):
        """Set the value of a given option using a string

        :param ostr: option string
        :returns:

        """
        name, string = self._checkvalidity(ostr)
        self.m_options[name].setval(string)

    def Print(self, fn):
        """Printing function

        :param fn: print function to be called (e.g. ErrorMessage or print)
        :returns:

        """

        fn(f"{self.m_descr} - Options:")
        for o in self.m_options.values():
            fn(f"{self.m_descr} - {o}")
        fn("--------------------------------------------")

    def save_todict(self, path):
        """Save options to dictionary (.json)

        :param path: Output path
        :returns:

        """
        with open(path, "wb") as f:
            pickle.dump(self.m_options, f)
