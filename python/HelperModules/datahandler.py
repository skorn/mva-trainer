"""Module handling the interaction with data stored in .h5 files and pandas DataFramesx

"""
import copy
from operator import eq, ne

import HelperModules.helperfunctions as hf
import numpy as np
import pandas as pd

from HelperModules.datascaler import scaler
from HelperModules.messagehandler import ErrorMessage, WarningMessage


class datahandler():
    """Class handling the storing and loading of variables to and from pandas DataFrames.

    """

    def __init__(self, cfgset):
        self.m_cfgset = cfgset
        self.m_data_sc = None
        self.m_data = self._getdata()
        self._addtmp()
        self._process()

    def _addtmp(self):
        """Adding temporary indeces to DataFrame

        :returns:

        """
        self.m_data["Index_tmp"] = np.arange(len(self.m_data))

    def _getdata(self):
        """Load and return merged.h5

        :returns: merged.h5

        """
        return pd.read_hdf(f"{hf.addslash(self.m_cfgset.get('DIR').get('Data'))}merged.h5")

    def _scalew(self, w, y):
        """Scake input weights

        :param w: weights
        :param y: targets
        :returns: scaled input weights

        """
        balance = (False if "Regression" in self.m_cfgset.get("MODEL").get(
            "Type") else self.m_cfgset.get("GENERAL").get("WeightScaling"))
        scale = self.m_cfgset.get("GENERAL").get("WeightsToUnity")
        if balance:
            unique_labels = np.unique(y[~np.isnan(y)])
            for i in unique_labels:
                w[y == i] /= np.sum(w[y == i])
        if scale:
            w[~np.isnan(y)] /= np.mean(w[~np.isnan(y)])
        return pd.DataFrame(data=w, columns=["Weight"])

    def _treatnegweights(self):
        """Handling negative weights

        :returns: Pandas DataFrame with negative weights treated.

        """
        w = copy.deepcopy(self.m_data["Weight"].values)
        n = self.m_data["Sample_Name"].values
        if self.m_cfgset.get("GENERAL").get("TreatNegWeights") == "TakeAbs":
            w = abs(w)
        if self.m_cfgset.get("GENERAL").get("TreatNegWeights") == "SetZero":
            w[w < 0] = 0
        elif self.m_cfgset.get("GENERAL").get("TreatNegWeights") == "Scale":
            unique_identifiers = np.unique(n)
            factors = {identifier: np.sum(
                w[n == identifier]) / np.sum(w[(w > 0) & (n == identifier)]) for identifier in unique_identifiers}
            w_corr = [factors[identifier] for identifier in n]
            w *= w_corr
            w[w < 0] = 0
        elif self.m_cfgset.get("GENERAL").get("TreatNegWeights") == "None":
            WarningMessage(
                "No treatment of negative weights is chosen. The training is potentially unstable.")
        return pd.DataFrame(data=w, columns=["Weight (treated)"])

    def _applypenalty(self, w, n):
        """Applying possibly penalty term

        :param w: weight
        :param n: sample name to apply penalty to
        :returns: Pandas DataFrame with adapted weights

        """
        penaltydict = {sample.get("Name"): sample.get("PenaltyFactor")
                       for sample in self.m_cfgset.get("SAMPLE")}
        for sample in self.m_cfgset.get("SAMPLE"):
            penaltydict[sample.get("Name")] = sample.get("PenaltyFactor")
        w_corr = w.values.T[0] * np.array([penaltydict[sname] for sname in n])
        return pd.DataFrame(data=w_corr, columns=["Weight"])

    def _process(self):
        """Processing a file to by applying the scaling of its inputs and weights

        :returns: Pandas DataFrame with scaled values

        """
        ntmp = copy.deepcopy(self.m_data["Sample_Name"])
        ftmp = copy.deepcopy(self.m_data[self.m_cfgset.get("GENERAL").get("FoldSplitVariable")])
        ytmp = copy.deepcopy(self.m_data["Label"])
        xtmp = copy.deepcopy(self.m_data[[v.get("Name") for v in self.m_cfgset.get("VARIABLE")]])
        isnominaltmp = copy.deepcopy(self.m_data["isNominal"])
        sc = scaler(self.m_cfgset)
        if self.m_cfgset.get("GENERAL").get("RecalcScaler"):
            xtmp = sc.fittransform(xtmp)
            sc.save(f"{hf.addslash(self.m_cfgset.get('DIR').get('Model'))}scaler.json")
        else:
            sc.load(f"{hf.addslash(self.m_cfgset.get('DIR').get('Model'))}scaler.json")
            xtmp = sc.transform(xtmp)
        wtreated = self._treatnegweights()  # We need the "treated" weights individually for neg. weight comparisons
        wtrain = self._scalew(w=np.copy(wtreated.values),
                              y=ytmp.values)
        wtrain = self._applypenalty(wtrain, ntmp)
        # TODO add regression scaling
        if "Regression" in self.m_cfgset.get("MODEL").get(
                "Type") and self.m_cfgset.get("MODEL").get("ReweightTargetDistribution"):
            wtrain["Weight"] *= self._reweighty(ytmp.values, wtrain["Weight"].values)
        self.m_data_sc = pd.concat([xtmp, ytmp, wtreated, wtrain, ntmp, ftmp, isnominaltmp], axis=1)
        self.m_data_sc["Index_tmp"] = np.arange(len(self.m_data_sc))

    def _reweighty(self, y_in, w_in):
        """Fills y_in into a numpy histogram using the weights w_in.
        Afterwards the histogram is "flattened" and the weights to
        produce this flattened histogram are returned.
        Usefull for regression tasks where mode-collapse can happen

        :param y_in: input targets
        :param w_in: input weights
        :returns: corrected weights

        """
        if len(self.m_cfgset.get("OUTPUT")) != 1:
            ErrorMessage("ReweightTargetDistribution is only supported if only 1 OUTPUT is defined")
        if self.m_cfgset.get("OUTPUT")[0].get("CustomBinning") is not None:
            binning = self.m_cfgset.get("OUTPUT")[0].get("CustomBinning")
        else:
            start = self.m_cfgset.get("OUTPUT")[0].get("Binning")[1]
            stop = self.m_cfgset.get("OUTPUT")[0].get("Binning")[2]
            num_bins = int(self.m_cfgset.get("OUTPUT")[0].get("Binning")[0] + 1)
            binning = np.linspace(start=start, stop=stop, num=num_bins)
        hist, bin_edges = np.histogram(a=y_in[~np.isnan(y_in)],
                                       bins=binning,
                                       weights=w_in[~np.isnan(y_in)])
        sfs = np.array([np.max(hist) / h if h > 0 else 1 for h in hist])
        indece = [ind - 1 for ind in np.digitize(y_in, bin_edges)]
        indece = [ind if ind != len(bin_edges) - 1 else len(bin_edges) - 2 for ind in indece]
        return sfs[indece]

    def addpredictions(self, df):
        """Method to add predictions do pandas DataFrame

        :param df: Pandas Dataframe to add predictions to.
        :returns:

        """
        self.m_data = pd.merge(self.m_data, df, how="inner", on=["Index_tmp"])

    def get(
            self,
            f,
            vnames=None,
            sname=None,
            return_NaNs=False,
            return_nomonly=True,
            test=False,
            scaled=True):
        """Getter function to access pandas Dataframes

        :param f: fold
        :param vnames: variable names
        :param sname: sample names
        :param return_NaNs: whether to also return non training data
        :param return_nomonly: whether to also return non nominal data
        :param test: boolean deciding whether we are interested in testing or training
        :param scaled: boolean steering whether the return values belong to the DataFrame with scaled values.
        :returns:

        """
        df = self.m_data_sc if scaled else self.m_data
        op = eq if test else ne
        lstring = []
        if sname is not None:
            lstring.append(df["Sample_Name"] == sname)
        if not return_NaNs:
            lstring.append(~np.isnan(df["Label"]))
        if return_nomonly:
            lstring.append(df["isNominal"])
        if f != "all":
            lstring.append(op(df[self.m_cfgset.get("GENERAL").get("FoldSplitVariable")] %
                           self.m_cfgset.get("GENERAL").get("Folds"), f))
        if vnames is None:
            return df[np.prod(lstring, axis=0, dtype=bool)]
        return df[np.prod(lstring, axis=0, dtype=bool)][vnames]
