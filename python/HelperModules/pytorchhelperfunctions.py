"""Collection of useful miscelleanous functions

"""

from torch.nn import (ELU, BatchNorm1d, Dropout, LeakyReLU, Linear, ReLU,
                      Sigmoid, Softmax, Tanh)
from torch_geometric.nn import Sequential


def gnnwrapper(data):
    """
    TODO describe function

    :param model:
    :param data:
    :returns:
    """
    if hasattr(data, 'x_dict'):
        datakeys = data.metadata()[0]
        batches = [data[key].batch for key in datakeys]
        return {"x_dict": data.x_dict,
                "edge_index_dict": data.edge_index_dict,
                "edge_attr_dict": data.edge_attr_dict,
                "u": data.u,
                "batches": batches,
                "datakeys": datakeys}  # Heterogeneous case
    else:
        if hasattr(data, 'u'):
            if hasattr(data, 'edge_attr'):
                return {"x": data.x,
                        "edge_index": data.edge_index,
                        "edge_attr": data.edge_attr,
                        "u": data.u,
                        "batch": data.batch}
            else:
                return {"x": data.x,
                        "edge_index": data.edge_index,
                        "u": data.u,
                        "batch": data.batch}
        else:
            if hasattr(data, 'edge_attr'):
                return {"x": data.x,
                        "edge_index": data.edge_index,
                        "edge_attr": data.edge_attr,
                        "batch": data.batch}
            else:
                return {"x": data.x,
                        "edge_index": data.edge_index,
                        "batch": data.batch}

def get_pytorch_act(act, dim=1):
    """Method to get PyTorch activation function based on string

    :param act:
    :returns:

    """
    if act == "sigmoid":
        return Sigmoid()
    if act == "softmax":
        return Softmax(dim=dim)
    if act == "tanh":
        return Tanh()
    if act == "leaky_relu":
        return LeakyReLU()
    if act == "elu":
        return ELU()
    return ReLU()


def buildgeomfc(
        in_nodes,
        hidden_nodes,
        hidden_act,
        out_nodes=None,
        out_act=None,
        dropoutindece=None,
        dropoutprob=0.3,
        batchnormindece=None,
        input_string='x'):
    """Method to create a fully connected (PyTorch) NN. We have to use this instead of the base version.

    :param in_nodes: Number of input nodes.
    :param hidden_nodes: List of integers defining the number of nodes per hidden layer.
    :param hidden_act: List of strings containing identifiers for activation functions in the hidden layers.
    :param out_nodes: Number of output nodes.
    :param out_act: String identifier for output activation function.
    :param dropoutindece: Indeces where to apply dropout
    :param dropoutprob: Probability of dropout
    :param batchnormindece: Indeces where to apply batch normalisation layers
    :param input_string: String to define the inputs for Sequential_geometric object.
    :returns: PyTorch Sequential object

    """
    fcmodel = []
    prevnodes = in_nodes
    layerid = 1
    if dropoutindece is None:
        dropoutindece = []
    if batchnormindece is None:
        batchnormindece = []
    for nodes, act in zip(hidden_nodes, hidden_act):
        fcmodel.append((Linear(prevnodes, nodes), f'{input_string} -> {input_string}'))
        fcmodel.append(get_pytorch_act(act))
        prevnodes = nodes
        if layerid in dropoutindece:
            fcmodel.append(Dropout(dropoutprob))
        if layerid in batchnormindece:
            fcmodel.append(BatchNorm1d(nodes))
        layerid += 1
    if out_nodes is not None:
        fcmodel.append((Linear(prevnodes, out_nodes), f'{input_string} -> {input_string}'))
    if out_act is not None:
        fcmodel.append(get_pytorch_act(out_act))
    return Sequential(f'{input_string}', fcmodel)
