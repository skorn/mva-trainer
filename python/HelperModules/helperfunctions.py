"""Collection of useful miscelleanous functions

"""

import os
import re

import awkward as ak
import numpy as np
import pandas as pd
import uproot
from sklearn.model_selection import train_test_split
from HelperModules.messagehandler import ErrorMessage


def get_vec_vars(string_list):
    """Function to detect vector variables including ':,[number]' in their names.
    The function then extracts the vector name itself

    :param string_list: List of input variable names
    :returns: List of output variable names (with cleaned vector variables)

    """
    # Regular expression pattern to match substrings followed by ':,[number]'
    pattern = r'(.+)(?=\[:,(\d+)\])'

    unique_subs = set()  # Set to store unique substrings
    normal_comps = []  # List to store normal components, i.e. without ':,[number]'
    highest_num = {}  # Dictionary to store highest [numbers] for each substring

    for string in string_list:
        matches = re.findall(pattern, string)  # Find all matches in the string
        if matches:
            for match in matches:
                sub, num = match  # Extract substring and number from the match
                unique_subs.add(sub)
                num = int(num)
                if sub not in highest_num or num > highest_num[sub]:
                    highest_num[sub] = num
        else:
            normal_comps.append(string)
    return list(unique_subs), normal_comps, highest_num


def save_load(fpath, tname, inputs, selection=None, entry_stop=None):
    """TODO describe function

    :param xuncut:
    :param cfgset:
    :returns:

    """
    if not os.path.exists(fpath):
        ErrorMessage(f"{fpath} does not seem to exist. You need to check this!")
    try:
        xuncut = uproot.open(f"{fpath}:{tname}")
    except FileNotFoundError as e:
        ErrorMessage(f"Could not find file {fpath}. {e.args[0]}")
    vnames = [v.get("Name") for v in inputs]
    vec_comps, normal_comps, vec_lengths = get_vec_vars(vnames)
    padval = {v.get("Name"): v.get("PaddingValue") for v in inputs}
    vector_inputs = []
    # Reading the vector components
    for vec_comp in vec_comps:
        events = xuncut.arrays(vec_comp,
                               cut=selection,
                               library="ak",
                               entry_stop=entry_stop)
        events_padded = ak.pad_none(events, target=vec_lengths[vec_comp] + 1, clip=True)
        # TODO fill with padding value
        padded = ak.to_dataframe(events_padded)
        padded.reset_index(inplace=True)
        padded = padded.pivot(index='entry', columns='subentry')
        c_names = []
        for i in range(vec_lengths[vec_comp] + 1):
            if f"{vec_comp}[:,{i}]" in vnames:
                c_names.append(f"{vec_comp}[:,{i}]")
        padded.columns = [f"{vec_comp}[:,{i}]" for i in range(vec_lengths[vec_comp] + 1)]
        padded = padded[c_names]  # take only the columns we are interested in
        for c_name in c_names:
            if padval[c_name] is None and padded[c_name].isnull().values.any():
                ErrorMessage(
                    f"Variable {c_name} has nan entries and no `PaddingValue` is specified!")
            padded[c_name] = padded[c_name].fillna(value=padval[c_name])
        vector_inputs.append(padded)
        x_vector = pd.concat(vector_inputs, axis=1)
    # Reading the scalar components
    if normal_comps:
        try:
            x_scalar = ak.to_dataframe(xuncut.arrays(normal_comps,
                                                     cut=selection,
                                                     library="ak",
                                                     entry_stop=entry_stop))
            x_scalar.reset_index(drop=True)
        except uproot.exceptions.KeyInFileError as e:
            ErrorMessage(
                f"The variable '{e.args[0]}' is missing in tree '{tname}' in file '{fpath}'. Consider using the 'IgnoreTreenames' option.")
    if len(vec_comps) == 0:
        x = x_scalar
    elif len(normal_comps) == 0:
        x = x_vector
    else:
        x = pd.concat([x_vector, x_scalar], axis=1)
    for variable in vnames:
        if x[variable].isnull().values.any():
            ErrorMessage(f"Variable {variable} has nan entries!")
    return x


def absfpaths(directory):
    """Method to get the absolute file paths w.r.t. a given directory path

    :param directory: path to directory
    :returns:

    """

    for dirpath, _, filenames in os.walk(directory):
        for f in filenames:
            yield os.path.abspath(os.path.join(dirpath, f))


def addslash(path):
    """
    Enusure a given path endes with a '/'.

    Parameters:
    path -- path to be checked

    returns path with '/' at the end.
    """
    if path[-1] == "/":
        return path
    return path + "/"


def char_isAllowed(c):
    """Function to check whether a char is an allowed char to be used in a filepath.

    :param c: input char
    :returns: Returns True if char c is allowed, False otherwise

    """
    return c.isalnum() or c in " _-.,[]()"


def filter_VarPathName(var):
    """Filters out illegal chars from variable names for filename usage

    For variable Ctrl-plots, some characters are illegal in the filename.
    This function replaces the illegal chars in `__pathCharReplDict` by
    the strings specified there and removes all other chars not allowed
    in `__char_isAllowed`.

    :param string: Variable name to be filtered by the rules outlined above.
    :returns: Filtered variable name suitable for filenames.

    """
    pathCharReplDict = {
        "/": "_DIV_",
        "*": "_TIMES_"
    }
    varpath = var
    # Replacing chars by their replacement for readability
    for char, repl in pathCharReplDict.items():
        varpath = varpath.replace(char, repl)

    # Removing other special characters
    varpath = ''.join([c for c in varpath if char_isAllowed(c)])
    varpath = varpath.replace(" ", "_")
    return varpath


def prepinputs(x, y, w, cfgset, library="np", randomstate=None):
    """Peparing inputs and separating them into train and test sets

    :param x: inputs data
    :param y: targets
    :param w: weights
    :returns: dictionary with train and validation sets

    """
    if randomstate is None:
        randomstate = np.random.randint(1000, size=1)[0]
    xtrain, xval, ytrain, yval = train_test_split(x, y, test_size=cfgset.get(
        "MODEL").get("ValidationSize"), random_state=randomstate)
    _, _, wtrain, wval = train_test_split(x,
                                          w,
                                          test_size=cfgset.get("MODEL").get("ValidationSize"),
                                          random_state=randomstate)
    if library == "pd":
        xtrain = pd.DataFrame(xtrain, columns=[v.get("Name") for v in cfgset.get("VARIABLE")])
        xval = pd.DataFrame(xval, columns=[v.get("Name") for v in cfgset.get("VARIABLE")])
        ytrain = pd.DataFrame(ytrain, columns=["Label"])
        yval = pd.DataFrame(yval, columns=["Label"])
        wtrain = pd.DataFrame(wtrain, columns=["Weight"])
        wval = pd.DataFrame(wval, columns=["Weight"])
    return {"xtrain": xtrain,
            "xval": xval,
            "ytrain": ytrain,
            "yval": yval,
            "wtrain": wtrain,
            "wval": wval}
