"""
Module to run the conversion of ntuples to hdf5 files.
"""
import glob
import os

import HelperModules.helperfunctions as hf
import numpy as np
import pandas as pd
import awkward as ak
import uproot
from HelperModules.messagehandler import (ConverterDoneMessage,
                                          ConverterMessage, ErrorMessage,
                                          WarningMessage)


def matched(string):
    """Function to check whether all brackets in string are matched.

    :param string: input string
    :returns: True of brackets are matched, False otherwise

    """

    count = 0
    for char in string:
        if char == "(":
            count += 1
        elif char == ")":
            count -= 1
            if count < 0:
                return False
    return count == 0


class conversionhandler():
    """
    Class handling the conversion of root files to pandas DataFrame objects.
    For this uproot is used.
    See here for more information on uproot: https://uproot.readthedocs.io/en/latest/basic.html
    """

    def __init__(self, cfgset):
        self.m_cfgset = cfgset
        self.m_df_all = {"x": [],
                         "y": [],
                         "weight": [],
                         "name": [],
                         "type": [],
                         "isnominal": [],
                         "foldsplit": []}
        self.m_metadata = {"tname": None,
                           "selection": "",
                           "mcweight": "",
                           "sanevariables": None,
                           "fpaths": [],
                           "emptyframes": 0,
                           "emptysamples": 0,
                           "nEvents": {},
                           "nEventsWeighted": {}}

    def _getfoldsplit(self, fpath, tname, selection):
        """Function to return a pandas DataFrame containing identifiers to identify the fold.

        :returns: pandas Dataframe

        """
        try:
            xuncut = uproot.open(f"{fpath}:{tname}")
            fsplitvar = xuncut.arrays(self.m_cfgset.get("GENERAL").get("FoldSplitVariable"),
                                      cut=None if selection == "" else selection,
                                      library="ak",
                                      entry_stop=self.m_cfgset.get("GENERAL").get("DoNOnly"))
            return ak.to_dataframe(fsplitvar)
        except FileNotFoundError as e:
            ErrorMessage(f"Could not find file {fpath}. {e.args[0]}")
        except KeyError as e:
            ErrorMessage(
                f"Could not find 'FoldSplitVariable' ({self.m_cfgset.get('GENERAL').get('FoldSplitVariable')}) in {fpath} : {tname}. {e.args[0]}")
        return None

    def _sizewarning(self, x, fpath):
        """Check whether a root file contributes an unusual amount of events.

        :param x: input data
        :param fpath: file path
        :returns:

        """
        if len(x) == 0:
            WarningMessage(f"No events added from {fpath}")
            self.m_metadata["emptyframes"] += 1
            return
        if len(x) < 50:
            WarningMessage(f"Less than 50 events added from {fpath}")
            return

    def _getx(self, fpath, tname, variables, selection):
        """Getter function for input variables.

        :param fpath: File path to root file
        :param tname: Tree name of TTree object within that root file
        :param variables: List of variables to be exctracted
        :param selection: Selection string obeying python logic
        :returns: Pandas DataFrame object containing input data

        """
        x = hf.save_load(fpath=fpath,
                         tname=tname,
                         inputs=variables,
                         selection=selection,
                         entry_stop=self.m_cfgset.get("GENERAL").get("DoNOnly"))
        self._sizewarning(x, fpath)
        return x

    def _gety(self, fpath, tname, selection, sample, ylen=None):
        """Getter function for targets.

        :param fpath: File path to root file
        :param tname: Tree name of TTree object within that root file
        :param variables: List of variables to be exctracted
        :param selection: Selection string obeying python logic
        :returns: Pandas DataFrame object containing targets:

        """
        if isinstance(sample.get("Target"), str):
            yuncut = uproot.open(f"{fpath}:{tname}")
            y = yuncut.arrays(sample.get("Target"),
                              cut=None if selection == "" else selection,
                              library="ak",
                              entry_stop=self.m_cfgset.get("GENERAL").get("DoNOnly"))
            y = ak.to_dataframe(y)
            return y.rename(columns={sample.get("Target"): "Label"})
        y = pd.DataFrame(data=np.array([sample.get("Target")] * ylen), columns=["Label"])
        y[y < 0] = np.nan
        return y

    def _getw(self, fpath, tname, selection, sample, wlen=None):
        """Getter function for weights

        :param fpath: File path to root file
        :param tname: Tree name of TTree object within that root file
        :param variables: List of variables to be exctracted
        :param selection: Selection string obeying python logic
        :returns: Pandas DataFrame object containing weights

        """
        if self.m_metadata["mcweight"] in ["", "1"] or sample.get("Type") == "Data":
            return pd.DataFrame(data=np.array([1.0] * wlen), columns=["Weight"])
        wuncut = uproot.open(f"{fpath}:{tname}")
        w = wuncut.arrays(self.m_metadata["mcweight"],
                          cut=None if selection == "" else selection,
                          library="ak",
                          entry_stop=self.m_cfgset.get("GENERAL").get("DoNOnly"))
        return ak.to_dataframe(w).rename(columns={self.m_metadata["mcweight"]: "Weight"})

    def _getname(self, sample, namelen=None):
        """Getter function for sample names.

        :param sample: optionhandler object describing an individual sample.
        :param namelen: default length to use for copying the sample name.
        :returns: Pandas DataFrame object containing sample names.

        """
        return pd.DataFrame(data=[str(sample.get("Name"))] * namelen, columns=["Sample_Name"])

    def _gettype(self, sample, typelen=None):
        """Getter function for sample names.

        :param sample: optionhandler object describing an individual sample.
        :param typelen: default length to use for copying the sample type.
        :returns: Pandas DataFrame object containing sample types.

        """
        return pd.DataFrame(data=[str(sample.get("Type"))] * typelen, columns=["Sample_Type"])

    def _getisnominal(self, sample, nomlen=None):
        """Getter function to determine whether a sample is a nominal sample

        :param sample: optionhandler object describing an individual sample.
        :param typelen: default length to use for copying the sample isnominal identifiers.
        :returns: Pandas DataFrame object containing isnominal identifiers.

        """

        if sample.get("Type") == "Data":
            isNominal = np.array([True] * nomlen)
        if sample.get("Type") == "Systematic":
            isNominal = np.array([False] * nomlen)
        else:
            isNominal = np.array([True] * nomlen)
        return pd.DataFrame(data=isNominal, columns=["isNominal"])

    def _initialise(self, sample):
        """Method to initialise a bunch of things based on information given in sample optionhandler object.

        :param sample: optionhandler object containing sample information
        :returns:

        """
        ConverterMessage(f"Processing {sample.get('Name')} ({sample.get('Type')})")
        ### Resetting DataFrames ###
        self.m_df_all = {k: [] for k in self.m_df_all}
        ### setting sample MC weight and selection ###
        ### Resetting some Variables ###
        if sample.get("Treename") is not None:
            self.m_metadata["tname"] = sample.get("Treename")
        else:
            self.m_metadata["tname"] = self.m_cfgset.get("GENERAL").get("Treename")
        self._setmcweight(sample)
        self._setselection(sample)
        self._checklogic()
        self.m_metadata["variables"] = self.m_cfgset.get("VARIABLE")
        ### get list of filepaths ###
        self.m_metadata["fpaths"] = []
        for fstring in sample.get("NtupleFiles"):
            if sample.get("NtuplePath") is None:
                lst = glob.glob(hf.addslash(self.m_cfgset.get("GENERAL").get("NtuplePath")) + fstring)
            else:
                lst = glob.glob(hf.addslash(sample.get("NtuplePath")) + fstring)
            if len(lst) == 0:
                ErrorMessage(
                    f"Could not find any file with pattern {fstring} in {hf.addslash(self.m_cfgset.get('GENERAL').get('NtuplePath'))}.")
            self.m_metadata["fpaths"].extend(lst)

        for fpath in self.m_metadata["fpaths"]:
            if not os.path.isfile(fpath):
                ErrorMessage(f"Could not open {fpath}. File does not exist")

    def _setmcweight(self, sample):
        """Sets the sample weight as a combiniation of the GENERAL MC weight and the sample specific MC weight.

        :param sample: optionhandler object containing sample information.
        :returns:

        """
        if sample.get("MCWeight") != "":
            self.m_metadata["mcweight"] = f"{self.m_cfgset.get('GENERAL').get('MCWeight')}*{sample.get('MCWeight')}"
        else:
            self.m_metadata["mcweight"] = self.m_cfgset.get("GENERAL").get("MCWeight")

    def _setselection(self, sample):
        """Sets the sample selection as a combiniation of the GENERAL selection and the sample specific selection.

        :param sample: optionhandler object containing sample information.
        :returns:

        """
        if sample.get("Selection") != "":
            if self.m_cfgset.get('GENERAL').get('Selection') == "":
                self.m_metadata["selection"] = f"({sample.get('Selection')})"
            else:
                self.m_metadata["selection"] = f"({self.m_cfgset.get('GENERAL').get('Selection')})&({sample.get('Selection')})"
        else:
            if self.m_cfgset.get("GENERAL").get("Selection") != "":
                self.m_metadata["selection"] = self.m_cfgset.get("GENERAL").get("Selection")
            else:
                self.m_metadata["selection"] = None

    def _checkmatched(self):
        """Checker function applying the matched method to the sample selection and mc weight.

        :returns:

        """
        if not self.m_metadata["selection"] is None and not matched(self.m_metadata["selection"]):
            ErrorMessage(f"Sample selection has unmatched brackets! {self.m_metadata['selection']}")
        if not self.m_metadata["mcweight"] is None and not matched(self.m_metadata["mcweight"]):
            ErrorMessage(f"Sample selection has unmatched brackets! {self.m_metadata['mcweight']}")

    def _checkcpp(self):
        """Checker function to look for possible c++ characters.

        :returns:

        """
        if not self.m_metadata["selection"] is None:
            if any(item in self.m_metadata["selection"] for item in ["@", "->", "^"]):
                ErrorMessage(
                    f"Found c++ style character in selection! {self.m_metadata['selection']}")
            if any(item in self.m_cfgset.get("GENERAL").get("MCWeight")
                   for item in ["@", "->", "^"]):
                ErrorMessage(f"Found c++ style character in weight! {self.m_metadata['mcweight']}")
            if "&&" in self.m_metadata["selection"]:
                ErrorMessage(
                    "Found c++ style '&&' in combined selection string. For a proper conversion please use (statement A)&(statement B) instead.")
            if "||" in self.m_metadata["selection"]:
                ErrorMessage(
                    "Found c++ style '||' in combined selection string. For a proper conversion please use (statement A)|(statement B) instead.")
            idx = self.m_metadata["selection"].find("!")
            if self.m_metadata["selection"] != "" and idx != - \
                    1 and self.m_metadata["selection"][idx + 1] != "=":
                ErrorMessage(
                    "Found c++ style not operator '!' in combined selection string. Please use != in a comparison instead.")
            while idx != -1:
                idx = self.m_metadata["selection"].find("!", idx + 1)
                if self.m_metadata["selection"][idx + 1] != "=" and idx != -1:
                    ErrorMessage(
                        "Found c++ style not operator '!' in combined selection string. Please use != in a comparison instead.")
        if not self.m_metadata["mcweight"] is None:
            if "&&" in self.m_metadata["mcweight"]:
                ErrorMessage(
                    "Found c++ style '&&' in combined MCWeight string. For a proper conversion please use (statement A)&(statement B) instead.")
            if "||" in self.m_metadata["mcweight"]:
                ErrorMessage(
                    "Found c++ style '||' in combined MCWeight string. For a proper conversion please use (statement A)|(statement B) instead.")
            idx = self.m_metadata["mcweight"].find("!")
            if idx != -1 and self.m_metadata["mcweight"][idx + 1] != "=":
                ErrorMessage(
                    "Found c++ style not operator '!' in combined MCWeight string. Please use != in a comparison instead.")
            while idx != -1:
                idx = self.m_metadata["mcweight"].find("!", idx + 1)
                if self.m_metadata["mcweight"][idx + 1] != "=" and idx != -1:
                    ErrorMessage(
                        "Found c++ style not operator '!' in combined MCWeight string. Please use != in a comparison instead.")

    def _checklogic(self):
        """Combining the c++ check and the matched check

        :returns:

        """
        self._checkmatched()
        self._checkcpp()

    def _finalise(self, sample):
        """Finalising the conversion of a sample by concatenating all pandas DataFrames.

        :param sample: optionhandler object containing sample information.
        :returns:

        """
        ConverterMessage(f"Finalising conversion for {sample.get('Name')} ({sample.get('Type')})")
        # Adding all temporary lists together
        for k, _ in self.m_df_all.items():
            if len(self.m_df_all[k]) == 0:
                continue
            self.m_df_all[k] = pd.concat([df for df in self.m_df_all[k] if len(df) > 0], copy=False)
            self.m_df_all[k] = self.m_df_all[k].reset_index(drop=True)
        ConverterMessage(f"Concatenating {sample.get('Name')} ({sample.get('Type')})")
        df_all = pd.concat(self.m_df_all.values(), axis=1, sort=False, copy=False)
        ConverterMessage(f"Writing {sample.get('Name')} ({sample.get('Type')})")
        df_all.to_hdf(
            f"{hf.addslash(self.m_cfgset.get('DIR').get('Data'))}{sample.get('Name')}.h5",
            key="df",
            mode="w")
        self.m_metadata["nEvents"][sample.get("Name")] = len(df_all["Weight"])
        self.m_metadata["nEventsWeighted"][sample.get("Name")] = np.sum(df_all["Weight"])
        ConverterMessage(f"Added {len(df_all):,} events from {sample.get('Name')} ({sample.get('Type')}).")
        ConverterDoneMessage(f"Processing {sample.get('Name')} ({sample.get('Type')}) DONE!")

    def _merge(self):
        """Simple function to merge the .h5 datasets which were created together into one big dataset containing everything.

        :returns:

        """
        if self.m_metadata["emptysamples"] == len(self.m_cfgset.get("SAMPLE")):
            ErrorMessage(
                "None of the given Samples has events passing the respective selection! You need to check this!")
        ConverterMessage("Merging datasets...")
        dfs = [
            pd.read_hdf(fname) for fname in [
                f"{hf.addslash(self.m_cfgset.get('DIR').get('Data'))}{s.get('Name')}.h5" for s in self.m_cfgset.get("SAMPLE")]]
        merged = pd.concat(dfs)
        # Shuffle the merged dataframes in place and reset index. Use random_state
        # for reproducibility
        seed = 1
        merged = merged.sample(frac=1, random_state=seed).reset_index(drop=True)
        ConverterMessage("Final MC event numbers:")
        common_keys = self.m_metadata["nEvents"].keys()
        max_key_length = max(map(len, common_keys))
        # Print the dictionaries in a tabular format
        ConverterMessage(f"{'Sample':<{max_key_length}} | {'Raw MC Events':<15} | {'Weighted MC Events':<15}")
        ConverterMessage('-' * (max_key_length + 38))  # Line separator
        for key in common_keys:
            ConverterMessage(f"{key:<{max_key_length}} | {self.m_metadata['nEvents'][key]:<15} | {self.m_metadata['nEventsWeighted'][key]:.2f}")

        merged.to_hdf(
            f"{hf.addslash(self.m_cfgset.get('DIR').get('Data'))}merged.h5",
            key="df",
            mode="w")
        ConverterDoneMessage(
            f"Merged hdf5 file (data) written to {hf.addslash(self.m_cfgset.get('DIR').get('Data'))}merged.h5")

    def convert(self):
        """Perform the conversion of a given file.

        :returns:

        """

        for sample in self.m_cfgset.get("SAMPLE"):
            self._initialise(sample)
            for fpath in self.m_metadata["fpaths"]:
                self.m_df_all["x"].append(self._getx(fpath=fpath,
                                                     tname=self.m_metadata["tname"],
                                                     variables=self.m_metadata["variables"],
                                                     selection=self.m_metadata["selection"]))
                l = len(self.m_df_all["x"][-1])
                self.m_df_all["y"].append(self._gety(fpath=fpath,
                                                     tname=self.m_metadata["tname"],
                                                     selection=self.m_metadata["selection"],
                                                     sample=sample,
                                                     ylen=l))
                self.m_df_all["weight"].append(self._getw(fpath=fpath,
                                                          tname=self.m_metadata["tname"],
                                                          selection=self.m_metadata["selection"],
                                                          sample=sample,
                                                          wlen=l))
                self.m_df_all["name"].append(self._getname(sample=sample,
                                                           namelen=l))
                self.m_df_all["type"].append(self._gettype(sample=sample,
                                                           typelen=l))
                self.m_df_all["isnominal"].append(self._getisnominal(sample=sample,
                                                                     nomlen=l))
                self.m_df_all["foldsplit"].append(
                    self._getfoldsplit(
                        fpath=fpath,
                        tname=self.m_metadata["tname"],
                        selection=self.m_metadata["selection"]))
            self._finalise(sample)
        self._merge()
