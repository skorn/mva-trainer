"""
Module for directories files are saved in.
"""
import os

from HelperModules.helperfunctions import addslash


def delete_trailing_slash(path):
    """
    :param path: path to be checked
    :returns: path without '/' at the end.

    """

    if path[-1] == "/":
        return path[:-1]
    return path


def create_output_dir(path):
    """Create an output directory (if it does not exit yet) using the path argument.

    :param path: path to the directory

    """
    if os.path.isdir(delete_trailing_slash(path)):
        return
    os.makedirs(delete_trailing_slash(path))


class directoryhandler():
    """
    This class defines output directories
    """

    def __init__(self, output_path):
        self.m_dirdict = {
            "Main": output_path,
            "Plots": addslash(output_path) + "Plots",
            "MVAPlots": addslash(output_path) + "Plots/MVAPlots",
            "CtrlPlots": addslash(output_path) + "Plots/CtrlPlots",
            "CtrlDataMC": addslash(output_path) + "Plots/CtrlPlots/DataMC",
            "CtrlDataMC_scaled": addslash(output_path) + "Plots/CtrlPlots/DataMC_scaled",
            "TrainStats": addslash(output_path) + "Plots/CtrlPlots/TrainStats",
            "CtrlMisc": addslash(output_path) + "Plots/CtrlPlots/Misc",
            "Separation": addslash(output_path) + "Plots/CtrlPlots/Separation",
            "CtrlTrainTest": addslash(output_path) + "Plots/CtrlPlots/TrainTest",
            "Data": addslash(output_path) + "Data",
            "Tables": addslash(output_path) + "Tables",
            "Model": addslash(output_path) + "Model",
            "Configs": addslash(output_path) + "Configs"}

    def get(self, d):
        """Get a directory path

        :param d: key of m_dirdict
        :returns: directory path

        """
        create_output_dir(self.m_dirdict[d])
        return self.m_dirdict[d]

    def Print(self, fn):
        """Print function

        :param fn: function to use for printing
        :returns:

        """
        for k, v in self.m_dirdict.items():
            fn(f"Directories: {k} - {v}")
