"""Module handling the interactions with histograms

"""
import numpy as np
from ROOT import TH1D


class histohandler():
    """Class handling the interactions with histograms

    """

    def __init__(self, cfgset):
        self.m_cfgset = cfgset
        self.m_histos = {}

    def addTH1F(self, sample, hname, binning, xvals, weights=None):
        """Adding a TH1F to the histohandler objects m_histos dictionary.

        :param hname: name of the histogram
        :param binning: binning of the histogram
        :param xvals: xvalues to be filled into the histogram
        :param weights: weights
        :returns:

        """
        if hname in self.m_histos:
            return
        weights = [1.0] * len(xvals) if weights is None else weights
        self.m_histos[hname] = TH1D(hname, "", int(len(binning) - 1), binning)
        if not sample.get("Type") == "Data" and not sample.get("FillColor") is None:
            self.m_histos[hname].SetFillColor(sample.get("FillColor"))
            self.m_histos[hname].SetLineWidth(0)
        for xval, weight in zip(xvals, weights):
            self.m_histos[hname].Fill(xval, weight)
        # underflow bin
        self.m_histos[hname].SetBinContent(
            1,
            self.m_histos[hname].GetBinContent(1) +
            self.m_histos[hname].GetBinContent(0))
        self.m_histos[hname].SetBinError(
            1,
            np.sqrt(
                self.m_histos[hname].GetBinError(1)**2 +
                self.m_histos[hname].GetBinError(0)**2))
        self.m_histos[hname].SetBinContent(0, 0)
        self.m_histos[hname].SetBinError(0, 0)
        # overflow bin
        self.m_histos[hname].SetBinContent(int(len(binning) -
                                               1), self.m_histos[hname].GetBinContent(int(len(binning) -
                                                                                          1)) +
                                           self.m_histos[hname].GetBinContent(int(len(binning))))
        self.m_histos[hname].SetBinError(int(len(binning) -
                                             1), np.sqrt(self.m_histos[hname].GetBinError(int(len(binning) -
                                                                                              1)) ** 2 +
                                                         self.m_histos[hname].GetBinError(int(len(binning)))**2))
        self.m_histos[hname].SetBinContent(int(len(binning)), 0)
        self.m_histos[hname].SetBinError(int(len(binning)), 0)

    def get(self, hname):
        """Getter function to get histogram from dictionary.

        :param hname: name of histogram
        :returns:

        """
        return self.m_histos[hname]
