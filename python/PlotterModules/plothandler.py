"""Module handling plotting
"""
import json
import warnings
import copy
from array import array

import HelperModules.helperfunctions as hf
import numpy as np
import pandas as pd
import PlotterModules.plots as plt
import ROOT
from HelperModules.datahandler import datahandler
from HelperModules.histohandler import histohandler
from HelperModules.messagehandler import InfoMessage
from ModelModules.modelhandler import modelhandler
from ROOT import TH1D, TH2D, THStack, gROOT
from sklearn.metrics import confusion_matrix


class plothandler():
    """Class handling the generation of plots.

    """

    def __init__(self, cfgset):
        # Tell ROOT to use Atlas style:
        ROOT.gROOT.SetStyle("ATLAS")
        self.m_cfgset = cfgset
        self.m_dhandler = datahandler(cfgset)
        self.m_hhandler = histohandler(cfgset)
        self.m_mhandler = modelhandler(cfgset)
        self.m_binningdict = self._getbinning()
        gROOT.SetBatch()
        ROOT.gErrorIgnoreLevel = ROOT.kWarning

    # private methods

    def _getbinning(self):
        """Getter function to determine the ROOT-style binning of a variable.

        :returns:

        """
        binning = {}
        for var in self.m_cfgset.get("VARIABLE"):
            name = var.get("Name")
            if var.get("CustomBinning") is not None:
                binning[name] = array("d", var.get("CustomBinning"))
            else:
                start = var.get("Binning")[1]
                stop = var.get("Binning")[2]
                num_bins = int(var.get("Binning")[0] + 1)
                binning[name] = array("d", np.linspace(start=start, stop=stop, num=num_bins))
        for output in self.m_cfgset.get("OUTPUT"):
            name = output.get("Name")
            if output.get("CustomBinning") is not None:
                binning[name] = array("d", output.get("CustomBinning"))
                # same binning for actual target distribution
                binning["Label"] = array("d", output.get("CustomBinning"))
            else:
                start = output.get("Binning")[1]
                stop = output.get("Binning")[2]
                num_bins = int(output.get("Binning")[0] + 1)
                binning[name] = array("d", np.linspace(start=start, stop=stop, num=num_bins))
                # same binning for actual target distribution
                binning["Label"] = array("d", np.linspace(start=start, stop=stop, num=num_bins))

        return binning

    def _getpred(self, verbose=True):
        """Getter functions for predictions

        :returns:

        """
        if verbose:
            InfoMessage("Calculating Predictions...")
        dfs = []
        for fold in range(self.m_cfgset.get("GENERAL").get("Folds")):
            vnames = [v.get("Name") for v in self.m_cfgset.get("VARIABLE")]
            xin = self.m_dhandler.get(fold,
                                      vnames=vnames,
                                      test=True,
                                      return_NaNs=True,
                                      scaled=True)
            tmpidx = self.m_dhandler.get(fold,
                                         vnames=["Index_tmp"],
                                         test=True,
                                         return_NaNs=True,
                                         scaled=False).reset_index(drop=True)
            df = pd.concat([self.m_mhandler.predict(xin, fold), tmpidx], axis=1)
            dfs.append(df)
        self.m_dhandler.addpredictions(pd.concat(dfs))

    # public methods

    def _fillctrlhistos(self):
        """Fill all CTRL plot histograms (after the conversion)

        :returns:

        """
        InfoMessage("Filling control plots...")
        for variable in self.m_cfgset.get("VARIABLE"):
            for sample in self.m_cfgset.get("SAMPLE"):
                InfoMessage(f"Filling control plots for variable {variable.get('Name')} in sample {sample.get('Name')}...")
                # input variables combined for all folds
                all_vals = self.m_dhandler.get(f="all",
                                               vnames=[variable.get("Name"), "Weight"],
                                               sname=sample.get("Name"),
                                               return_NaNs=True,
                                               return_nomonly=True,
                                               test=True,
                                               scaled=False)
                self.m_hhandler.addTH1F(hname=f"{sample.get('Name')}_{variable.get('Name')}_all_test",
                                        sample=sample,
                                        binning=self.m_binningdict[variable.get("Name")],
                                        xvals=all_vals[variable.get("Name")].values,
                                        weights=all_vals["Weight"].values)
                # with scaled weights
                weights_sc = self.m_dhandler.get(f="all",
                                                 vnames=["Weight"],
                                                 sname=sample.get("Name"),
                                                 return_NaNs=True,
                                                 return_nomonly=True,
                                                 test=True,
                                                 scaled=True)
                weights_pos = weights_sc.copy()
                weights_pos[weights_pos["Weight"] < 0] = 0
                self.m_hhandler.addTH1F(hname=f"{sample.get('Name')}_{variable.get('Name')}_all_test-wtreated",
                                        sample=sample,
                                        binning=self.m_binningdict[variable.get("Name")],
                                        xvals=all_vals[variable.get("Name")].values,
                                        weights=weights_sc["Weight"].values)
                self.m_hhandler.addTH1F(hname=f"{sample.get('Name')}_{variable.get('Name')}_all_test-wpos",
                                        sample=sample,
                                        binning=self.m_binningdict[variable.get("Name")],
                                        xvals=all_vals[variable.get("Name")].values,
                                        weights=weights_pos["Weight"].values)
                InfoMessage(f"Filling control plots for variable {variable.get('Name')} in sample {sample.get('Name')} for individual folds...")
                for fold in range(self.m_cfgset.get("GENERAL").get("Folds")):
                    # input variables training
                    train_vals = self.m_dhandler.get(f=fold,
                                                     vnames=[variable.get("Name"), "Weight"],
                                                     sname=sample.get("Name"),
                                                     return_NaNs=True,
                                                     return_nomonly=True,
                                                     test=False,
                                                     scaled=False)
                    self.m_hhandler.addTH1F(hname=f"{sample.get('Name')}_{variable.get('Name')}_{fold}_train",
                                            sample=sample,
                                            binning=self.m_binningdict[variable.get("Name")],
                                            xvals=train_vals[variable.get("Name")].values,
                                            weights=train_vals["Weight"].values)
                    # input variables testing
                    test_vals = self.m_dhandler.get(f=fold,
                                                    vnames=[variable.get("Name"), "Weight"],
                                                    sname=sample.get("Name"),
                                                    return_NaNs=True,
                                                    return_nomonly=True,
                                                    test=True,
                                                    scaled=False)
                    self.m_hhandler.addTH1F(hname=f"{sample.get('Name')}_{variable.get('Name')}_{fold}_test",
                                            sample=sample,
                                            binning=self.m_binningdict[variable.get("Name")],
                                            xvals=test_vals[variable.get("Name")].values,
                                            weights=test_vals["Weight"].values)

    def _fillevalhistos(self):
        """Fill all evaluation plot histograms

        :returns:

        """
        for output in self.m_cfgset.get("OUTPUT"):
            for sample in self.m_cfgset.get("SAMPLE"):
                # output variables combined for all folds
                all_vals = self.m_dhandler.get(f="all",
                                               vnames=[output.get("Name"), "Weight"],
                                               sname=sample.get("Name"),
                                               return_NaNs=True,
                                               return_nomonly=True,
                                               test=True,
                                               scaled=False)
                self.m_hhandler.addTH1F(hname=f"{sample.get('Name')}_{output.get('Name')}_all_test",
                                        sample=sample,
                                        binning=self.m_binningdict[output.get("Name")],
                                        xvals=all_vals[output.get("Name")].values,
                                        weights=all_vals["Weight"].values)
                for fold in range(self.m_cfgset.get("GENERAL").get("Folds")):
                    # output variables training
                    # these we manually need to evaluate. I.e. we need scaled x-values and
                    # unscaled weights!
                    vnames = [v.get("Name") for v in self.m_cfgset.get("VARIABLE")]
                    train_vals_input = self.m_dhandler.get(f=fold,
                                                           vnames=vnames,
                                                           sname=sample.get("Name"),
                                                           return_NaNs=True,
                                                           return_nomonly=True,
                                                           test=False,
                                                           scaled=True)
                    train_vals_weight = self.m_dhandler.get(f=fold,
                                                            vnames=["Weight"],
                                                            sname=sample.get("Name"),
                                                            return_NaNs=True,
                                                            return_nomonly=True,
                                                            test=False,
                                                            scaled=False)
                    y_pred = self.m_mhandler.predict(train_vals_input, fold)[output.get("Name")]
                    self.m_hhandler.addTH1F(hname=f"{sample.get('Name')}_{output.get('Name')}_{fold}_train",
                                            sample=sample,
                                            binning=self.m_binningdict[output.get("Name")],
                                            xvals=y_pred,
                                            weights=train_vals_weight["Weight"].values)
                    # output variables testing
                    # these are easy to get as we have already calculated them
                    test_vals = self.m_dhandler.get(f=fold,
                                                    vnames=[output.get("Name"), "Weight"],
                                                    sname=sample.get("Name"),
                                                    return_NaNs=True,
                                                    return_nomonly=True,
                                                    test=True,
                                                    scaled=False)
                    self.m_hhandler.addTH1F(hname=f"{sample.get('Name')}_{output.get('Name')}_{fold}_test",
                                            sample=sample,
                                            binning=self.m_binningdict[output.get("Name")],
                                            xvals=test_vals[output.get("Name")].values,
                                            weights=test_vals["Weight"].values)

    def doratioplots(self, variable):
        """Method to perform ratioplot for optionhandler object containing variable information.

        :param variable: optionhandler object containing variable information.
        :returns:

        """
        stacks = {"mc": THStack(),
                  "sig": THStack(),
                  "bkg": THStack()}
        has = {"sig": False,
               "bgk": False,
               "data": False}
        dhist = None
        for sample in self.m_cfgset.get("SAMPLE"):
            hname = f"{sample.get('Name')}_{variable.get('Name')}_all_test"
            if sample.get("Name") == "Data":
                dhist = self.m_hhandler.get(hname)
                has["data"] = True
            else:
                mchist = self.m_hhandler.get(hname)
                if sample.get("Type") == "Signal":
                    stacks["sig"].Add(mchist)
                    has["sig"] = True
                if sample.get("Type") == "Background":
                    stacks["bkg"].Add(mchist)
                    has["bkg"] = True
                stacks["mc"].Add(mchist)
        # apply blinding
        blindingindece = []
        if any(
            i is not None for i in [
                self.m_cfgset.get("GENERAL").get("SBBlinding"),
                self.m_cfgset.get("GENERAL").get("SignificanceBlinding"),
                self.m_cfgset.get("GENERAL").get("Blinding")]) and has["sig"] and has["bkg"] and has["data"]:
            s_hist = stacks["sig"].GetStack().Last().Clone("s_hist")
            b_hist = stacks["bkg"].GetStack().Last().Clone("b_hist")
            for bin_i in range(stacks["mc"].GetStack().Last().GetNbinsX() + 1):
                if s_hist.GetBinContent(bin_i) == 0 and b_hist.GetBinContent(bin_i) == 0:
                    continue
                if s_hist.GetBinContent(bin_i) > 0 and b_hist.GetBinContent(bin_i) <= 0:
                    blindingindece.append(bin_i)
                if (
                    self.m_cfgset.get("GENERAL").get("SBBlinding") is not None and s_hist.GetBinContent(bin_i) /
                    b_hist.GetBinContent(bin_i) > self.m_cfgset.get("GENERAL").get("SBBlinding") or (
                        self.m_cfgset.get("GENERAL").get("SignificanceBlinding") is not None and s_hist.GetBinContent(bin_i) /
                        np.sqrt(
                            b_hist.GetBinContent(bin_i)) > self.m_cfgset.get("GENERAL").get("SignificanceBlinding")) or (
                        self.m_cfgset.get("GENERAL").get("Blinding") is not None and s_hist.GetBinCenter(bin_i) > self.m_cfgset.get("GENERAL").get("Blinding"))):
                    blindingindece.append(bin_i)
        rp = plt.ratioplot(
            stacks["mc"],
            dhist,
            plotname="Ratio Plot",
            doyields=self.m_cfgset.get("GENERAL").get("DoYields"),
            blindingindece=blindingindece,
            ratiomin=self.m_cfgset.get("GENERAL").get("RatioMin"),
            ratiomax=self.m_cfgset.get("GENERAL").get("RatioMax"),
            LegendXMin=self.m_cfgset.get("GENERAL").get("LegendXMin"),
            LegendXMax=self.m_cfgset.get("GENERAL").get("LegendXMax"),
            LegendYMin=self.m_cfgset.get("GENERAL").get("LegendYMin"),
            LegendYMax=self.m_cfgset.get("GENERAL").get("LegendYMax"))
        rp.draw()
        rp.setxlabel("ratio", variable.get("Label"))
        rp.setylabel("ratio", "Data/MC")
        rp.setylabel("h1", "Events")
        rp.setyrange("h1", variable.get("YMin"), variable.get("YMax"))
        rp.setscale("y", variable.get("Scale"))
        rp.setLabels(self.m_cfgset.get("GENERAL").get("ATLASLabel"),
                     self.m_cfgset.get("GENERAL").get("CMLabel"),
                     self.m_cfgset.get("GENERAL").get("CustomLabel"),
                     opt="data" if has["data"] else "MC"
                     )
        fnames = [f"{self.m_cfgset.get('DIR').get('CtrlDataMC')}/{hf.filter_VarPathName(variable.get('Name'))}.{ending}"
                  for ending in self.m_cfgset.get('GENERAL').get('PlotFormat')]
        rp.savefig(fnames)

    def dopredtruthplot2D(self, output):
        """Method to produce a 2D plot of truth versus prediction. Useful for regression tasks.

        :param output: Output (config) settings object containing information on the variable to plot.
        :returns:

        """
        hist2D = TH2D(f"{output.get('Name')}_vs_Truth",
                      "",
                      int(len(self.m_binningdict[output.get("Name")]) - 1),
                      self.m_binningdict[output.get("Name")],
                      int(len(self.m_binningdict[output.get("Name")]) - 1),
                      self.m_binningdict[output.get("Name")])
        for values in self.m_dhandler.get("all",
                                          vnames=[output.get("Name"), "Label", "Weight"],
                                          return_NaNs=True,
                                          test=True,
                                          scaled=False).values:
            hist2D.Fill(values[0], values[1], values[2])
        plot2D = plt.stack2D(hist2D)
        plot2D.draw()
        plot2D.setxlabel("h1", f"{output.get('Label')} (Pred.)")
        plot2D.setylabel("h1", f"{output.get('Label')} (Truth)")
        plot2D.setzlabel("h1", "Number of events")
        plot2D.setLabels(self.m_cfgset.get("GENERAL").get("ATLASLabel"),
                         self.m_cfgset.get("GENERAL").get("CMLabel"),
                         self.m_cfgset.get("GENERAL").get("CustomLabel"))
        vs = hf.filter_VarPathName(f"{output.get('Name')}_vs_Truth")
        fnames = [f"{self.m_cfgset.get('DIR').get('MVAPlots')}/{vs}.{ending}"
                  for ending in self.m_cfgset.get('GENERAL').get('PlotFormat')]
        plot2D.savefig(fnames)

    def dostack2D(self):
        """Method to produce a simple 2D stack plot to show output distributions. Useful for classification tasks.

        :returns:

        """
        outputs = list(self.m_cfgset.get("OUTPUT"))
        for i in range(len(outputs) - 1):
            output1 = outputs[i]
            for j in range(i + 1, len(outputs)):
                for sample in self.m_cfgset.get("SAMPLE"):
                    if sample.get("Type") == "Data":
                        continue
                    output2 = outputs[j]
                    hist2D = TH2D(
                        f"{output1.get('Name')}_{output2.get('Name')}_{sample.get('Name')}",
                        "",
                        50,
                        0,
                        1,
                        50,
                        0,
                        1)
                    for values in self.m_dhandler.get(
                            "all",
                            vnames=[
                                output1.get("Name"),
                                output2.get("Name"),
                                "Weight"],
                            sname=sample.get("Name"),
                            return_NaNs=True,
                            test=True,
                            scaled=False).values:
                        hist2D.Fill(values[0], values[1], values[2])
                    plot2D = plt.stack2D(hist2D)
                    plot2D.draw()
                    plot2D.setxlabel("h1", output1.get("Label"))
                    plot2D.setylabel("h1", output2.get("Label"))
                    plot2D.setzlabel("h1", "Number of events")
                    if output1.get("Scale") == "Log" or output2.get("Scale") == "Log":
                        plot2D.setscale("z", "Log")
                    plot2D.setLabels(self.m_cfgset.get("GENERAL").get("ATLASLabel"),
                                     self.m_cfgset.get("GENERAL").get("CMLabel"),
                                     self.m_cfgset.get("GENERAL").get("CustomLabel"),
                                     align="right_3")
                    s = hf.filter_VarPathName(f"{sample.get('Name')}")
                    vs = hf.filter_VarPathName(f"{output1.get('Name')}_{output2.get('Name')}")
                    fnames = [f"{self.m_cfgset.get('DIR').get('MVAPlots')}/{s}_{vs}.{ending}"
                              for ending in self.m_cfgset.get('GENERAL').get('PlotFormat')]
                    plot2D.setcustomLabel(0.4, 0.7, f"{sample.get('Name')} Response")
                    plot2D.savefig(fnames)

    def dotrainstatsplot(self):
        """Build a bar chart containing the MC stats for all declared samples.

        :returns:

        """
        unique = [sample.get("Name") for sample in self.m_cfgset.get(
            "SAMPLE") if sample.get("Type") != "Data"]
        counts = [len(self.m_dhandler.get(f="all",
                                          vnames=["Sample_Name"],
                                          sname=sample.get("Name"),
                                          return_NaNs=True,
                                          scaled=False))
                  for sample in self.m_cfgset.get("SAMPLE") if sample.get("Type") != "Data"]
        indece = sorted(range(len(counts)), key=lambda k: counts[k], reverse=True)
        counts_sorted = np.array(counts)[indece]
        unique_sorted = np.array(unique)[indece]
        hist = TH1D("Histogram", "", int(len(counts_sorted)), 0, int(len(counts_sorted)))
        for bin_ID, count in enumerate(counts_sorted):
            hist.SetBinContent(bin_ID + 1, count)
        statsplot = plt.trainstats(hist, unique_sorted, "Training Stats")
        statsplot.draw()
        statsplot.setylabel("h1", "Raw MC Events")
        statsplot.setLabels(self.m_cfgset.get("GENERAL").get("ATLASLabel"),
                            self.m_cfgset.get("GENERAL").get("CMLabel"),
                            self.m_cfgset.get("GENERAL").get("CustomLabel"),
                            align="top_outside")
        fnames = [
            f"{self.m_cfgset.get('DIR').get('TrainStats')}/TrainStats.{ending}" for ending in self.m_cfgset.get('GENERAL').get('PlotFormat')]
        statsplot.savefig(fnames)

    def donegweightstatsplot(self):
        """Method to produce a plot containing the information on the statistics of negative weights.

        :returns:

        """
        df = self.m_dhandler.get(
            "all",
            vnames=[
                "Sample_Name",
                "Weight"],
            return_NaNs=True,
            test=True,
            scaled=False)
        snames = [s.get("Name") for s in self.m_cfgset.get("SAMPLE") if s.get("Name") != "Data"]

        yields = np.array([sum(df[df["Sample_Name"] == sname]["Weight"].values)
                          for sname in snames])
        negweights = np.array(
            [sum(df[(df["Sample_Name"] == sname) & (df["Weight"] < 0)]["Weight"].values) for sname in snames])

        negweights = (-1) * negweights / yields * 100
        yields = yields / np.sum(yields) * 100

        yield_hist = TH1D("Histogram", "", int(len(yields)), 0, int(len(yields)))
        negweights_hist = TH1D("Histogram", "", int(len(negweights)), 0, int(len(negweights)))
        for (bin_ID, y), (bin_ID, w) in zip(enumerate(yields), enumerate(negweights)):
            yield_hist.SetBinContent(bin_ID + 1, y)
            negweights_hist.SetBinContent(bin_ID + 1, w)
        negweightstatsplot = plt.negweightstats(yield_hist, negweights_hist, snames)
        negweightstatsplot.draw()
        negweightstatsplot.setylabel("h1", "Percentage of Events")
        negweightstatsplot.setLabels(self.m_cfgset.get("GENERAL").get("ATLASLabel"),
                                     self.m_cfgset.get("GENERAL").get("CMLabel"),
                                     self.m_cfgset.get("GENERAL").get("CustomLabel"),
                                     align="top_outside")
        fnames = [
            f"{self.m_cfgset.get('DIR').get('TrainStats')}/NegWeightTrainStats.{ending}" for ending in self.m_cfgset.get('GENERAL').get('PlotFormat')]
        negweightstatsplot.savefig(fnames)

    def dopredtruthplot(self, output):
        """Method to produce a plot showing truth and predicted values. Useful for regression tasks.

        :param output: output object
        :returns:

        """
        for sample in self.m_cfgset.get("SAMPLE"):
            y = self.m_dhandler.get(f="all",
                                    vnames=["Label", "Weight"],
                                    sname=sample.get("Name"),
                                    return_NaNs=True,
                                    return_nomonly=True,
                                    test=True,
                                    scaled=False)
            self.m_hhandler.addTH1F(hname=f"{sample.get('Name')}_Label_all_test",
                                    sample=sample,
                                    binning=self.m_binningdict["Label"],
                                    xvals=y["Label"].values,
                                    weights=y["Weight"].values)
        predstack = THStack()
        truthstack = THStack()
        for sample in self.m_cfgset.get("SAMPLE"):
            if sample.get("Type") == "Data":
                continue
            predstack.Add(self.m_hhandler.get(
                f"{sample.get('Name')}_{output.get('Name')}_all_test"))
            truthstack.Add(self.m_hhandler.get(f"{sample.get('Name')}_Label_all_test"))
        truthpredplot = plt.predtruth(predstack.GetStack().Last(),
                                      truthstack.GetStack().Last())
        truthpredplot.draw()
        truthpredplot.setxlabel("ratio", output.get("Label"))
        truthpredplot.setylabel("ratio", "Truth/Pred.")
        truthpredplot.setylabel("h1", "Events")
        truthpredplot.setLabels(self.m_cfgset.get("GENERAL").get("ATLASLabel"),
                                self.m_cfgset.get("GENERAL").get("CMLabel"),
                                self.m_cfgset.get("GENERAL").get("CustomLabel"))
        p = f"{hf.addslash(self.m_cfgset.get('DIR').get('MVAPlots'))}"
        t = f"{output.get('Name')}_TruthvsPred"
        fnames = [f"{p}{t}.{ending}" for ending in self.m_cfgset.get('GENERAL').get('PlotFormat')]
        truthpredplot.savefig(fnames)

    def doseparation1D(self):
        """Method to produce a simple one-dimensional separation plot.

        :returns:

        """
        tlabels = self.m_dhandler.get("all", "Label", return_NaNs=True, test=True, scaled=False)
        tlabels = np.unique(tlabels[~np.isnan(tlabels)])
        if len(tlabels) == 2:
            tlabels = np.array([1])
        for tlabel, output in zip(tlabels, self.m_cfgset.get("OUTPUT")):
            sigstack = THStack()
            bkgstack = THStack()
            for sample in self.m_cfgset.get("SAMPLE"):
                if sample.get("Type") == "Data":
                    continue
                hname = f"{sample.get('Name')}_{output.get('Name')}_all_test"
                if sample.get("Target") == tlabel:
                    sigstack.Add(self.m_hhandler.get(hname))
                else:
                    bkgstack.Add(self.m_hhandler.get(hname))
            sep = plt.sep1D(sigstack.GetStack().Last(), bkgstack.GetStack().Last())
            sep.draw()
            sep.setxlabel("Sig", output.get("Label"))
            sep.setylabel("Sig", "Fraction of Events")
            sep.setyrange("Sig", output.get("YMin"), output.get("YMax"))
            sep.setscale("y", output.get("Scale"))
            sep.setLabels(self.m_cfgset.get("GENERAL").get("ATLASLabel"),
                          self.m_cfgset.get("GENERAL").get("CMLabel"),
                          self.m_cfgset.get("GENERAL").get("CustomLabel"),
                          opt="MC")
            p = f"{hf.addslash(self.m_cfgset.get('DIR').get('Separation'))}"
            t = f"{output.get('Name')}_Separation1D"
            fnames = [f"{p}{t}.{ending}" for ending in self.m_cfgset.get(
                'GENERAL').get('PlotFormat')]
            sep.savefig(fnames)

    def doseparation2D(self, variable):
        """Method to produce a simple two-dimensional separation plot. I.e. separating different samples.

        :param variable: (Configparser) variable object
        :returns:

        """
        sweights = self.m_dhandler.get("all",
                                       vnames=["Sample_Name", "Weight"],
                                       return_NaNs=True,
                                       test=True,
                                       scaled=False)
        snames = np.unique(sweights["Sample_Name"].values)
        hist2D = TH2D(f"{variable.get('Name')}_hist",
                      "",
                      int(len(self.m_binningdict[variable.get("Name")]) - 1),
                      self.m_binningdict[variable.get("Name")],
                      len(snames),
                      0,
                      len(snames))
        norm = {sname: np.sum(sweights[sweights["Sample_Name"] == sname]
                              ["Weight"].values) for sname in snames}
        for sname in snames:
            for v in self.m_dhandler.get("all",
                                         vnames=[variable.get("Name"), "Weight"],
                                         sname=sname,
                                         return_NaNs=True,
                                         test=True,
                                         scaled=False).values:

                hist2D.Fill(v[0], sname, v[1] / norm[sname] * 100)

        sep = plt.sep2D(hist2D, "2D Separation")
        sep.draw()
        sep.setxlabel("h1", variable.get("Label"))
        sep.setylabel("h1", "Sample")
        sep.setscale("z", variable.get("Scale"))
        sep.setLabels(self.m_cfgset.get("GENERAL").get("ATLASLabel"),
                      self.m_cfgset.get("GENERAL").get("CMLabel"),
                      self.m_cfgset.get("GENERAL").get("CustomLabel"),
                      opt="MC")
        p = f"{hf.addslash(self.m_cfgset.get('DIR').get('Separation'))}"
        t = f"{variable.get('Name')}_Separation2D"
        fnames = [f"{p}{t}.{ending}" for ending in self.m_cfgset.get('GENERAL').get('PlotFormat')]
        sep.savefig(fnames)

    def donnegweightscomp(self, variable):
        """Method to produce a plot comparing the effects of negative weights and the chosen neg. weight treatment.

        :returns:

        """
        stacks = {"sig": {"nominal": THStack(),
                          "wpos": THStack(),
                          "wtreated": THStack()},
                  "bkg": {"nominal": THStack(),
                          "wpos": THStack(),
                          "wtreated": THStack()}}
        hassig, hasbkg = False, False
        for sample in self.m_cfgset.get("SAMPLE"):
            hname = f"{sample.get('Name')}_{variable.get('Name')}_all_test"
            if sample.get("Type") == "Data":
                continue
            mchist_nominal = self.m_hhandler.get(hname)
            mchist_wpos = self.m_hhandler.get(f"{hname}-wpos")
            mchist_wtreated = self.m_hhandler.get(f"{hname}-wtreated")
            if sample.get("Type") == "Signal":
                hassig = True
                stacks["sig"]["nominal"].Add(mchist_nominal)
                stacks["sig"]["wpos"].Add(mchist_wpos)
                stacks["sig"]["wtreated"].Add(mchist_wtreated)
            if sample.get("Type") == "Background":
                hasbkg = True
                stacks["bkg"]["nominal"].Add(mchist_nominal)
                stacks["bkg"]["wpos"].Add(mchist_wpos)
                stacks["bkg"]["wtreated"].Add(mchist_wtreated)

        nnegweightscompplot = plt.nnegweightscomp(
            stacks["sig"]["nominal"].GetStack().Last() if hassig else None,
            stacks["sig"]["wpos"].GetStack().Last() if hassig else None,
            stacks["sig"]["wtreated"].GetStack().Last() if hassig else None,
            stacks["bkg"]["nominal"].GetStack().Last() if hasbkg else None,
            stacks["bkg"]["wpos"].GetStack().Last() if hasbkg else None,
            stacks["bkg"]["wtreated"].GetStack().Last() if hasbkg else None)
        nnegweightscompplot.draw()
        nnegweightscompplot.setxlabel("ratio", variable.get("Label"))
        nnegweightscompplot.setylabel("h1", "Events")
        nnegweightscompplot.setylabel("ratio", "Ratio")
        nnegweightscompplot.setLabels(self.m_cfgset.get("GENERAL").get("ATLASLabel"),
                                      self.m_cfgset.get("GENERAL").get("CMLabel"),
                                      self.m_cfgset.get("GENERAL").get("CustomLabel"))
        p = f"{hf.addslash(self.m_cfgset.get('DIR').get('CtrlMisc'))}"
        t = f"{variable.get('Name')}_NegWeightComparison"
        fnames = [f"{p}{t}.{ending}" for ending in self.m_cfgset.get('GENERAL').get('PlotFormat')]
        nnegweightscompplot.savefig(fnames)

    def docorrmatrix(self, variables):
        """Method to produce a correlation matrix

        :returns:

        """
        vnames = [v.get("Name") for v in variables]
        corrs = self.m_dhandler.get(f="all",
                                    vnames=vnames,
                                    scaled=False).corr()
        corrs.to_csv(f"{self.m_cfgset.get('DIR').get('Tables')}/CorrelationMatrix.csv")
        corrhist = TH2D(
            "Correlations2Dhist",
            "",
            len(corrs),
            0,
            len(corrs),
            len(corrs),
            0,
            len(corrs))
        for xbin, varname1 in enumerate(vnames):
            for ybin, varname2 in enumerate(vnames):
                corrhist.SetBinContent(xbin + 1, ybin + 1, corrs[varname1][varname2])
        corrmatrix = plt.corrmatrix(corrhist)
        corrmatrix.draw()
        corrmatrix.setLabels(self.m_cfgset.get("GENERAL").get("ATLASLabel"),
                             self.m_cfgset.get("GENERAL").get("CMLabel"),
                             self.m_cfgset.get("GENERAL").get("CustomLabel"),
                             align="top_outside")
        corrhist.GetXaxis().SetLabelSize(0.01 * 50 / len(variables))
        corrhist.GetYaxis().SetLabelSize(0.01 * 50 / len(variables))
        for i, var in enumerate(variables):
            corrhist.GetXaxis().SetBinLabel(
                i + 1,
                var.get("Label").replace(
                    "[GeV]",
                    "").replace(
                    "/",
                    "#"))
            corrhist.GetYaxis().SetBinLabel(
                i + 1,
                var.get("Label").replace(
                    "[GeV]",
                    "").replace(
                    "/",
                    "#"))
            corrhist.GetXaxis().SetBinLabel(
                i + 1,
                var.get("Label").replace(
                    "[MeV]",
                    "").replace(
                    "/",
                    "#"))
            corrhist.GetYaxis().SetBinLabel(
                i + 1,
                var.get("Label").replace(
                    "[MeV]",
                    "").replace(
                    "/",
                    "#"))
        corrhist.GetXaxis().LabelsOption("v")
        corrmatrix.savefig([f"{self.m_cfgset.get('DIR').get('CtrlMisc')}/CorrelationMatrix.{ending}"
                            for ending in self.m_cfgset.get('GENERAL').get('PlotFormat')])

        # Now we only plot the large correlations
        corrsvars = [
            var for var in vnames if any(
                True for x in corrs[var].values if (
                    x > self.m_cfgset.get("GENERAL").get("LargeCorrelation")) & (
                    x != 1))]
        corrs = corrs[corrsvars].drop(list(set(vnames) - set(corrsvars)))
        corrs.to_csv(f"{self.m_cfgset.get('DIR').get('Tables')}/LargeCorrelationMatrix.csv")
        if len(corrs.keys()) > 0:
            corrhist = TH2D(
                "Correlations2Dhist",
                "",
                len(corrs),
                0,
                len(corrs),
                len(corrs),
                0,
                len(corrs))
            for xbin, varname1 in enumerate(corrsvars):
                for ybin, varname2 in enumerate(corrsvars):
                    corrhist.SetBinContent(xbin + 1, ybin + 1, corrs[varname1][varname2])
            corrmatrix = plt.corrmatrix(corrhist)
            corrmatrix.draw()
            corrmatrix.setLabels(self.m_cfgset.get("GENERAL").get("ATLASLabel"),
                                 self.m_cfgset.get("GENERAL").get("CMLabel"),
                                 self.m_cfgset.get("GENERAL").get("CustomLabel"),
                                 align="top_outside")
            corrhist.GetXaxis().SetLabelSize(0.01 * 50 / len(variables))
            corrhist.GetYaxis().SetLabelSize(0.01 * 50 / len(variables))
            ldict = {var.get("Name"): var.get("Label") for var in variables}
            for i, vname in enumerate(corrs.keys()):
                corrhist.GetXaxis().SetBinLabel(
                    i + 1,
                    ldict[vname].replace(
                        "[GeV]",
                        "").replace(
                        "/",
                        "#"))
                corrhist.GetYaxis().SetBinLabel(
                    i + 1,
                    ldict[vname].replace(
                        "[GeV]",
                        "").replace(
                        "/",
                        "#"))
                corrhist.GetXaxis().SetBinLabel(
                    i + 1,
                    ldict[vname].replace(
                        "[MeV]",
                        "").replace(
                        "/",
                        "#"))
                corrhist.GetYaxis().SetBinLabel(
                    i + 1,
                    ldict[vname].replace(
                        "[MeV]",
                        "").replace(
                        "/",
                        "#"))
            corrhist.GetXaxis().LabelsOption("v")
            corrmatrix.savefig([f"{self.m_cfgset.get('DIR').get('CtrlMisc')}/LargeCorrelationMatrix.{ending}"
                                for ending in self.m_cfgset.get('GENERAL').get('PlotFormat')])

    def docorrcomp(self):
        """Method to produce a plot comparing correlations using different treatments of negative weights.

        :returns:

        """

        warnings.warn("Correlation comparison plots are not yet implemented")

    def doperfmplot(self):
        """Method to produce a simple performance plot, i.e. loss curves

        :returns:

        """
        for fold in range(self.m_cfgset.get("GENERAL").get("Folds")):
            histpath = f"{hf.addslash(self.m_cfgset.get('DIR').get('Model'))}{self.m_cfgset.get('MODEL').get('Name')}_{self.m_cfgset.get('MODEL').get('Type')}_{fold}_history.json"
            with open(histpath, encoding="utf-8") as f:
                history = json.load(f)
            pp = plt.perfmplot(history)
            pp.draw()
            pp.setxlabel("mg", "Epochs")
            pp.setylabel("mg", "Loss")
            pp.setscale("x", self.m_cfgset.get("GENERAL").get("LossPlotXScale"))
            pp.setscale("y", self.m_cfgset.get("GENERAL").get("LossPlotYScale"))
            pp.setLabels(self.m_cfgset.get("GENERAL").get("ATLASLabel"),
                         self.m_cfgset.get("GENERAL").get("CMLabel"),
                         self.m_cfgset.get("GENERAL").get("CustomLabel"),
                         opt="MC")
            fnames = [f"{self.m_cfgset.get('DIR').get('MVAPlots')}/Loss_{fold}.{ending}"
                      for ending in self.m_cfgset.get('GENERAL').get('PlotFormat')]
            pp.savefig(fnames)

    def dotraintestplots(self, variable):
        """Method to produce comparison plots for train and test

        :param variable: (Configparser) variable object
        :returns:

        """
        for fold in range(self.m_cfgset.get("GENERAL").get("Folds")):
            trainsigstack, trainbkgstack = THStack(), THStack()
            testsigstack, testbkgstack = THStack(), THStack()
            hassig, hasbkg = False, False
            for sample in self.m_cfgset.get("SAMPLE"):
                hname = f"{sample.get('Name')}_{variable.get('Name')}_{fold}"
                if sample.get("Type") == "Signal":
                    trainsigstack.Add(self.m_hhandler.get(f"{hname}_train"))
                    testsigstack.Add(self.m_hhandler.get(f"{hname}_test"))
                    hassig = True
                elif sample.get("Type") == "Background":
                    trainbkgstack.Add(self.m_hhandler.get(f"{hname}_train"))
                    testbkgstack.Add(self.m_hhandler.get(f"{hname}_test"))
                    hasbkg = True
            ksplot = plt.traintestplot(
                testsigstack.GetStack().Last() if hassig else None,
                trainsigstack.GetStack().Last() if hassig else None,
                testbkgstack.GetStack().Last() if hasbkg else None,
                trainbkgstack.GetStack().Last() if hasbkg else None,
                comparisontest=self.m_cfgset.get("GENERAL").get("ComparisonTest"))
            ksplot.draw()
            ksplot.setylabel("h1", "Fraction of Events")
            ksplot.setxlabel("ratio", variable.get("Label"))
            ksplot.setylabel("ratio", "Train/Test")
            ksplot.setLabels(self.m_cfgset.get("GENERAL").get("ATLASLabel"),
                             self.m_cfgset.get("GENERAL").get("CMLabel"),
                             self.m_cfgset.get("GENERAL").get("CustomLabel"))
            fnames = [f"{self.m_cfgset.get('DIR').get('CtrlTrainTest')}/{variable.get('Name')}_traintest_{fold}.{ending}"
                      for ending in self.m_cfgset.get('GENERAL').get('PlotFormat')]
            ksplot.savefig(fnames)

    def doROCCurvePlotsCombined(self, output, target):
        """Method to produce ROC curve plots

        :param variable: (Configparser) variable object
        :returns:

        """
        valdict = {}
        vnames = [v.get("Name") for v in self.m_cfgset.get("VARIABLE")]
        for fold in range(self.m_cfgset.get("GENERAL").get("Folds")):
            train_inputs = copy.deepcopy(self.m_dhandler.get(f=fold,
                                                             vnames=vnames,
                                                             return_NaNs=False,
                                                             test=False,
                                                             scaled=True))
            test_inputs = copy.deepcopy(self.m_dhandler.get(f=fold,
                                                            vnames=vnames,
                                                            return_NaNs=False,
                                                            test=True,
                                                            scaled=True))
            train_vals = copy.deepcopy(self.m_dhandler.get(f=fold,
                                                           vnames=["Label", "Weight"],
                                                           return_NaNs=False,
                                                           test=False,
                                                           scaled=False))
            test_vals = copy.deepcopy(self.m_dhandler.get(f=fold,
                                                          vnames=["Label", "Weight"],
                                                          return_NaNs=False,
                                                          test=True,
                                                          scaled=False))
            train_yhat = self.m_mhandler.predict(train_inputs, fold)
            test_yhat = self.m_mhandler.predict(test_inputs, fold)
            valdict[f"Fold {fold}"] = {"Train": (train_vals["Label"],
                                                 train_yhat[output.get("Name")],
                                                 train_vals["Weight"]),
                                       "Test": (test_vals["Label"],
                                                test_yhat[output.get("Name")],
                                                test_vals["Weight"])}
        roccurveplot = plt.roccurveplot(valdict=valdict,
                                        sampling=self.m_cfgset.get("GENERAL").get("ROCSampling"),
                                        target=target,
                                        LegendXMin=self.m_cfgset.get("GENERAL").get("LegendXMin"),
                                        LegendXMax=self.m_cfgset.get("GENERAL").get("LegendXMax"),
                                        LegendYMin=self.m_cfgset.get("GENERAL").get("LegendYMin"),
                                        LegendYMax=self.m_cfgset.get("GENERAL").get("LegendYMax"))
        roccurveplot.draw()
        roccurveplot.setxlabel("mg", "False Positive Rate")
        roccurveplot.setylabel("mg", "True Positive Rate")
        roccurveplot.setLabels(self.m_cfgset.get("GENERAL").get("ATLASLabel"),
                               self.m_cfgset.get("GENERAL").get("CMLabel"),
                               self.m_cfgset.get("GENERAL").get("CustomLabel"))
        fnames = [f"{self.m_cfgset.get('DIR').get('MVAPlots')}/{output.get('Name')}_ROC.{ending}".replace(
            " ", "_") for ending in self.m_cfgset.get('GENERAL').get('PlotFormat')]
        roccurveplot.savefig(fnames)

    def doPermutationImportance(self, output, target):
        """TODO describe function

        :param output:
        :param target:
        :returns:

        """
        vnames = [v.get("Name") for v in self.m_cfgset.get("VARIABLE")]
        valdict = {vname: [] for vname in vnames}
        vlabels = [v.get("Label") for v in self.m_cfgset.get("VARIABLE")]
        xnom = self.m_dhandler.get(0,
                                   vnames=vnames + ["Label", "Weight", "Index_tmp"],
                                   test=True,
                                   return_NaNs=False,
                                   scaled=True)
        y_pred = self.m_mhandler.predict(xnom[vnames], 0)[output.get("Name")]
        valdict["nominal"] = [(xnom["Label"], y_pred, xnom["Weight"])]
        for i, vname in enumerate(vnames):
            InfoMessage(f"Calculating {output.get('PermImportanceShuffles')} Predictions for shuffled {vname}. Variable {i+1}/{len(vnames)}.")
            shuffled = copy.deepcopy(xnom[vnames].values)  # deep copy because shuffle shuffles in place
            for _ in range(output.get("PermImportanceShuffles")):
                np.random.shuffle(shuffled[:, i])
                shuffled_df = pd.DataFrame(shuffled, columns=vnames)
                y_pred = self.m_mhandler.predict(shuffled_df, 0)[output.get("Name")]
                valdict[vname].append((xnom["Label"], y_pred, xnom["Weight"]))
        permplot = plt.permutationimportanceplot(
            valdict=valdict,
            sampling=self.m_cfgset.get("GENERAL").get("ROCSampling"),
            target=target,
            vlabels=vlabels)
        permplot.draw()
        permplot.setylabel("h1", "(AUC_{nom}-AUC)/AUC_{nom}")
        permplot.setscale("x", output.get("PermImportancePlotScale"))
        permplot.setLabels(self.m_cfgset.get("GENERAL").get("ATLASLabel"),
                           self.m_cfgset.get("GENERAL").get("CMLabel"),
                           self.m_cfgset.get("GENERAL").get("CustomLabel"),
                           align="top_outside",
                           opt="MC")
        permplot.setcustomLabel(0.7, 0.65, output.get("Label"))
        fnames = [
            f"{self.m_cfgset.get('DIR').get('MVAPlots')}/{output.get('Name')}_PermutationImportance.{ending}".replace(
                " ", "_") for ending in self.m_cfgset.get('GENERAL').get('PlotFormat')]
        permplot.savefig(fnames)

    def doConfusionMatrix(self):
        all_vals = self.m_dhandler.get(f="all", vnames=[output.get("Name") for output in self.m_cfgset.get(
            "OUTPUT")] + ["Weight", "Label"], return_NaNs=False, return_nomonly=True, test=True, scaled=False)
        if len(self.m_cfgset.get("OUTPUT")) == 1:
            y_pred = np.round(all_vals[self.m_cfgset.get("OUTPUT")[0].get("Name")].values)
            labels = ["Background", "Signal"]
        else:
            y_pred = np.argmax(all_vals[[output.get("Name")
                               for output in self.m_cfgset.get("OUTPUT")]].values, axis=1)
            labels = [output.get("Label") for output in self.m_cfgset.get("OUTPUT")]
        confmatrix = confusion_matrix(
            all_vals["Label"].values,
            y_pred,
            sample_weight=all_vals["Weight"].values)
        if self.m_cfgset.get("GENERAL").get("ConfusionMatrixNorm") == "column":
            confmatrix = confmatrix / np.sum(confmatrix, axis=0)
        elif self.m_cfgset.get("GENERAL").get("ConfusionMatrixNorm") == "row":
            confmatrix = (confmatrix.T / np.sum(confmatrix, axis=1)).T
        confmatrixplt = plt.confusionmatrix(confmatrix, labels)
        confmatrixplt.draw()
        confmatrixplt.setscale("z", self.m_cfgset.get("GENERAL").get("ConfusionPlotScale"))
        confmatrixplt.setLabels(self.m_cfgset.get("GENERAL").get("ATLASLabel"),
                                self.m_cfgset.get("GENERAL").get("CMLabel"),
                                self.m_cfgset.get("GENERAL").get("CustomLabel"),
                                align="top_outside")
        fnames = [f"{self.m_cfgset.get('DIR').get('MVAPlots')}/ConfusionMatrix.{ending}".replace(" ", "_")
                  for ending in self.m_cfgset.get('GENERAL').get('PlotFormat')]
        confmatrixplt.savefig(fnames)

    def doplots(self, args):
        """Do requested plots

        :param args:
        :returns:

        """
        self._fillctrlhistos()
        if "all" in args.plots:
            self.dotrainstatsplot()
            self.donegweightstatsplot()
            for variable in self.m_cfgset.get("VARIABLE"):
                self.doratioplots(variable)
            for variable in self.m_cfgset.get("VARIABLE"):
                self.doseparation2D(variable)
            for variable in self.m_cfgset.get("VARIABLE"):
                self.dotraintestplots(variable)
            self.docorrmatrix(self.m_cfgset.get("VARIABLE"))
            self.docorrcomp()
            for variable in self.m_cfgset.get("VARIABLE"):
                self.donnegweightscomp(variable)
        if "TrainStats" in args.plots:
            self.dotrainstatsplot()
        if "NegativeWeightSummary" in args.plots:
            self.donegweightstatsplot()
        if "DataMC" in args.plots:
            for variable in self.m_cfgset.get("VARIABLE"):
                self.doratioplots(variable)
        if "Separation2D" in args.plots:
            for variable in self.m_cfgset.get("VARIABLE"):
                self.doseparation2D(variable)
        if "CorrelationMatrix" in args.plots:
            self.docorrmatrix(self.m_cfgset.get("VARIABLE"))
        if "CorrelationComparison" in args.plots:
            self.docorrcomp()
        if "nonNegativeWeights" in args.plots:
            for variable in self.m_cfgset.get("VARIABLE"):
                self.donnegweightscomp(variable)
        # Todo add scaled ratio plot decision

    def doevalplots(self, args):
        """Function to perform evaluation plots

        :param args: argparse arguments
        :returns:

        """
        self._getpred()
        self._fillevalhistos()
        if "all" in args.plots:
            self.doperfmplot()
            for output in self.m_cfgset.get("OUTPUT"):
                self.doratioplots(output)
                if "Classification" in self.m_cfgset.get("MODEL").get("Type"):
                    self.doseparation2D(output)
                self.dotraintestplots(output)
            if len(self.m_cfgset.get("OUTPUT")) > 1:
                self.dostack2D()
            if "Classification" in self.m_cfgset.get("MODEL").get("Type"):
                self.doseparation1D()
                self.doConfusionMatrix()
                for i, output in enumerate(self.m_cfgset.get("OUTPUT")):
                    if len(self.m_cfgset.get("OUTPUT")) == 1:
                        i += 1
                    self.doROCCurvePlotsCombined(output, target=float(i))
                for i, output in enumerate(self.m_cfgset.get("OUTPUT")):
                    if len(self.m_cfgset.get("OUTPUT")) == 1:
                        i += 1
                    self.doPermutationImportance(output, target=i)
            if "Regression" in self.m_cfgset.get("MODEL").get("Type"):
                for output in self.m_cfgset.get("OUTPUT"):
                    self.dopredtruthplot(output)
                    self.dopredtruthplot2D(output)
        if "Performance" in args.plots:
            self.doperfmplot()
        if "Separation1D" in args.plots and "Classification" in self.m_cfgset.get(
                "MODEL").get("Type"):
            self.doseparation1D()
        if "Sepearation2D" in args.plots and "Classification" in self.m_cfgset.get(
                "MODEL").get("Type"):
            for output in self.m_cfgset.get("OUTPUT"):
                self.doseparation2D(output)
        if "DataMC" in args.plots:
            for output in self.m_cfgset.get("OUTPUT"):
                self.doratioplots(output)
        if "TrainTestPlots" in args.plots:
            for output in self.m_cfgset.get("OUTPUT"):
                self.dotraintestplots(output)
        if "ROCCurvePlots" in args.plots:
            for i, output in enumerate(self.m_cfgset.get("OUTPUT")):
                self.doROCCurvePlotsCombined(output, target=i)
        if "PermutationImportances" in args.plots:
            for i, output in enumerate(self.m_cfgset.get("OUTPUT")):
                self.doPermutationImportance(output, target=i)
        if "ConfusionMatrix" in args.plots:
            self.doConfusionMatrix()
