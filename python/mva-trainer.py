#!/usr/bin/env python3
"""TODO add module description

"""
import argparse
import gc
import os
import glob
from multiprocessing import Pool

import HelperModules.helperfunctions as hf
from HelperModules.configparser import configparser
from HelperModules.messagehandler import ErrorMessage, InfoMessage, InfoDelimiter


def inject(c, a, fname, processid, maxprocessid):
    """Multi-processing function to do an injection.

    :param c: configparser object
    :param a: argparse object
    :param fname: file name
    :param processid: id of the process
    :param maxprocessid: maximum id of all processes
    :returns:

    """
    inj = injectionhandler(c, a)
    inj.inject(fname, processid, maxprocessid)
    del inj
    gc.collect()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    cargs = parser.add_argument_group(title="\033[1mCommon arguments\033[0m")
    injargs = parser.add_argument_group(title="\033[1mInjection-specific arguments\033[0m")
    optargs = parser.add_argument_group(title="\033[1mOptimisation-specific arguments\033[0m")
    othargs = parser.add_argument_group(title="\033[1mOther arguments\033[0m")
    # Common arguments
    cargs.add_argument("-c", "--configfile", help="Config file", required=True)
    cargs.add_argument("--convert",
                       help="flag to set in case you want to run the conversion",
                       action='store_true',
                       dest="convert",
                       default=False)
    cargs.add_argument("--train",
                       help="flag to set in case you want to run the training",
                       action='store_true',
                       dest="train",
                       default=False)
    cargs.add_argument("--evaluate",
                       help="flag to set in case you want to run the evaluation",
                       action='store_true',
                       dest="evaluate",
                       default=False)
    cargs.add_argument("--inject",
                       help="flag to set in case you want to run the injection",
                       action='store_true',
                       dest="inject",
                       default=False)
    cargs.add_argument("--optimise",
                       help="flag to set in case you want to run the optimisation",
                       action='store_true',
                       dest="optimise",
                       default=False)
    cargs.add_argument("--compare",
                       help="flag to set in case you want to compare different models",
                       type=int,
                       dest="compare",
                       default=0)
    # Training-specific arguments:
    injargs.add_argument("--fold", dest="fold", type=int, default=-1, help="Which fold to run the training for")
    # Injection-specific arguments
    injargs.add_argument("-i", "--input_path", help="Ntuple path")
    injargs.add_argument("-o", "--output_path", help="output path for the converted ntuples")
    injargs.add_argument("-f",
                         "--filter",
                         nargs="+",
                         dest="wildcard",
                         help="additional string to filter input files",
                         default=[''])
    injargs.add_argument("--ignore",
                         nargs="+",
                         dest="ignore",
                         help="additional string to ignore input files",
                         default=[''])
    injargs.add_argument(
        "-t",
        "--treefilter",
        dest="treewildcard",
        help="additional string to filter trees",
        type=str,
        default='')
    injargs.add_argument("--processes",
                         help="the number of parallel processes to run",
                         type=int,
                         default=8)
    injargs.add_argument(
        "--additionalconfigs",
        help="Additional config files for injection",
        nargs="+",
    )
    injargs.add_argument(
        "--outputonly",
        help="Only inject the output and don't clone the full tree",
        action="store_true",
        dest="outputonly",
        default=False)
    # Optimisation-specific arguments
    optargs.add_argument("--nModels",
                         dest="nModels",
                         help="Number of configs to be created for hyperparameter optimisation.",
                         type=int,
                         default=10)
    optargs.add_argument("--HO_options",
                         help="Hyperparameter running option",
                         choices=[
                             "Converter",
                             "Trainer",
                             "Evaluater",
                             "All"],
                         dest="HO_options",
                         default="Trainer")
    # Other arguments
    othargs.add_argument("--plots",
                         nargs="+",
                         help="the dedicated plots to be produced, default is 'all'",
                         default=["all"])
    othargs.add_argument("--plots-only",
                         help="flag to set in case you only want to redo the plots",
                         action='store_true',
                         dest="plots_only",
                         default=False)
    othargs.add_argument("--data-only",
                         help="flag to set in case you only want to convert the data",
                         action='store_true',
                         dest="data_only",
                         default=False)
    args = parser.parse_args()

    cfgset = configparser(args.configfile)
    cfgset.read()
    if args.convert:
        cfgset.Print()
        from HelperModules.conversionhandler import conversionhandler
        convhandler = conversionhandler(cfgset)
        if not args.plots_only:
            convhandler.convert()
        if not args.data_only:
            from PlotterModules.plothandler import plothandler
            phandler = plothandler(cfgset)
            phandler.doplots(args)
    if args.train:
        cfgset.Print()
        from ModelModules.modelhandler import modelhandler
        mhandler = modelhandler(cfgset)
        if args.fold == -1:
            for fold in range(cfgset.get("GENERAL").get("Folds")):
                mhandler.runTraining(fold)
        else:
            mhandler.runTraining(args.fold)
    if args.evaluate:
        cfgset.Print()
        from PlotterModules.plothandler import plothandler
        phandler = plothandler(cfgset)
        phandler.doevalplots(args)
    if args.inject:
        from HelperModules.injectionhandler import injectionhandler
        if args.input_path is None:
            ErrorMessage("No input path given!")
        if args.output_path is None:
            ErrorMessage("No output path given!")
        for dirpath, dirnames, filenames in os.walk(args.input_path):
            structure = os.path.join(args.output_path, dirpath[len(args.input_path):].strip("/"))
            if not os.path.isdir(structure):
                os.makedirs(structure)
        InfoMessage("Reading configs...")
        if args.additionalconfigs is not None:
            if "*" in args.additionalconfigs:
                addconfigs = [configparser(cfg) for cfg in glob.glob(args.additionalconfigs)]
            else:
                addconfigs = [configparser(cfg) for cfg in args.additionalconfigs]
        else:
            addconfigs = []
        cfgset.Print()
        InfoDelimiter(3)
        for addconfig in addconfigs:
            addconfig.read()
            addconfig.Print()
            InfoDelimiter(3)
        cfgsets = [cfgset] + addconfigs
        fnames = hf.absfpaths(args.input_path)
        fprocess = []
        fnames = [f for f in fnames if ".root" in f]
        fnames = [f for f in fnames if (
            any(w in f for w in args.wildcard) or args.wildcard == [''])]
        fnames = [f for f in fnames if (
            all(i not in f for i in args.ignore) or args.ignore == [''])]
        p = Pool(args.processes, maxtasksperchild=1)
        MAX_ID = len(fnames)
        results = [p.apply_async(inject, (cfgsets, args, f, i + 1, MAX_ID,))
                   for i, f in enumerate(fnames)]
        results_g = [r.get() for r in results]
        p.close()
        p.join()
    if args.optimise:
        from ModelModules.modeloptimisation import modeloptimisation
        modeloptimiser = modeloptimisation(args, cfgset)
        modeloptimiser.writeconfigs()
    if args.compare > 0:
        from ModelModules.modelcomparison import modelcomparison
        if args.additionalconfigs is not None:
            if "*" in args.additionalconfigs:
                addconfigs = [configparser(cfg) for cfg in glob.glob(args.additionalconfigs)]
            else:
                addconfigs = [configparser(cfg) for cfg in args.additionalconfigs]
        else:
            addconfigs = []
        for addconfig in addconfigs:
            addconfig.read()
        cfgsets = [cfgset] + addconfigs
        modelcomparator = modelcomparison(cfgsets, args.compare)
        modelcomparator.auc_ranking()
        modelcomparator.save()
