""" Module generalising the interaction with models

"""

from collections import OrderedDict

import HelperModules.helperfunctions as hf
import numpy as np
import pandas as pd
import torch
from HelperModules.datahandler import datahandler
from HelperModules.graphbuilder import graphbuilder
from HelperModules.messagehandler import TrainerDoneMessage, InfoMessage, ErrorMessage
from ModelModules.dnnmodel import dnnmodel
from ModelModules.gnnmodel import gnnmodel
from ModelModules.onnxhandler import onnxhandler
from torch_geometric.loader import DataLoader


class modelhandler():
    """Class handling the interactions with models such as loading or predicting.

    """

    def __init__(self, cfgset, verbose=True):
        self.m_cfgset = cfgset
        self.m_datahandler = None
        self.m_models = {}
        self.m_verbose = verbose

    def _compile(self):
        """Method to call the necessary model constructor

        :returns:

        """
        if self.m_cfgset.get("MODEL").get("Type") in ["Classification-DNN",
                                                      "Regression-DNN"]:
            model = dnnmodel(self.m_cfgset)
        elif self.m_cfgset.get("MODEL").get("Type") in ["Classification-GNN",
                                                        "Regression-GNN"]:
            model = gnnmodel(self.m_cfgset)
        else:
            raise NotImplementedError
        return model

    def _getmodelpath(self, fold):
        """Method to figure out the correct model path

        :param fold: Fold number
        :returns:

        """
        p = f"{hf.addslash(self.m_cfgset.get('DIR').get('Model'))}"
        n = f"{self.m_cfgset.get('MODEL').get('Name')}"
        t = f"{self.m_cfgset.get('MODEL').get('Type')}"
        return f"{p}{n}_{t}_{fold}.pkl"

    def _load(self, fold):
        """Loading a model from disk

        :param fold: Fold number
        :returns:

        """
        if fold not in self.m_models:
            model = self._compile()
            if self.m_verbose:
                InfoMessage(f"Compiled model {model.getname()} for fold {fold}.")
            model.load_state_dict(torch.load(self._getmodelpath(fold)), assign=True)
            if self.m_verbose:
                InfoMessage(f"Restored model parameters from {self._getmodelpath(fold)} for fold {fold}.")
            self.m_models[fold] = model
        return self.m_models[fold]

    def predict(self, xin, fold):
        """Wrapper method to perform predictions for a given fold.

        :param fold: Fold number
        :returns:

        """
        model = self._load(fold)
        if self.m_cfgset.get("MODEL").get("Type") in ["Classification-DNN",
                                                      "Regression-DNN"]:
            model.eval()
            p = model(torch.from_numpy(xin.values).float())
            p = p.detach().numpy()
        elif self.m_cfgset.get("MODEL").get("Type") == "Classification-GNN":
            model.eval()
            # for graphs we have to use a dataloader
            # TODO figure out whether there is a smarter way to do this
            gbuilder = graphbuilder(
                self.m_cfgset.get("GRAPHNODE"),
                self.m_cfgset.get("MODEL").get("GraphStructure"))
            loader = DataLoader(gbuilder.buildgraphs(xin.to_dict('records')),
                                self.m_cfgset.get("MODEL").get("BatchSize"))
            p = []
            for data in loader:
                p.extend(model(data).detach().numpy())
            p = np.array(p)
        else:
            raise NotImplementedError
        outnames = [output.get("Name") for output in self.m_cfgset.get("OUTPUT")]
        return pd.DataFrame(data=p, columns=outnames)

    def save(self, model, history, fold):
        """Save model and history for a given fold.

        :param model: Model object to be saved
        :param history: Dictionary containing losses
        :param fold: Fold
        :returns:

        """
        histpath = f"{hf.addslash(self.m_cfgset.get('DIR').get('Model'))}{self.m_cfgset.get('MODEL').get('Name')}_{self.m_cfgset.get('MODEL').get('Type')}_{fold}_history.json"
        modelpath = f"{hf.addslash(self.m_cfgset.get('DIR').get('Model'))}{self.m_cfgset.get('MODEL').get('Name')}_{self.m_cfgset.get('MODEL').get('Type')}_{fold}.pkl"
        metadatapath = f"{hf.addslash(self.m_cfgset.get('DIR').get('Model'))}_model_metadata_{fold}.json"
        onnxpath = f"{hf.addslash(self.m_cfgset.get('DIR').get('Model'))}{self.m_cfgset.get('MODEL').get('Name')}_{self.m_cfgset.get('MODEL').get('Type')}_{fold}.onnx"

        if self.m_verbose:
            TrainerDoneMessage(f"Training done! Writing model to {modelpath}")
        if self.m_cfgset.get("MODEL").get("Type") in ["Classification-DNN",
                                                      "Classification-GNN",
                                                      "Regression-DNN",
                                                      "Regression-GNN"]:
            d = OrderedDict([(f"m_model.{k}", v) for k, v in model.state_dict().items()])
            torch.save(d, modelpath)
        if self.m_verbose:
            TrainerDoneMessage(f"Writing history to {histpath}")
        histdf = pd.DataFrame(history)
        with open(histpath, mode='w', encoding="utf-8") as f:
            histdf.to_json(f,
                           indent=4)
        if self.m_verbose:
            TrainerDoneMessage(f"Writing history to {metadatapath}")
        self.m_cfgset.get("MODEL").save_todict(metadatapath)
        if self.m_cfgset.get("MODEL").get("GetONNXModel"):
            if "DNN" in self.m_cfgset.get("MODEL").get(
                    "Type") or "GNN" in self.m_cfgset.get("MODEL").get("Type"):
                model.eval()
            ohandler = onnxhandler(self.m_cfgset)
            ohandler.convert(model, onnxpath)
            if self.m_verbose:
                TrainerDoneMessage(f"Exported model to {onnxpath}")

    def runTraining(self, fold):
        """Run a full training of a model

        :param fold: Which fold to run the training for
        :returns:

        """
        if fold >= self.m_cfgset.get("GENERAL").get("Folds"):
            ErrorMessage(f"--fold argument has to be <= {self.m_cfgset.get('GENERAL').get('Folds')}")
        if self.m_verbose:
            InfoMessage(f"Running training for fold {fold}")
        self.m_datahandler = datahandler(self.m_cfgset)
        model = self._compile()
        vnames = [v.get("Name") for v in self.m_cfgset.get("VARIABLE")]
        model, history = model.run(x=self.m_datahandler.get(f=fold,
                                                            vnames=vnames).values,
                                   y=self.m_datahandler.get(f=fold,
                                                            vnames=["Label"]).values,
                                   w=self.m_datahandler.get(f=fold,
                                                            vnames=["Weight"]).values)
        self.save(model, history, fold)
