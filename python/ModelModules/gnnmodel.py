"""This module handles the generation of Graph Neural Networks (GNNs) of two types:
* homogeneous graph neural networks
* heterogeneous graph neural networks
The networks are build using PyG (PyTorch Geometric), a library built upon PyTorch to easily write and train GNNs.
Each GNN consists of three parts:
* A graph convolutional part, handling graph structured data
* A global dense NN, handling "regular" input data
* A final dense NN combining the forementioned graph and global outputs.

"""
import HelperModules.helperfunctions as hf
import HelperModules.pytorchhelperfunctions as pyhf
import numpy as np
import torch
from HelperModules.graphbuilder import graphbuilder
from HelperModules.messagehandler import InfoMessage, ErrorMessage
from ModelModules.basetorchmodel import basetorchmodel
from torch_geometric.loader import DataLoader
from torch_geometric.nn import (GATConv, GCNConv, GraphConv, HeteroConv, SAGEConv,
                                TransformerConv,
                                Sequential, global_mean_pool)


class gnnmodel(basetorchmodel):
    """Class to build a homogeneous or heterogeneous GNN.
    The GNN is build using the constructors of the homognnmodel or heterognnmodel classes.

    """

    def __init__(self, cfgset):
        super().__init__(cfgset)
        self.m_model = None
        self.m_ishomognn = self._initialize_model()
        self.m_gbuilder = graphbuilder(
            self.m_cfgset.get("GRAPHNODE"),
            self.m_cfgset.get("MODEL").get("GraphStructure")
        )

        self.m_optimiser = self.setoptimiser(self.m_model)
        self.setlossfct()
        self.graphinformation = {}

    def _initialize_model(self):
        """Initialize the neural network model based on the specified graph structure.

        This private method is responsible for initializing the neural network model
        according to the graph structure specified in the configuration settings.

        :return: A boolean flag indicating whether the initialized model is homogeneous.
             Returns True if the model is homogeneous, and False for a heterogeneous model.
        """
        graph_structure = self.m_cfgset.get("MODEL").get("GraphStructure")

        if graph_structure == "homogeneous":
            self.m_model = homognnmodel(self.m_cfgset, self._getglobal())
            return True
        if graph_structure == "heterogeneous":
            self.m_model = heterognnmodel(
                self.m_cfgset,
                self._getglobal(),
                self._getedgetypes()
            )
            return False
        self.m_model = None
        return False

    def _getglobal(self):
        """Get the number of global input features for a Graph Neural Network (GNN).

        This method iterates through the nodes specified in the configuration settings
        and identifies the node of type 'Global' to determine the number of global input features.

        :return: The number of global input features for the GNN. Returns 0 if no global node is found.
        :rtype: int
        """
        for node in self.m_cfgset.get("GRAPHNODE"):
            if node.get("Type") == "Global":
                return len(node.get("Features"))
        return 0

    def _getedgetypes(self):
        """Get a list of different connection types for a heterogeneous Graph Neural Network (GNN).

        This method builds a list of connection types based on the specified graph structure
        in the configuration settings. Each connection type is represented as a tuple, e.g.,
        ('SourceNode', 'SourceNode_to_TargetNode', 'TargetNode').

        :return: List of connection types.
        :rtype: list[tuple]
        """
        edgetypes = []
        nodemap = {}

        # Build a map of node names to their types and configurations
        for node in self.m_cfgset.get("GRAPHNODE"):
            if node.get("Type") != "Global":
                nodemap[node.get("Name")] = {"Type": node.get("Type"), "Node": node}
        # Iterate through nodes to create unique edge types
        for node in self.m_cfgset.get("GRAPHNODE"):
            if node.get("Type") != "Global":
                source_type = node.get("Type")
                # Iterate through targets to create edge types
                for target in node.get("Targets"):
                    target_type = nodemap[target]["Type"]
                    edge_type = (source_type, f"{source_type}_to_{target_type}", target_type)

                    if edge_type not in edgetypes:
                        edgetypes.append(edge_type)

        return edgetypes

    def forward(self, data):
        """Perform the forward pass in PyTorch.

        This method takes input data and performs the forward pass through the neural network model.
        It utilizes the `pyhf.gnnwrapper` function to process the data before passing it to the model.

        :param data: Input data for the forward pass.
        :type data: Any suitable input type for the model.

        :return: Model predictions.
        :rtype: Tensor.
        """
        return self.m_model(**pyhf.gnnwrapper(data))

    def run(self, x, y, w):
        """Run the training and validation process for a Graph Neural Network (GNN).

        The method takes input data (x, y, w) and uses a graph builder object to construct graphs from it.
        These graphs are then parsed to DataLoader objects, and the GNN is trained using those.

        :param x: Input data in pd.DataFrame format.
        :param y: Targets in pd.DataFrame format.
        :param w: Weights in pd.DataFrame format.
        :returns: Trained GNN model and training history.
        """
        data = hf.prepinputs(x, y, w, self.m_cfgset, library="pd")
        xtrain_dict, xval_dict = data["xtrain"].to_dict('records'), data["xval"].to_dict('records')
        train_graphs = self.m_gbuilder.buildgraphs(
            xtrain_dict, data["ytrain"].values, data["wtrain"].values)
        val_graphs = self.m_gbuilder.buildgraphs(
            xval_dict, data["yval"].values, data["wval"].values)
        trainloader = DataLoader(train_graphs,
                                 batch_size=self.m_cfgset.get("MODEL").get("BatchSize"),
                                 shuffle=True)
        valloader = DataLoader(val_graphs,
                               batch_size=self.m_cfgset.get("MODEL").get("BatchSize"),
                               shuffle=True)
        InfoMessage(f"Sending graph model to {self.m_device}")
        self.m_model.to(self.m_device)
        if self.m_cfgset.get("MODEL").get("PreCompileModel"):
            InfoMessage("Pre-compiling graph model...")
            self.m_model = torch.compile(self.m_model)
        self.m_model, history = self.runepochs(self.m_model, trainloader, valloader)
        return self.m_model, history

    def _run_epoch(self, model, loader, is_training=True):
        """
        Run a training or testing epoch.

        :param model: PyTorch model for training/testing.
        :param loader: PyTorch DataLoader object.
        :param is_training: Boolean indicating whether it's a training epoch.
        :returns: Loss of the current epoch.
        """
        if is_training:
            model.train()
        else:
            model.eval()
        running_loss = 0
        total_samples = len(loader.dataset)
        with torch.set_grad_enabled(is_training):
            for data in loader:
                data.to(self.m_device)
                if is_training:
                    self.m_optimiser.zero_grad()
                out = model(**pyhf.gnnwrapper(data))
                loss = torch.mul(self.get_loss(out, data.y, data.w), len(data) / total_samples)
                if is_training:
                    loss.backward()
                    self.m_optimiser.step()
                running_loss += loss.item()
        return running_loss


class homoconvmodel(torch.nn.Module):
    """Class for a convolutional model for a homogeneous GNN.
    A 'homoconvmodel' takes convolutional layers and stack them.
    The available convulutional layers are taken from:
    https://pytorch-geometric.readthedocs.io/en/latest/modules/nn.html
    """

    def __init__(self, glayers, gact, gchannels, dropoutinformation):
        super().__init__()
        convmodel, prevch = [], -1
        layerid = 1
        dropoutprob = dropoutinformation["prob"]
        dropoutindece = dropoutinformation["indece"]
        for layer, act, channels in zip(glayers, gact, gchannels):
            dropout = 0.0
            if layerid in dropoutindece:
                dropout = dropoutprob
            if layer == "GCNConv":
                convmodel.append((GCNConv(prevch, channels), 'x, edge_index -> x'))
            elif layer == "SAGEConv":
                convmodel.append((SAGEConv(prevch, channels), 'x, edge_index -> x'))
            elif layer == "GraphConv":
                convmodel.append((GraphConv(prevch, channels, dropout=dropout), 'x, edge_index -> x'))
            elif layer == "TransformerConv":
                convmodel.append((TransformerConv(prevch, channels, dropout=dropout, heads=1), 'x, edge_index -> x'))
            elif layer == "GATConv":
                convmodel.append((GATConv(prevch, channels, dropout=dropout), 'x, edge_index, edge_attr -> x'))
            elif layer == "SAGEConv":
                convmodel.append((SAGEConv(prevch, channels, dropout=dropout), 'x, edge_index, edge_attr -> x'))
            else:
                ErrorMessage(f"Unknown homogenous graph layer: {layer}")
            prevch = channels
            convmodel.append(pyhf.get_pytorch_act(act))
            layerid+=1
        self.convmodel = Sequential('x, edge_index, edge_attr', convmodel)

    def forward(self, x: torch.Tensor, edge_index: torch.Tensor, edge_attr: torch.Tensor) -> torch.Tensor:
        """Forward method for homogeneous graph convolutional model.

        :param x: (PyTorch Tensor), [N, F_x], where N is the number of nodes and F_x the number of node features.
        :param edge_index: (PyTorch Tensor), [2, E] with max entry N - 1, where E is the number of edges.
        :param edge_attr: (PyTorch Tensor): [E, F_e] with F_e being the edge features
        :returns: (PyTorch Tensor), The output of the convolutional model.
        """
        return self.convmodel(x, edge_index, edge_attr)


class homognnmodel(basetorchmodel):
    """Class for a homogeneous GNN.
    Takes a homogeneous convolutional model and combines it with a dense NN.
    Finally the convolutional output and the dense output are combined using:
    * the global mean of the convolutional layers
    * the global maximum of the convolutional layers
    * the output of the global dense network

    """

    def __init__(self, cfgset, in_nodes):
        super().__init__(cfgset)

        indece = self.m_cfgset.get("MODEL").get("GraphDropoutIndece")
        if indece == None:
            indece = []
        prob = self.m_cfgset.get("MODEL").get("GraphDropoutProb")
        dropoutinformation = {"prob": prob,
                              "indece": indece}

        self.convmodel = homoconvmodel(
            glayers=self.m_cfgset.get("MODEL").get("GraphLayers"),
            gact=self.m_cfgset.get("MODEL").get("GraphActivationFunctions"),
            gchannels=self.m_cfgset.get("MODEL").get("GraphChannels"),
            dropoutinformation=dropoutinformation)

        if any(g.get("Type") == "Global" for g in self.m_cfgset.get("GRAPHNODE")):
            self.m_has_global = True
            self.glob = pyhf.buildgeomfc(
                in_nodes=in_nodes,
                hidden_nodes=self.m_cfgset.get("MODEL").get("GlobalNodes"),
                hidden_act=self.m_cfgset.get("MODEL").get("GlobalActivationFunctions"))
            in_nodes = self.m_cfgset.get("MODEL").get("GlobalNodes")[-1] + self.m_cfgset.get("MODEL").get("GraphChannels")[-1]
        else:
            self.m_has_global = False
            in_nodes = self.m_cfgset.get("MODEL").get("GraphChannels")[-1] * 2
        self.fcmodel = pyhf.buildgeomfc(
            in_nodes=in_nodes,
            hidden_nodes=self.m_cfgset.get("MODEL").get("FinalNodes"),
            hidden_act=self.m_cfgset.get("MODEL").get("FinalActivationFunctions"),
            out_nodes=len(
                self.m_cfgset.get("OUTPUT")),
            out_act=self.m_cfgset.get("MODEL").get("OutputActivation"))

    # pylint: disable=W0221
    # TODO find a solution for this
    def forward(self, x: torch.Tensor, edge_index: torch.Tensor, edge_attr: torch.Tensor, u: torch.Tensor, batch: torch.Tensor) -> torch.Tensor:
        """PyTorch forward pass method

        :param data: A pytorch data object describing a homogeneous graph. The data object can hold node-level, link-level and graph-level attributes.
        :returns:

        """
        x_new = self.convmodel(x.float(), edge_index, edge_attr)
        u_new = self.glob(u.float())
        x_mean_pool = global_mean_pool(
            x_new, batch)
        out = torch.cat([u_new, x_mean_pool], dim=1).float()
        return self.fcmodel(out)
    # pylint: enable=W0221


class heteroconvmodel(torch.nn.Module):
    """
    Class for a convolutional mode for a heterogeneous graph convolutions.
    We use the generic wrapper for computing graph convolution on heterogeneous graphs.
    The 'HeteroConv' layer will pass messages from source nodes to target nodes based on the bipartite GNN layer given for a specific edge type.
    The results will be aggregated using the (default) sum function.
    """

    def __init__(self, glayers, gact, gchannels, edgetypes, dropoutinformation):
        """Constructur of heteroconvmodel class. Creates a sequential set of graph convolutions.
        The used convolutions are defined via 'glayers'.
        Their activation functions are defined via 'gact'.

        :param: glayers: List of strings containing identifiers for the convolutional layers.
        :param: grap_act: List of strings containing the identifiers for the activation functions.
        :param: gchannels: List of Integers defining the hidden channels.
        :param: edgetypes: List of edge types.
        :param: dropoutinformation: Dictionary including dropout information.
        :returns:

        """
        super().__init__()
        convmodel = []
        dropoutprob = dropoutinformation["prob"]
        dropoutindece = dropoutinformation["indece"]
        layerid = 1
        for layer, _, channels in zip(glayers, gact, gchannels):
            dropout = 0.0
            if layerid in dropoutindece:
                dropout = dropoutprob
            if layer == "GCNConv":
                raise NotImplementedError
            if layer == "GraphConv":
                raise NotImplementedError
            if layer == "SAGEConv":
                convmodel.append((HeteroConv({
                    edgetype: SAGEConv((-1, -1), channels, dropout=dropout)
                    for edgetype in edgetypes
                }), 'x_dict, edge_index_dict -> x_dict'))
            elif layer == "GATConv":
                convmodel.append((HeteroConv({
                    edgetype: GATConv((-1, -1), channels, dropout=dropout, add_self_loops=False)
                    for edgetype in edgetypes
                }), 'x_dict, edge_index_dict, edge_attr_dict -> x_dict'))
            else:
                ErrorMessage(f"Unknown heterogeneous graph layer: {layer}")
            layerid+=1
        self.convmodel = Sequential('x_dict, edge_index_dict, edge_attr_dict', convmodel)

    # pylint: disable=W0221
    # TODO find solution for this
    def forward(self, x_dict, edge_index_dict, edge_attr_dict):
        """PyTorch forward pass method

        :param x_dict: {Node type:[N, F_x]}, where N is the number of nodes and F_x the number of node features.
        :param edge_index_dict: {Node type:[2, E]} with max entry N - 1, where E is the number of edges.
        :returns: The output of the heterogeneous graph convolution model

        """
        x_dict = self.convmodel(x_dict, edge_index_dict, edge_attr_dict)
        return x_dict
    # pylint: enable=W0221


class heterognnmodel(basetorchmodel):
    """
    Takes a heterogeneous convolutional model and combines it with a dense NN.
    Finally the convolutional output and the dense output are combined using:
    * the iundividual global mean of the convolutional layers separated into edge types.
    * the output of the global dense network
    """

    def __init__(self, cfgset, in_nodes, edgetypes):
        super().__init__(cfgset)

        indece = self.m_cfgset.get("MODEL").get("GraphDropoutIndece")
        if indece == None:
            indece = []
        prob = self.m_cfgset.get("MODEL").get("GraphDropoutProb")
        dropoutinformation = {"prob": prob,
                              "indece": indece}
        nodetypes = np.unique([i[0] for i in edgetypes])
        self.convmodel = heteroconvmodel(
            glayers=self.m_cfgset.get("MODEL").get("GraphLayers"),
            gact=self.m_cfgset.get("MODEL").get("GraphActivationFunctions"),
            gchannels=self.m_cfgset.get("MODEL").get("GraphChannels"),
            edgetypes=edgetypes,
            dropoutinformation=dropoutinformation)
        self.glob = pyhf.buildgeomfc(
            in_nodes=in_nodes,
            hidden_nodes=self.m_cfgset.get("MODEL").get("GlobalNodes"),
            hidden_act=self.m_cfgset.get("MODEL").get("GlobalActivationFunctions"))
        self.fcmodel = pyhf.buildgeomfc(
            in_nodes=self.m_cfgset.get("MODEL").get("GlobalNodes")[
                -1] + self.m_cfgset.get("MODEL").get("GraphChannels")[
                -1] * len(nodetypes),
            hidden_nodes=self.m_cfgset.get("MODEL").get("FinalNodes"),
            hidden_act=self.m_cfgset.get("MODEL").get("FinalActivationFunctions"),
            out_nodes=len(
                self.m_cfgset.get("OUTPUT")),
            out_act=self.m_cfgset.get("MODEL").get("OutputActivation"))

    # pylint: disable=W0221
    # TODO find a solution for this
    def forward(self, x_dict, edge_index_dict, edge_attr_dict, u, batches, datakeys):
        """PyTorch forward pass method

        :param x_dict:
        :param edge_index_dict:
        :param edge_attr_dict:
        :param u:
        :returns:

        """
        x_dict = self.convmodel(x_dict, edge_index_dict, edge_attr_dict)
        x_dict = {key: global_mean_pool(x_dict[key], batch) for key, batch in zip(datakeys, batches)}
        x_dict = torch.cat(list(x_dict.values()), dim=1)
        u_new = self.glob(u.float())
        out = torch.cat([u_new, x_dict], dim=1).float()
        return self.fcmodel(out)
    # pylint: enable=W0221
