"""This module serves as a base model for PyTorch based neural networks.
Any more complicated PyTorch (or Pytorch gemoetric) NN inherits from the
basetorchmodel class in this module.

In addition callbacks such as EarlyStopping should be introduced here.

"""
import torch
import numpy as np
import torch.nn.functional as F
from HelperModules.messagehandler import TrainerMessage

class modelcheckpoint():
    """
    Class to save the best model while training. If the current epoch's
    validation loss is less than the previous least less, then save the
    model state.
    """

    def __init__(
        self, best_valid_loss=float('inf')
    ):
        self.modelstate = {"epoch": 0,
                           "model": None,
                           "loss": best_valid_loss}

    def __call__(
        self, current_valid_loss,
        epoch, model
    ):
        """Call method

        :param current_valid_loss: Current validation loss
        :param epoch: Current epoch
        :param model: PyTorch model object
        :returns:

        """
        if current_valid_loss < self.modelstate["loss"]:
            self.modelstate["loss"] = current_valid_loss
            self.modelstate["epoch"] = epoch
            self.modelstate["model"] = model

    def __str__(self):
        """custom print function

        :returns: custom string describing the object

        """
        return f"Restoring model {self.modelstate['model']}\n Best epoch: {self.modelstate['epoch']}, best loss: {self.modelstate['loss']}"

    def getbestmodel(self):
        """Getter function to return the best model configuration

        :returns:

        """
        return self.modelstate["model"]


class EarlyStopping():
    """Early Stopping class

    """

    def __init__(self, patience=None, mindelta=None):

        self.patience = patience
        self.mindelta = mindelta
        self.counter = 0
        self.refloss = np.inf
        self.earlystop = False

    def __call__(self, validation_loss):
        if (self.refloss - validation_loss) < self.mindelta:
            self.counter += 1
            if self.counter >= self.patience:
                self.earlystop = True
        else:
            self.counter = 0
            self.refloss = validation_loss


class basetorchmodel(torch.nn.Module):
    """Base class for a PyTorch based NN

    """

    def __init__(self, cfgset):
        super().__init__()
        self.m_cfgset = cfgset
        self.m_lossfct = None
        # torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self.m_device = torch.device('cpu')
        self.m_name = self.m_cfgset.get("MODEL").get("Name")

        if not self.m_cfgset.get("MODEL").get(
                "MinDelta") is None and not self.m_cfgset.get("MODEL").get("Patience") is None:
            self.m_earlystopping = EarlyStopping(
                patience=self.m_cfgset.get("MODEL").get("Patience"),
                mindelta=self.m_cfgset.get("MODEL").get("MinDelta"))
        else:
            self.m_earlystopping = None

    def getname(self):
        """Return name of model

        :returns: name of model (str)

        """
        return self.m_name

    def setlossfct(self):
        """Setting the loss function based on given string

        :returns:

        """
        if self.m_cfgset.get("MODEL").get("Loss") == "binary_crossentropy":
            self.m_lossfct = torch.nn.BCELoss(reduction='none')
        elif self.m_cfgset.get("MODEL").get("Loss") == "categorical_crossentropy":
            self.m_lossfct = torch.nn.CrossEntropyLoss(reduction='none')
        elif self.m_cfgset.get("MODEL").get("Loss") == "MSE":
            self.m_lossfct = torch.nn.MSELoss(reduction='none')
        elif self.m_cfgset.get("MODEL").get("Loss") == "MAE":
            self.m_lossfct = torch.nn.L1Loss(reduction='none')
        elif self.m_cfgset.get("MODEL").get("Loss") == "HuberLoss":
            self.m_lossfct = torch.nn.HuberLoss(
                reduction='none', delta=self.m_cfgset.get("MODEL").get("HuberDelta"))

    def setoptimiser(self, model):
        """Getter method to return the actual optimiser object

        :param model:
        :returns:

        """
        lr = self.m_cfgset.get("MODEL").get("LearningRate")
        optimisers = {"Adadelta": torch.optim.Adadelta(model.parameters(), lr=lr),
                      "Adam": torch.optim.Adam(model.parameters(), lr=lr),
                      "Adamax": torch.optim.Adamax(model.parameters(), lr=lr),
                      "Nadam": torch.optim.NAdam(model.parameters(), lr=lr),
                      "RMSprop": torch.optim.RMSprop(model.parameters(), lr=lr)}
        return optimisers[self.m_cfgset.get("MODEL").get("Optimiser")]

    def get_loss(self, out, targets, weights):
        """Return weighted loss

        :param lossfct: Loss function to be used for loss calculation
        :param out: Output of the model (PyTorch tensor).
        :param data: Batch of graph from which one can extract labels and sample weights.
        :returns: The calculated loss on which backpropagation can be performed.
        :rtype: PyTorch tensor

        """
        if self.m_cfgset.get("MODEL").get("Loss") in ["binary_crossentropy",
                                                      "MSE",
                                                      "MAE",
                                                      "HuberLoss"]:
            loss = self.m_lossfct(out, targets.view(-1, 1))
        elif self.m_cfgset.get("MODEL").get("Loss") == "categorical_crossentropy":
            one_hot = F.one_hot(targets.long(), num_classes=out.size(1)).squeeze()
            loss = torch.sum(torch.mul(one_hot, -torch.log(out)), dim=1)
        else:
            loss = self.m_lossfct(out, targets.flatten().long())
            # loss = self.m_lossfct(out, targets.long())
        loss = torch.div(torch.dot(loss.flatten(), weights.flatten()), torch.sum(weights.flatten()))
        return loss

    def _run_epoch(self, model, loader, is_training=True):
        """
        Run a training or testing epoch.

        :param model: PyTorch model for training/testing.
        :param loader: PyTorch DataLoader object.
        :param is_training: Boolean indicating whether it's a training epoch.
        :returns: Running loss of the current epoch.
        """
        if is_training:
            model.train()
        else:
            model.eval()
        running_loss = 0
        total_samples = len(loader.dataset)

        with torch.set_grad_enabled(is_training):
            for data in loader:
                inputs, targets, weights = data
                inputs.to(self.m_device)  # TO-DO smarter handling of CPU vs GPU usage.
                targets.to(self.m_device)
                weights.to(self.m_device)

                if is_training:
                    self.m_optimiser.zero_grad()

                out = model(inputs)
                loss = torch.mul(self.get_loss(out, targets, weights), len(inputs) / total_samples)

                if is_training:
                    loss.backward()
                    self.m_optimiser.step()

                running_loss += loss.item()
        return running_loss

    def forward(self, data):
        """PyTorch forward pass method.
        The method 'forward' is abstract in 'torch.nn.modules.module' hence we have to override it here.
        It is further overriden by the child classes inheriting from basetorchmodel later on.

        :param data: PyTorch data object
        :returns: PyTorch data object (overriden with model output later)

        """
        return data

    def runepochs(self, model, train_loader, val_loader):
        """Wrapper method to run training over epochs.

        :param model: PyTorch model
        :param train_loader: DataLoader object holding train data
        :param val_loader: DataLoader object holding validation data
        :returns:

        """
        trainloss, valloss = [], []
        mcpt = modelcheckpoint()
        model.to(self.m_device)
        for epoch in range(1, self.m_cfgset.get("MODEL").get("Epochs")):
            trainloss.append(float(self._run_epoch(model=model, loader=train_loader, is_training=True)))
            valloss.append(float(self._run_epoch(model=model, loader=val_loader, is_training=False)))
            mcpt(valloss[-1], epoch, model)
            TrainerMessage(
                f'Epoch: {epoch:03d}, Train Loss: {trainloss[-1]:.4f}, Val Loss: {valloss[-1]:.4f}')
            if self.m_earlystopping is not None:
                self.m_earlystopping(valloss[-1])
                if self.m_earlystopping.earlystop:
                    break
        if self.m_cfgset.get("MODEL").get("RestoreBest"):
            TrainerMessage(f"Restoring best model configuration: {mcpt}")
            model = mcpt.getbestmodel()
        return model, {"loss": trainloss,
                       "val_loss": valloss}
