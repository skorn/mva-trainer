"""This module handles the generation of Dense Neural Network.
The network is build using PyTorch. Find the documentation here:
https://pytorch.org/docs/stable/index.html

"""
import HelperModules.helperfunctions as hf
import HelperModules.pytorchhelperfunctions as pyhf
from HelperModules.messagehandler import InfoMessage
import torch
from ModelModules.basetorchmodel import basetorchmodel
from torch.utils.data import DataLoader


class dataset(torch.utils.data.Dataset):
    """Characterizes a dataset for PyTorch

    """

    def __init__(self, x, y, w):
        self.x = torch.from_numpy(x).float()
        self.y = torch.from_numpy(y).float()
        self.w = torch.from_numpy(w).float()

    def __len__(self):
        """Denotes the total number of samples

        :returns: length of dataset

        """
        return len(self.x)

    def __getitem__(self, index):
        """Generates one sample of data

        :param index: index of dataset
        :returns: item of dataset

        """
        return (self.x[index], self.y[index], self.w[index])


class dnnmodel(basetorchmodel):
    """Standard DNN class creating a densely connected NN.

    """

    def __init__(self, cfgset):
        super().__init__(cfgset)
        self.m_model = pyhf.buildgeomfc(
            in_nodes=len(self.m_cfgset.get("VARIABLE")),
            hidden_nodes=self.m_cfgset.get("MODEL").get("Nodes"),
            hidden_act=self.m_cfgset.get("MODEL").get("ActivationFunctions"),
            out_nodes=len(self.m_cfgset.get("OUTPUT")),
            out_act=self.m_cfgset.get("MODEL").get("OutputActivation"),
            dropoutindece=self.m_cfgset.get("MODEL").get("DropoutIndece"),
            dropoutprob=self.m_cfgset.get("MODEL").get("DropoutProb"),
            batchnormindece=self.m_cfgset.get("MODEL").get("BatchNormIndece"),
            input_string="x"
        )
        self.m_optimiser = self.setoptimiser(self.m_model)
        self.setlossfct()
        if self.m_cfgset.get("MODEL").get("PreCompileModel"):
            InfoMessage("Pre-compiling DNN model...")
            self.m_model = torch.compile(self.m_model)

    def forward(self, data):
        """PyTorch forward pass method

        :param x: input data
        :returns: model output

        """
        return self.m_model(data)

    def run(self, x, y, w):
        """Runner method for a DNN.
        Takes input data and used DataLoader objects to train NN.

        :param x: Input data in pd.DataFrame format
        :param y: Targets in pd.DataFrame format
        :param w: Weights in pd.DataFrame format
        :returns:

        """
        data = hf.prepinputs(x, y, w, self.m_cfgset)
        trainloader = DataLoader(dataset(data["xtrain"], data["ytrain"], data["wtrain"]),
                                 batch_size=self.m_cfgset.get("MODEL").get("BatchSize"),
                                 shuffle=True)
        valloader = DataLoader(dataset(data["xval"], data["yval"], data["wval"]),
                               batch_size=self.m_cfgset.get("MODEL").get("BatchSize"),
                               shuffle=True)
        self.m_model, history = self.runepochs(self.m_model, trainloader, valloader)
        return self.m_model, history
