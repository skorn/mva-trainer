""" Module handling the conversion of models to onnx

"""
import numpy as np
import torch
import HelperModules.pytorchhelperfunctions as pyhf
from HelperModules.graphbuilder import graphbuilder
from torch_geometric.loader import DataLoader


class onnxhandler():
    """
    Class to handle the conversion of models to onnx
    """

    def __init__(self, cfgset):
        self.m_cfgset = cfgset
        self.m_sampleinput = self._getsampleinput()

    def _getsampleinput(self):
        if "DNN" in self.m_cfgset.get("MODEL").get("Type"):
            return torch.tensor(
                np.array([np.random.rand(len(self.m_cfgset.get("VARIABLE"))).astype(np.float32)]))
        if "GNN" in self.m_cfgset.get("MODEL").get("Type"):
            gbuilder = graphbuilder(gnodes=self.m_cfgset.get("GRAPHNODE"),
                                    graphstruct=self.m_cfgset.get("MODEL").get("GraphStructure"))
            x_dummy = np.array([{v.get("Name"): 1. for v in self.m_cfgset.get("VARIABLE")}])
            y_dummy = np.array([[0]])
            w_dummy = np.array([[1.]])
            dummy_graphs = gbuilder.buildgraphs(x_dummy, y_dummy, w_dummy)
            dummy_loader = DataLoader(dummy_graphs,
                                      batch_size=1,
                                      shuffle=True)
            dummy_graph = next(iter(dummy_loader))
            return pyhf.gnnwrapper(dummy_graph)
        return None

    def _convertDNN(self, model, onnxpath):
        """Method to convert a PyTorch DNN to onnx

        :param model:
        :param onnxpath:
        :returns:

        """
        torch.onnx.export(model,  # model being run
                          self.m_sampleinput,  # model input (or a tuple for multiple inputs)
                          onnxpath,  # where to save the model (can be a file or file-like object)
                          export_params=True,  # store the trained parameter weights inside the model file
                          opset_version=self.m_cfgset.get("MODEL").get(
                              "ONNXVersion"),  # the ONNX version to export the model to
                          do_constant_folding=True,  # whether to execute constant folding for optimization
                          input_names=['input'],   # the model's input names
                          output_names=['output'],  # the model's output names
                          dynamic_axes={'input': {0: 'batch_size'},    # variable length axes
                                        'output': {0: 'batch_size'}})

    def _convertGNN(self, model, onnxpath):
        """Method to convert a PyTorch GNN to onnx

        :param model:
        :param onnxpath:
        :returns:

        """
        torch.onnx.export(model,  # model being run
                          self.m_sampleinput,  # model input (or a tuple for multiple inputs)
                          onnxpath,  # where to save the model (can be a file or file-like object)
                          export_params=True,  # store the trained parameter weights inside the model file
                          opset_version=self.m_cfgset.get("MODEL").get(
                              "ONNXVersion"),  # the ONNX version to export the model to
                          do_constant_folding=True,  # whether to execute constant folding for optimization
                          input_names=['input'],   # the model's input names
                          output_names=['output'])  # the model's output names

    def convert(self, model, onnxpath):
        """Main method to convert models to onnx

        :param model: Model object
        :param onnxpath: File path to onnx object
        :returns:

        """
        if "DNN" in self.m_cfgset.get("MODEL").get("Type"):
            self._convertDNN(model, onnxpath)
        if "GNN" in self.m_cfgset.get("MODEL").get("Type"):
            self._convertGNN(model, onnxpath)
