"""Module to handle optimisation of models

"""

import os
import numpy as np
import HelperModules.helperfunctions as hf
from HelperModules.messagehandler import OptimiserMessage


class modeloptimisation():
    """Class handling the optimisation of models
    by defining config files (e.g. for HTCondor).

    """

    def __init__(self, args, cfgset):
        self.m_args = args
        self.m_cfgset = cfgset

    def getdnnpardict(self):
        """Function to generate a parameter dictionary containing relevant optimisation information for a DNN

        :returns: Parameter dictionary

        """
        smodel = self.m_cfgset.get("MODEL")
        d = {
            "Name": smodel.get("Name"),
            "MinLayers": smodel.get("MinLayers"),
            "MaxLayers": smodel.get("MaxLayers"),
            "Nodes": range(
                smodel.get("MinNodes"),
                smodel.get("MaxNodes"),
                smodel.get("StepNodes")),
            "SetActivationFunctions": smodel.get("SetActivationFunctions"),
            "LearningRate": np.arange(
                smodel.get("MinLearningRate"),
                smodel.get("MaxLearningRate") +
                smodel.get("StepLearningRate"),
                smodel.get("StepLearningRate"))}

        modeldict = {}
        for iteration in range(self.m_args.nModels):
            modeldict[f"P{iteration}"] = {}
            nodes = [
                str(item) for item in np.random.choice(
                    d["Nodes"],
                    np.random.randint(
                        d["MinLayers"],
                        d["MaxLayers"],
                        1),
                    replace=True).tolist()]
            layers = len(nodes)
            choicearr = range(0, layers)
            dropoutsize = np.random.randint(0, layers, 1)
            batchnormsize = np.random.randint(0, layers, 1)
            modeldict[f"P{iteration}"]["Name"] = f"{d['Name']}_P{iteration}"
            modeldict[f"P{iteration}"]["Nodes"] = ",".join(nodes)
            modeldict[f"P{iteration}"]["DropoutIndece"] = ",".join(
                [str(item) for item in sorted(np.random.choice(choicearr, dropoutsize, replace=False).tolist())])
            modeldict[f"P{iteration}"]["BatchNormIndece"] = ",".join(
                [str(item) for item in sorted(np.random.choice(choicearr, batchnormsize, replace=False).tolist())])
            modeldict[f"P{iteration}"]["ActivationFunctions"] = ",".join(
                [str(item) for item in [np.random.choice(d["SetActivationFunctions"]) for _ in range(layers)]])
            modeldict[f"P{iteration}"]["LearningRate"] = np.random.choice(d["LearningRate"], size=1)[0]
            if not modeldict[f"P{iteration}"]["DropoutIndece"]:
                modeldict[f"P{iteration}"]["DropoutIndece"] = "-1"
            if not modeldict[f"P{iteration}"]["BatchNormIndece"]:
                modeldict[f"P{iteration}"]["BatchNormIndece"] = "-1"
        return modeldict

    def getgnnpardict(self):
        """Function to generate a parameter dictionary containing relevant optimisation information for a GNN

        :returns: Parameter dictionary

        """
        smodel = self.m_cfgset.get("MODEL")
        d = {
            "MinLayers": smodel.get("MinLayers"),
            "MaxLayers": smodel.get("MaxLayers"),
            "Nodes": range(
                smodel.get("MinNodes"),
                smodel.get("MaxNodes"),
                smodel.get("StepNodes")),
            "SetActivationFunctions": smodel.get("SetActivationFunctions"),
            "SetGraphLayers": smodel.get("SetGraphLayers"),
            "LearningRate": np.arange(
                smodel.get("MinLearningRate"),
                smodel.get("MaxLearningRate") +
                smodel.get("StepLearningRate"),
                smodel.get("StepLearningRate"))}

        modeldict = {}
        for iteration in range(self.m_args.nModels):
            modeldict[f"P{iteration}"] = {}
            globalnodes = [
                str(item) for item in np.random.choice(
                    d["Nodes"],
                    np.random.randint(
                        d["MinLayers"],
                        d["MaxLayers"],
                        1),
                    replace=True).tolist()]
            finalnodes = [
                str(item) for item in np.random.choice(
                    d["Nodes"],
                    np.random.randint(
                        d["MinLayers"],
                        d["MaxLayers"],
                        1),
                    replace=True).tolist()]
            graphchannels = [
                str(item) for item in np.random.choice(
                    d["Nodes"],
                    np.random.randint(
                        d["MinLayers"],
                        d["MaxLayers"],
                        1),
                    replace=True).tolist()]
            layers_globalnodes = len(globalnodes)
            layers_finalnodes = len(finalnodes)
            layers_graphchannels = len(graphchannels)
            modeldict[f"P{iteration}"]["Name"] = smodel.get("Name") + f"_P{iteration}"
            modeldict[f"P{iteration}"]["GlobalNodes"] = ",".join(globalnodes)
            modeldict[f"P{iteration}"]["FinalNodes"] = ",".join(finalnodes)
            modeldict[f"P{iteration}"]["GraphChannels"] = ",".join(graphchannels)
            modeldict[f"P{iteration}"]["GraphLayers"] = ",".join(
                [str(item) for item in [np.random.choice(d["SetGraphLayers"]) for _ in range(layers_graphchannels)]])
            modeldict[f"P{iteration}"]["GraphActivationFunctions"] = ",".join(
                [str(item) for item in [np.random.choice(d["SetActivationFunctions"]) for _ in range(layers_graphchannels)]])
            modeldict[f"P{iteration}"]["GlobalActivationFunctions"] = ",".join(
                [str(item) for item in [np.random.choice(d["SetActivationFunctions"]) for _ in range(layers_globalnodes)]])
            modeldict[f"P{iteration}"]["FinalActivationFunctions"] = ",".join(
                [str(item) for item in [np.random.choice(d["SetActivationFunctions"]) for _ in range(layers_finalnodes)]])
            modeldict[f"P{iteration}"]["LearningRate"] = np.random.choice(d["LearningRate"], size=1)[0]
        return modeldict

    def writeconfigs(self):
        """Method to write functions to disk.

        :returns:

        """
        self.m_cfgset.get("DIR").get("Configs")
        if self.m_cfgset.get("GENERAL").get("RandomSeed") is not None:
            np.random.seed(self.m_cfgset.get("GENERAL").get("RandomSeed"))
        if "DNN" in self.m_cfgset.get("MODEL").get("Type"):
            pardict = self.getdnnpardict()
        if "GNN" in self.m_cfgset.get("MODEL").get("Type"):
            pardict = self.getgnnpardict()

        optimisationpath = f"{self.m_cfgset.get('GENERAL').get('Job')}/Configs"
        modelpath = f"{self.m_cfgset.get('GENERAL').get('Job')}/Model"

        for iteration in range(self.m_args.nModels):
            OptimiserMessage(
                f"Generating HTCondor (P{iteration}) config file in {hf.addslash(os.path.abspath(optimisationpath))}{self.m_cfgset.get('MODEL').get('Name')}_P{iteration}.cfg")
            fcfg = open(
                f"{hf.addslash(os.path.abspath(optimisationpath))}{self.m_cfgset.get('MODEL').get('Name')}_P{iteration}.cfg",
                "w",
                encoding="utf-8")
            # GENERAL block
            fcfg.write("GENERAL\n")
            for key, opt in self.m_cfgset.get("GENERAL").getopts().items():
                if opt.is_set():
                    fcfg.write(f"\t{opt.getostr()}\n")
            fcfg.write("\n")
            for sample in self.m_cfgset.get("SAMPLE"):
                fcfg.write("SAMPLE\n")
                for key, opt in sample.getopts().items():
                    if opt.is_set():
                        fcfg.write(f"\t{opt.getostr()}\n")
                fcfg.write("\n")
            for variable in self.m_cfgset.get("VARIABLE"):
                fcfg.write("VARIABLE\n")
                for key, opt in variable.getopts().items():
                    if opt.is_set():
                        fcfg.write(f"\t{opt.getostr()}\n")
                fcfg.write("\n")
            if "GNN" in self.m_cfgset.get("MODEL").get("Type"):
                for variable in self.m_cfgset.get("GRAPHNODE"):
                    fcfg.write("GRAPHNODE\n")
                    for key, opt in variable.getopts().items():
                        if opt.is_set():
                            fcfg.write(f"\t{opt.getostr()}\n")
                    fcfg.write("\n")
            for variable in self.m_cfgset.get("OUTPUT"):
                fcfg.write("OUTPUT\n")
                for key, opt in variable.getopts().items():
                    if opt.is_set():
                        fcfg.write(f"\t{opt.getostr()}\n")
                fcfg.write("\n")

            if "DNN" in self.m_cfgset.get("MODEL").get("Type"):
                fcfg.write("DNNMODEL\n")
            if "GNN" in self.m_cfgset.get("MODEL").get("Type"):
                fcfg.write("GNNMODEL\n")

            for key, opt in self.m_cfgset.get("MODEL").getopts().items():
                if opt.is_set():
                    if key in pardict[f"P{iteration}"].keys():
                        fcfg.write(f"\t{key} = {pardict[f'P{iteration}'][key]}\n")
                    else:
                        fcfg.write(f"\t{opt.getostr()}\n")
            fcfg.close()
            OptimiserMessage(
                f"Generating HTCondor (P{iteration}) submission file in {hf.addslash(os.path.abspath(optimisationpath))}{self.m_cfgset.get('MODEL').get('Name')}_P{iteration}.sub")
            fsub = open(
                f"{hf.addslash(optimisationpath)}{self.m_cfgset.get('MODEL').get('Name')}_P{iteration}.sub",
                "w",
                encoding="utf-8")
            fsub.write("###################\n")
            fsub.write("### Job Options ###\n")
            fsub.write("###################\n")
            fsub.write(
                "executable\t\t= $ENV(MVA_TRAINER_BASE_DIR)/scripts/HTCondorScripts/HTCondorMain.sh\n")
            fsub.write(
                f"arguments\t\t= {self.m_args.HO_options} {hf.addslash(optimisationpath)}{self.m_cfgset.get('MODEL').get('Name')}_P{iteration}.cfg $(ProcId)\n")
            fsub.write("request_memory\t\t= 2000\n")
            # Setting the maximum run time to 8 hours (because this does not exist as
            # an independent flavour)
            fsub.write("+MaxRuntime\t\t= 28800\n")
            fsub.write("max_retries\t\t= 3\n")
            fsub.write("requirements\t\t= Machine =!= LastRemoteHost\n")
            fsub.write("on_exit_remove\t\t= (ExitBySignal == False) && (ExitCode == 0)\n")
            fsub.write(f'+JobBatchName\t\t= {self.m_cfgset.get("MODEL").get("Name")}_P{iteration}\n')
            fsub.write("\n")
            fsub.write("###################\n")
            fsub.write("### OutputFiles ###\n")
            fsub.write("###################\n")
            fsub.write(
                f"log\t\t= {hf.addslash(optimisationpath)}{self.m_cfgset.get('MODEL').get('Name')}_P{iteration}_P$(ProcId)_log.txt\n")
            fsub.write(
                f"error\t\t= {hf.addslash(optimisationpath)}{self.m_cfgset.get('MODEL').get('Name')}_P{iteration}_P$(ProcId)_error.txt\n")
            fsub.write(
                f"output\t\t= {hf.addslash(optimisationpath)}{self.m_cfgset.get('MODEL').get('Name')}_P{iteration}_P$(ProcId)_out.txt\n")
            fsub.write("getenv\t\t= True\n")
            fsub.write("stream_error\t\t= True\n")
            fsub.write("stream_output\t\t= True\n")
            fsub.write('requirements\t\t= OpSysAndVer == "CentOS7"\n')
            fsub.write("should_transfer_files\t= YES\n")
            fsub.write("when_to_transfer_output\t= ON_EXIT\n")
            fsub.write("\n")
            fsub.write("#####################\n")
            fsub.write("### Notifications ###\n")
            fsub.write("#####################\n")
            fsub.write(f"#notify_user\t\t= {os.environ.get('USER')}@cern.ch\n")
            fsub.write("#notification\t\t= always\n")
            fsub.write(f"queue {self.m_cfgset.get('GENERAL').get('Folds')}\n")
            fsub.close()
        OptimiserMessage(
            f"Generating HTCondor dag file in {hf.addslash(os.path.abspath(optimisationpath))}{self.m_cfgset.get('MODEL').get('Name')}.dag")
        dag = open(
            f"{hf.addslash(optimisationpath)}{self.m_cfgset.get('MODEL').get('Name')}.dag",
            "w",
            encoding="utf-8")
        for iteration in range(self.m_args.nModels):
            dag.write(f"JOB P_{iteration} {hf.addslash(os.path.abspath(optimisationpath))}{self.m_cfgset.get('MODEL').get('Name')}_P{iteration}.sub\n")
        dag.write(f"FINAL $ENV(MVA_TRAINER_BASE_DIR)/scripts/HTCondorScripts/HTCondorSummary.sh --directory {hf.addslash(os.path.abspath(modelpath))}")
        dag.close()
