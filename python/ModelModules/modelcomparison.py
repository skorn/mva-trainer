"""Module to handle comparison of models

"""
from ModelModules.modelhandler import modelhandler
from HelperModules.datahandler import datahandler
from HelperModules.messagehandler import ComparatorMessage
import PlotterModules.plots as plt
import pandas as pd
from sklearn import metrics
import pickle


class modelcomparison():
    """Class handling the comparison of models.

    """

    def __init__(self, cfgsets, comparisons):
        self.m_cfgsets = cfgsets
        self.m_comparisons = comparisons
        self.m_aucs = {}
        self.m_tprs = {}
        self.m_fprs = {}

    def auc_ranking(self):
        for cfgset in self.m_cfgsets:
            for target, _ in enumerate(cfgset.get("OUTPUT")):
                if target not in self.m_aucs:
                    self.m_aucs[target] = {}
                    self.m_tprs[target] = {}
                    self.m_fprs[target] = {}
                mhandler = modelhandler(cfgset, verbose=False)
                dhandler = datahandler(cfgset)
                dfs = []
                for fold in range(cfgset.get("GENERAL").get("Folds")):
                    vnames = [v.get("Name") for v in cfgset.get("VARIABLE")]
                    xin = dhandler.get(fold,
                                       vnames=vnames,
                                       test=True,
                                       return_NaNs=True,
                                       scaled=True)
                    tmpidx = dhandler.get(fold,
                                          vnames=["Index_tmp"],
                                          test=True,
                                          return_NaNs=True,
                                          scaled=False).reset_index(drop=True)
                    df = pd.concat([mhandler.predict(xin, fold), tmpidx], axis=1)
                    dfs.append(df)
                predictions = pd.concat(dfs)
                dhandler.addpredictions(predictions)
                outnames = [output.get("Name") for output in cfgset.get("OUTPUT")]
                self.m_fprs[target], self.m_tprs[target], _ = plt.roc_curve(
                    y_true=dhandler.get("all", "Label", scaled=False),
                    y_score=dhandler.get("all", outnames[target], scaled=False),
                    sampling=cfgset.get("GENERAL").get("ROCSampling"),
                    target=target if len(cfgset.get("OUTPUT")) != 1 else target + 1,
                    weights=dhandler.get("all", "Weight", scaled=False))
                self.m_aucs[target][cfgset.get("MODEL").get("Name")] = metrics.auc(self.m_fprs[target], self.m_tprs[target])
        for key, _ in self.m_aucs.items():
            self.m_aucs[key] = dict(sorted(self.m_aucs[key].items(), key=lambda item: item[1], reverse=True))
            counter = 0
            while counter < len(self.m_aucs[key].items()):
                counter += 1
                if counter > self.m_comparisons:
                    break
        for key, d in self.m_aucs.items():
            ComparatorMessage(f"Comparison of AUCs for target: {key}")
            counter = 0
            for key_, value in d.items():
                counter += 1
                if counter > self.m_comparisons and not self.m_comparisons == -1:
                    break
                ComparatorMessage(f"{key_}\t combined AUC: {value}")

    def save(self):
        with open(f"{self.m_cfgsets[0].get('DIR').get('Model')}/aucs.json", "wb") as f:
            pickle.dump(self.m_aucs, f)
        with open(f"{self.m_cfgsets[0].get('DIR').get('Model')}/tprs.json", "wb") as f:
            pickle.dump(self.m_tprs, f)
        with open(f"{self.m_cfgsets[0].get('DIR').get('Model')}/fprs.json", "wb") as f:
            pickle.dump(self.m_fprs, f)
